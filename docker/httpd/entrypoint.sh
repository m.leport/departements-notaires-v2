#!/bin/sh

uid=$(stat -c %u /srv)
gid=$(stat -c %g /srv)

sed -i -r "s/web:x:\d+:\d+:/web:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/web:x:\d+:/web:x:$gid:/g" /etc/group

httpd-foreground
