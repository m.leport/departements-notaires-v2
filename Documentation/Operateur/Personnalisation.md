# Personnalisation de l'application

Sont décrites ici les étapes pour personnaliser l'application.

Cette personnalisation vous permettra de modifier tous les "templates" de l'application (y compris les emails et les
pdfs).

## Créer un nouveau template

A la racine de l'application, dans le répertoire `templates`, créer un nouveau dossier, par exemple `montemplate`

Copier à l'intérieur l'intégralité du dossier `templates/default`
(ou uniquement le ou les fichiers templates que vous souhaitez modifier)

Vous êtes maintenant libre de modifier complétement les templates du dossier `templates/montemplate`

## Important

Après chaque modification effectuée dans les nouveaux templates, il est nécessaire de vider
les caches de l'application pour la voir apparaître sur le site en utilisant la commande :

> ./bin/console cache:clear

## Associer le template créé à l'application

Il ne reste plus qu'a configurer l'application pour lui dire d'utiliser le nouveau template créé.

Pour cela il faut modifier le fichier `.env` à la racine de l'application.

Il suffit de remplacer la valeur de la variable `APP_THEME`, par exemple `APP_THEME=montemplate`

Si aucune valeur n'est renseigné pour `APP_THEME`, l'application utilise par défaut les templates du
dossier `templates/default`

## Arborescence du dossier template

* `account` : templates des informations utiliateurs (pages "mon compte" et "réinitialisation du mot de passe")
* `admin` : templates des pages l'administration (instructeurs, utilisateurs, logs et paramètres)
* `emails` : templates des emails
* `form` et `macro` : templates servant à l'affichage des champs des formulaires
* `pdf` : templates des pdf
* `search` : templates des pages recherche et historique des recherches
* `security` : templates liés au système de connexion (mot de passe, connexion, ....)
* `stats` : templates des pages statistiques
* `utils` : templates des entetes et pieds de pages

## Ajout de classes CSS personnalisées pour modifier l'apparence graphique de l'application

Il est possible de modifier ou de surcharger les classes CSS utilisées par l'application.

Pour cela il suffit d'intégrer le code CSS souhaité dans le fichier à la racine de l'application `public/styles.css`

## Description des balises de modèles appliquables dans le template

La disponibilité des champs en provenance de la base de données est assurée uniquement dans les
templates où une personne a été trouvé, soit *search_found.html.twig* et
*search_found_rec.html.twig*.

|                   | Provenance            | Valeur possible             | Entité                     | Colonne                                                         | Traitement                                    | type de template | Template utilisé                                                                |
|:-----------------:|:----------------------|:----------------------------|:---------------------------|:----------------------------------------------------------------|:----------------------------------------------|:-----------------|:--------------------------------------------------------------------------------|
|    `firstName`    | formulaire            | string(255)                 | Person                     | prenom                                                          | -                                             | mail, pdf        | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|   `middleName`    | formulaire            | string(255)                 | Person                     | prenomd                                                         | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest              |
|    `thirdName`    | formulaire            | string(255)                 | Person                     | prenomt                                                         | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest              |
|     `useName`     | formulaire            | string(255)                 | Person                     | nom_usage                                                       | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|    `civilName`    | formulaire            | string(255)                 | Person                     | nom_civil                                                       | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|      `name`       | formulaire            | string(255)                 | AbstractPerson             | `civilName` ou `useName` selon le paramétrage de l'instance D&N | -                                             | mail, pdf        | search_found, search_notfound, search_ambiguous, search_hasrequest              |
|    `birthDate`    | formulaire            | DateTime                    | Person/SearchLog           | date_naissance                                                  | Transformation en type string au format d/m/Y | mail, pdf        | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|    `deathDate`    | formulaire            | DateTime                    | Person                     | date_deces                                                      | Transformation en type string au format d/m/Y | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|  `responseType`   | formulaire            | DateTime                    | SearchLog                  | type_reponse                                                    | -                                             | pdf              | search_found_notrec, search_found_rec, search_notfound                          |
|  `firstNameBdd`   | base de données       | varchar(255)                | Person                     | prenom                                                          | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|     `nameBdd`     | base de données       | varchar(255)                | AbstractPerson             | `civilName` ou `useName` selon le paramétrage de l'instance D&N | Transformation en majuscule                   | mail, pdf        | search_found, search_found_rec                                                  |
|  `birthDateBdd`   | base de données       | date                        | Person                     | date_naissance                                                  | Transformation en type string au format d/m/Y | mail, pdf        | search_found, search_found_rec                                                  |
|  `deathDateBdd`   | base de données       | date                        | Person                     | date_deces                                                      | Transformation en type string au format d/m/Y | mail, pdf        | search_found, search_found_rec                                                  |
|    `genderBdd`    | base de données       | varchar(1)                  | Person                     | sexe                                                            | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|   `useNameBdd`    | base de données       | varchar(255)                | Person                     | nom_usage                                                       | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|  `civilNameBdd`   | base de données       | varchar(255)                | Person                     | nom_civil                                                       | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|  `middleNameBdd`  | base de données       | varchar(255)                | Person                     | prenomd                                                         | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|  `thirdNameBdd`   | base de données       | varchar(255)                | Person                     | prenomt                                                         | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|   `addressBdd`    | base de données       | varchar(255)                | Person                     | adresse                                                         | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|  `mdtAddressBdd`  | base de données       | varchar(255)                | Person                     | mdt_adresse                                                     | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|     `mdtBdd`      | base de données       | varchar(255)                | Person                     | mdt                                                             | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|   `mdtPhoneBdd`   | base de données       | varchar(255)                | Person                     | mdt_telephone                                                   | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|   `mdtMailBdd`    | base de données       | varchar(255)                | Person                     | mdt_courriel                                                    | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|    `labelBdd`     | base de données       | varchar(255)                | Person                     | libelle                                                         | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|   `helpCodeBdd`   | base de données       | varchar(50)                 | Person                     | code_aide                                                       | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
|  `personNumBdd`   | base de données       | integer                     | Person                     | num_ind                                                         | -                                             | mail, pdf        | search_found, search_found_rec                                                  |
| `libelle_notaire` | formulaire            | string(255)                 | User                       | nom                                                             | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest              |
|  `mail_notaire`   | formulaire            | string(255)                 | User                       | mail                                                            | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest              |
|    `userName`     | base de données       | varchar(255)                | User                       | nom                                                             | -                                             | mail             | death_alert                                                                     |
|    `userEmail`    | base de données       | varchar(255)                | User                       | mail                                                            | -                                             | mail             | death_alert                                                                     |
|     `gender`      | base de données       | varchar(1)                  | Person                     | sexe                                                            | -                                             | mail, pdf        | death_alert, search_found_notrec, search_found_rec                              |
|  `deathLocation`  | formulaire            | string(255)                 | -                          | -                                                               | -                                             | mail             | search_found, search_notfound, search_ambiguous, search_hasrequest, death_alert |
|  `person.title`   | fichier de traduction | `.H` : *Monsieur*, `.F` : * |                            |                                                                 |                                               |                  |                                                                                 |
|      Madame*      | -                     | -                           | Appel de la fonction trans | mail, pdf                                                       | *                                             |                  |                                                                                 |
|  `person.birth`   | fichier de traduction | `.H` : *né*, `.F` : *       |                            |                                                                 |                                               |                  |                                                                                 |
|       née*        | -                     | -                           | Appel de la fonction trans | mail, pdf                                                       | *                                             |                  |                                                                                 |

## Paramètres disponibles dans le template

Dans tous les templates d'email il est possible d'utiliser les données définies dans activation parameter
les paramètres de l'application, tel que :

* `settings.data.appName` : le nom de l'application
* `settings.data.appUrl` : l'url de l'application
* `settings.data.appLogo` : le logo de l'application
* `settings.data.appFavicon` : le favicon du site
* `settings.data.departmentName` : le nom du département
* `settings.data.departmentSite` : le site web du département
* `settings.data.departmentCilSite` : le site CIL du département
* `settings.data.connectionAttempts` : le nombre de tentatives de connexion avant blocage
* `settings.data.soundexSearch` : la valeur de la recherche soundex
* `settings.data.acceptErrorInBirthDate` : la précision de la date de naissance
* `settings.data.ignoreDifferentFirstNameSearch` : la valeur du prénom est différent alors il donne ambigu et non connu
* `settings.data.searchByName` : la valeur de la recherche par le nom
* `settings.data.deathAlert` : la valeur de l'alerte décès
* `settings.data.deathAlertMail` : le mail de destination des alertes décès
* `settings.data.mailSignature` : la signature des mails système
* `settings.data.mailIntroduction` : l'introduction des mails système
* `settings.data.mailNotaBene` : le notabene des mails système
* `settings.data.claims` : l'activation des créances
* `settings.data.claimsRecoverableStep` : le seuil des créances récupérables
* `settings.data.claimsNonRecoverableStep` : le seuil des créances non récupérables
* `settings.data.claimsMail` : le mail du service des créances
* `settings.data.claimsMailManagement` : le mail du service de gestion
* `settings.data.mailBusinessSignature` : la signature des mails métier
* `settings.data.pdfDestinationCity` : la ville destinataire des lettres PDF
* `settings.data.pdfLogo1` : le logo en entête des lettres PDF
* `settings.data.pdfLogo2` : le logo en pied de page des lettres PDF
* `settings.data.pdfServiceLogo` : le logo de service des lettres PDF
* `settings.data.pdfSuccessionMail` : le mail de succession des lettres PDF
* `settings.data.pdfSignature` : la signature des lettres PDF
* `settings.data.pdfDepartmentalHouseName` : la maison départementale des lettres PDF
* `settings.data.pdfDepartmentalHousePhone` : le téléphone de la maison départementale des lettres PDF
* `settings.data.pdfCSS` : le fichier CSS personnalisé
