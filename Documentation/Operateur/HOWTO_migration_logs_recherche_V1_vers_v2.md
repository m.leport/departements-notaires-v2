# Migrer les logs de recherche de la v1 vers la v2

L'import des logs de recherche de la v1 se fait à partir d'un export CSV qui devra avoir la même structure que le
fichier
d'exemple [appli_sf/imports/logs_recherche_v1.csv](../../appli_sf/imports/logs_recherche_v1.csv)

> :warning: **Tous les comptes utilisateurs et individus de la V1 devront être importés avant les logs de recherche**

- [Importer les comptes utilisateurs de la V1 à la V2](HOWTO_migration_utilisateurs_v1_vers_v2.md)
- [Importer les individus de la V1 à la V2](Import-individus.md)

- Une fois les imports effectués se positionner à la racine de l'application
- Déposer le fichier à importer dans le dossier `imports` à la racine du projet.
- Lancer la commande (en remplaçant "imports/logs_recherche_v1.csv" avec le chemin vers le fichier d'export de la v1) :

  ```shell
  bin/console import:logs_recherche_v1 imports/logs_recherche_v1.csv
  ```

## Détail du fichier CSV

Fichier CSV au format suivant :

* Séparateur : `;`
* Délimitation des chaines de caractères : `"`

Les différentes colonnes :

* `id` est auto-incrémenté, propre à la table
* `id_recherche` identifiant de la recherche, généré aléatoirement
* `nom_etude` est le login du membre ayant réalisé la recherche
* `libelle` est le nom du membre ayant réalisé la recherche
* `nom_usage` est le nom d'usage de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç ')
* `nom_civil` est le nom de naissance de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç ')
* `prenom` est le prénom de la personne recherchée (accepte : - espace è é ô ö ï ë ä ç)
* `date_naissance` est la date de naissance de la personne recherchée (YYYY-MM-DD)
* `lien_pdf` identique à id_recherche sinon "pas de pdf"
* `date` est la date de la réalisation de la recherche (YYYY-MM-DD HH: mm:ss)
* `reponse` est le type de réponse (individu connu ou non)
    - 1 : connu ASG
    - 2 : connus ADPA PCH
    - 3 : pas connu
    - 4 : ambigu
* `date_deces` est la date de décès de la personne recherchée (YYYY-MM-DD)
* `lieu_deces` est le lieu de décès de la personne recherchée (- espace ')
* `date_acte` est la date de l'acte de décès de la personne recherchée (YYYY-MM-DD)
* `id_individus` renseigné uniquement si l’individu est connu, correspond à son identifiant issu de l’application tiers
* `mdr` est le nom du référent administratif du dossier de la personne
* `tel_mdr` est le téléphone du référent administratif
* `prenom2` est le champ reprenant les autres prénoms de la personne recherchée
* `dest_etude` est un champ libre pour le membre réalisant la recherche dans l'étude notariale

Important : Il faut bien laisser la première ligne d'entête comme dans le fichier d'exemple.
