# Fonctionnalité de Choix du Fuseau Horaire

L'ajout de la fonctionnalité de choix du fuseau horaire dans votre application permet
de personnaliser le fuseau horaire utilisé par l'application en utilisant une variable
d'environnement appelée `APP_TIMEZONE`. Cette fonctionnalité est particulièrement utile lorsque le
fuseau horaire par défaut récupéré à partir de la configuration serveur ne convient pas.

## Configuration par Défaut

Par défaut, le fuseau horaire de l'application est configuré pour être "Europe/Paris".
Cependant, vous pouvez modifier ce réglage en spécifiant la valeur de la variable d'environnement
`APP_TIMEZONE` selon le fuseau horaire de votre choix.
Si un fuseau horaire est renseigné l'application vérifie d'abord si la
valeur fournie est un fuseau horaire valide en utilisant la fonction timezone_identifiers_list().
Si c'est le cas, elle utilise cette valeur comme nouveau
fuseau horaire pour l'application. Si la valeur n'est pas valide, elle utilise "Europe/Paris"
comme fuseau horaire par défaut.

