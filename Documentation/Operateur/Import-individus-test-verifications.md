# Données CSV de test

## Pré-requis

Avoir [importé les données CSV](Import-individus.md) (de test) des individus

## Utilisation du jeu de test

Le jeu de test importé permet de tester tous les cas de résultats de recherche.
Voici la liste des critères de recherche à renseigner pour retrouver le cas souhaité :

### Cas 1 Résultat attendu : connue, aide récupérable

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/12/1977

### Cas 2 : Résultat attendu : connue, aide non récupérable

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Maesson
- Nom d'usage : ALOUANE
- Date de naissance : 03/12/1954

### Cas 3 : Résultat attendu : inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Mickael
- Nom d'usage : Dupont
- Date de naissance : 07/08/1986

### Cas 4 : Résultat attendu : inconnue, demande en cours d'instruction

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Yvette
- Nom d'usage : ROSA
- Date de naissance : 08/12/1968

### Cas 5 : Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Clara
- Nom d'usage : BOURDIAU
- Date de naissance : 01/09/1966

### Cas 6

Cas 6 : Nom composée saisie dans le mauvais ordre, Résultat attendu : connue, aide non récupérable

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Gaël
- Nom d'usage : PAYSAA CAMPO
- Date de naissance : 16/04/2008

### Cas 7

Cas 7 : Année de naissance différente saisie, paramètre Année différente donne ambigu au lieu de connue n'est pas
sélectionné, Résultat attendu : inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/12/1976

### Cas 8

Cas 8 : Année de naissance différente saisie, paramètre Année différente donne ambigu au lieu de connue est sélectionné,
Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/12/1976

### Cas 9

Cas 9 : Prénom différent saisi paramètre Prénom différent donne ambigu au lieu de connue n'est pas sélectionné, Résultat
attendu : connue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Jeanine
- Nom d'usage : MILLINER
- Date de naissance : 01/01/1930

### Cas 10

Cas 10 : Prénom différent saisie, paramètre Prénom différent donne ambigu au lieu de connue est sélectionné, Résultat
attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Jeanine
- Nom d'usage : MILLINER
- Date de naissance : 01/01/1930

### Cas 11

Cas 11 : Jour de naissance différent saisie, paramètre Jour différent donne ambigu au lieu d\'inconnue n'est pas
sélectionné ni Jour et mois différents donne ambigu au lieu d'inconnue (paramètre par défaut), Résultat attendu :
inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 01/12/1977

### Cas 12

Cas 12 : Jour de naissance différent saisie, paramètre Jour différent donne ambigu au lieu d'inconnue est sélectionné ou
Jour et mois différents donne ambigu au lieu d\'inconnue (paramètre par défaut), Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 01/12/1977

### Cas 13

Cas 13 : Mois de naissance différent saisie, paramètre Mois différent donne ambigu au lieu d'inconnue n'est pas
sélectionné ni Jour et mois différents donne ambigu au lieu d'inconnue (paramètre par défaut), Résultat attendu :
inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/01/1977

### Cas 14

Cas 14 : Mois de naissance différent saisie, paramètre Mois différent donne ambigu au lieu d'inconnue est sélectionné ou
Jour et mois différents donne ambigu au lieu d'inconnue (paramètre par défaut), Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 06/01/1977

### Cas 15

Cas 15 : Date de naissance différente saisie, paramètre Date différente donne ambigu au lieu d'inconnue n'est pas
sélectionné, Résultat attendu : inconnue

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 01/01/1901

### Cas 16

Cas 16 : Date de naissance différente saisie, paramètre Date différente donne ambigu au lieu d'inconnue est sélectionné,
Résultat attendu : ambigu

- Date de décès : 10/02/2020
- Lieu de décès : VILLEFRANCHE
- Date de l'acte de décès : 12/02/2020
- Prénom : Aimé
- Nom d'usage : OLMO
- Date de naissance : 01/01/1901
