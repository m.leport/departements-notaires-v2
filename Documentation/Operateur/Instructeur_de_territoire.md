# Instructeur de territoire

## Activation

Pour activer la fonctionnalité *Instructeurs de territoires* :

1. Modifier le fichier `.env` et y ajouter `APP_INSTRUCTEUR_TERRITOIRE=1`
2. Redémarrer le serveur web

Pour vérifier que la fonctionnalité est bien activée :

1. Se connecter en tant qu'administrateur
2. Se rendre dans `Administration > Gérer les instructeurs`, identifier le titre `h2` *Instructeurs de territoire*.

## Limitation

:caution: Chaque ligne du fichier d'import doit
obligatoirement posséder les informations des Maisons de territoires (champs `mdt`, `mdt_telephone`, `mdt_courriel`,
`mdt_adresse` remplis).

La vérification de la présence de ces informations n'est **pas** faite par Départements &
Notaires, c'est au Conseil Départemental de le faire avant l'import des données.
