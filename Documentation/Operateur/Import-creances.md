# Import des créances

Sont décrites ici les étapes pour importer les données de créances dans l'application via un fichier CSV.

Un fichier CSV comprenant des données d'exemples est disponible : [creances.csv](../../appli_sf/imports/creances.csv)

L'import permet de remplir la table `creance` de l'application.

## Procédure d'import

Déposer le fichier à importer dans le dossier `imports` à la racine du projet.
Lancer la commande suivante (où "fichier.csv" correspond au nom du fichier à importer) :

```shell
bin/console import:creances "imports/fichier.csv"
```

Un import supprime entièrement les précédentes données importées. Il faut importer l'intégralité des données à
chaque nouvel import.

## Détail du fichier CSV

Si vous ne souhaitez pas transmettre une valeur, il faudra tout de même bien mettre les délimiteurs pour que l'import
ne saute pas de colonnes.

Fichier CSV au format suivant :

* Séparateur : `;`
* Délimitation des chaines de caractères : `"`
* Une ligne d'entête comme dans le fichier d'exemple
* Les doubles sont délimités par un `.` et non une `,`

Les différentes colonnes :

* `num_ind` : numéro individu issu de l'application tiers, propre à elle : `int(15)`
* `date_deb_periode` : date de début de la période de calcul du montant de la créance au format `jj/mm/aaaa` ou
  `aaaa-mm-jj`
* `date_fin_periode` : date de fin de la période de calcul du montant de la créance au format `jj/mm/aaaa` ou
  `aaaa-mm-jj`
* `montant` : montant de la créance calculé : `double(10,2)`
* `date_calcul` : date donnant le point de départ du recouvrement en tenant compte de la date de décès,
  des délais de prescriptions, des interruptions de droits au format `jj/mm/aaaa` ou `aaaa-mm-jj`
* `affichable` : condition permettant de tenir compte du fait que certaines créances imprécises ne peuvent être
  communiquées : `boolean` (1/0)

**IMPORTANT :**

L'import *d'individus* doit être effectué **avant** l'import de *créances*. En effet, le `num_ind` doit exister pour la
création d'une créance. En cas non-respect de cet ordre, l'erreur suivante est levée :

```injectablephp
argument 1 passed to App\Entity\Claim::addPerson() must be an instance of App\Entity\Person, null given
```


