# Migrer les comptes utilisateurs de la v1 vers la v2

L'import des comptes de la v1 se fait à partir d'un export CSV qui devra avoir la même structure que le fichier
d'exemple [appli_sf/imports/utilisateurs-v1.csv](../../appli_sf/imports/utilisateurs-v1.csv)

> :warning: **Tous les comptes utilisateurs déjà présents seront supprimés**

- Déposer le fichier à importer dans le dossier `imports` à la racine du projet.
- Se positionner à la racine de l'appli
- Lancer la commande (en remplaçant "imports/utilisateurs-v1.csv" avec le chemin vers le fichier d'export de la v1) :

  ```shell
  bin/console import:utilisateurs-v1 imports/utilisateurs-v1.csv
  ```

Une fois les comptes de la v1 importés les utilisateurs devront obligatoirement faire une demande de réinitialisation
de mot de passe en cliquant sur "J'ai oublié mon mot de passe" sur la page de login.
