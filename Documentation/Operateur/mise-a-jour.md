# Montée de version de Départements & Notaires

## Procédure de montée de version

Sont décrites ici les étapes pour effectuer une mise à jour de l'application

### Effacer les fichiers générés (sauf les médias et ceux pour la personnalisation) dans le dossier de l'application

```shell
    rm -rf app/assets
    rm -rf app/config
    rm -rf app/public/build
    rm -rf app/src
    rm -rf app/templates/default
    rm -rf app/tests
    rm -rf app/translations/messages.fr.yaml
    rm -rf app/vendor
```

### Récupérer la dernière version

* Télécharger la dernière
  version [Départements & Notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/packages)
* Dézipper le fichier dans le dossier de l'application
* Remettre les bons droits si besoin (utilisateur et groupe utilisé par le serveur web sur tout ce qu'il y a dans
  le dossier de l'application)

Attention : Si vous utilisez un fichier de CSS personnalisé (`app/public/styles.css`), pensez à le récupérer AVANT de
dézipper le fichier dans le dossier de l'application. Vous pourrez ensuite copier votre fichier en lieu et place du
fichier vide contenu dans l'archive.

### Vider les caches

```shell
./bin/console cache:clear
```

Si le message *Unable to write in the "cache" directory* s'affiche, lancer la commande avec l'utilisateur utilisé
par le serveur web, par exemple :

```shell
sudo -u www-data ./bin/console cache:clear
```

### Mettre à jour la base de données

```shell
./bin/console doctrine:migrations:migrate --allow-no-migration
```

### Mise à jour des modèles (`templates/*`)

Sauf indication contraire, les modèles personnalisés fonctionnent toujours après une montée de version.
Cependant, afin d'éviter une dette technique locale, il est **recommandé de mettre à jour les
modèle personnalisés** à chaque nouvelle version.

La méthodologie proposée est la suivante :

1. Archiver le modèle personnalisé (`templates/mon-departement-a-moi`)
2. Une fois la montée de version effectuée, copier le modèle par défaut (`templates/default`) dans le modèle
   personnalisé
   (`templates/mon-departement-a-moi`)
3. Reporter les personnalisations dans `templates/mon-departement-a-moi`

## Version 2.6.0 *breaking changes*

* PHP 8.1 (au moins) est désormais requis. Les versions antérieures de PHP ne sont plus supportées.
* Les fichiers de modèle suivants ont été modifiés :

  ```shell
  appli_sf/templates/default/admin/settings.html.twig
  appli_sf/templates/default/emails/search_found.html.twig
  appli_sf/templates/default/search/logs.html.twig
  appli_sf/templates/default/security/login.html.twig
  appli_sf/templates/default/admin/instructor/list.html.twig
  appli_sf/templates/default/admin/user/list.html.twig
  appli_sf/templates/default/base.html.twi
  appli_sf/templates/default/stats/pdf.html.twig
  appli_sf/templates/default/stats/stats.html.twig
  ```

## Version 2.5.1 *breaking changes*

Pas de changement cassant pour la version 2.5.1

## Version 2.5.0 *breaking changes*

* Conformément aux
  [recommandations de la CNIL](https://lincnil.github.io/Guide-RGPD-du-developpeur/#Fluidifier_la_gestion_des_profils_d%E2%80%99habilitation),
  Les profils administrateurs n'ont désormais plus accès aux fonctionnalités métiers, mais uniquement aux
  fonctionnalités d'administration.

## Version 2.4.0 *breaking changes*

### Version 2.4.0 - Modèles modifiés

Les fichiers de modèle suivants ont été modifiés :

```shell
appli_sf/templates/default/emails/search_found.html.twig
appli_sf/templates/default/pdf/base.html.twig
```

### Version 2.4.0 - Base de données

Plusieurs champs sont renommés :

| Avant       | Après           |
|-------------|-----------------|
| `mdr`       | `mdt`           |
| `telephone` | `mdt_telephone` |
| `mail_mdr`  | `mdt_courriel`  |

Un champ est ajouté : `mdt_adresse`

### Version 2.4.0 - Consistence des données à l'import

:caution: Si la fonctionnalité Instructeurs de Territoires et activée, chaque ligne du fichier d'import doit
obligatoirement posséder les informations des Maisons de territoires (champs `mdt`, `mdt_telephone`, `mdt_courriel`,
`mdt_adresse` remplis). La vérification de la présence de ces informations n'est **pas** faite par Départements &
Notaires, c'est au Conseil Départemental de le faire avant l'import des données.

## Version 2.3.2 *breaking changes*

Les fichiers de modèle suivants ont été modifiés :

```shell
appli_sf/templates/default/account/change_password.html.twig
appli_sf/templates/default/account/infos.html.twig
appli_sf/templates/default/admin/index.html.twig
appli_sf/templates/default/admin/instructor/add.html.twig
appli_sf/templates/default/admin/instructor/edit.html.twig
appli_sf/templates/default/admin/instructor/list.html.twig
appli_sf/templates/default/admin/logs.html.twig
appli_sf/templates/default/admin/settings.html.twig
appli_sf/templates/default/admin/user/add.html.twig
appli_sf/templates/default/admin/user/edit.html.twig
appli_sf/templates/default/admin/user/list.html.twig
appli_sf/templates/default/base.html.twig
appli_sf/templates/default/emails/search_found.html.twig
appli_sf/templates/default/help.html.twig
appli_sf/templates/default/pdf/search_found_notrec.html.twig
appli_sf/templates/default/pdf/search_found_rec.html.twig
appli_sf/templates/default/pdf/search_notfound.html.twig
appli_sf/templates/default/search/logs.html.twig
appli_sf/templates/default/search/result.html.twig
appli_sf/templates/default/search/search.html.twig
appli_sf/templates/default/security/forgot_password.html.twig
appli_sf/templates/default/security/init_password.html.twig
appli_sf/templates/default/security/login.html.twig
appli_sf/templates/default/security/reset_password.html.twig
appli_sf/templates/default/security/unlock_user.html.twig
appli_sf/templates/default/stats/stats.html.twig
appli_sf/templates/default/terms_of_use.html.twig
appli_sf/templates/default/utils/header.html.twig
appli_sf/templates/default/utils/not_found.html.twig
```

## Version 2.2.0 *breaking changes*

:caution: **La version 2.2.0 requiert une mise à jour des modèles.**

* Les fichiers de modèle suivants ont été renommés :
    - `search_find.html.twig` => `search_found.html.twig`
    - `search_notfind.html.twig` => `search_notfound.html.twig`,
    - `search_find_rec.html.twig` => `search_found_rec.html.twig`
    - `search_find_notrec.html.twig` => `search_found_notrec.html.twig`
    - Ce changement est aussi valable sur toutes les variables et constantes y faisant référence.
* Pour plus de simplicité quant au traitement des différents statuts d'aides, et plus de clarté quant au nommage des
  différents modèles de mail, le modèle `search_hasrequest.html.twig` a été fusionné avec `search_found.html.twig`.
  Le traitement particulier des demandes en *cours d'instruction* n'a plus de sens d'être séparé des autres demandes.
  De ce fait, les personnes *en cours d'instruction* sont désormais reconnues comme **connues**.
* Le champ `num_ind` de la table `individu` **n'est plus** considéré comme **unique**, et ne comporte plus d'index
  unique à son nom. Ce changement implique qu'il est désormais possible de stocker plusieurs lignes par individu, et
  donc plusieurs `code_aide`.
* Le champ `type_reponse` dans la table `log_recherche` a **changé de type**. En effet, afin de permettre la gestion
  des aides multiples, il est nécessaire que nous puissions stocker toutes les valeurs de types de réponses associées
  à une recherche. Le champ `type_reponse` n'est donc plus un `int` mais un type `json`. Il aura le format
  `[1,1,2,5]` ou `1,1,2,5` représente un code d'aide associé à la recherche. Pour permettre la migration des anciennes
  valeurs stockées dans la table `log_recherche` au format `int` un `UPDATE SET` a été fait dans la migration relative
  à ce développement pour permettre la transformation. Toutes les valeurs seront alors transformées (ex: `1` => `[1]`).

