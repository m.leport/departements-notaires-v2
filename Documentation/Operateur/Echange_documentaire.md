# Échange documentaire

## Remarque technique de la fonctionnalité

* Seuls les fichiers de type PDF, JPEG, GIF, PNG sont acceptés.
* Les fichiers téléversés doivent être conservés chiffrés sur le serveur.
* Le nom de fichier téléversé se voit ajouté une chaîne de texte aléatoire, afin de rendre son nom imprédictible.
* Les fichiers sont stockés dans le dossier `private/documents/`.

## Clé de chiffrement

Il ne reste plus qu'à configurer l'application pour lui donner la clé utilisée pour le chiffrement.
Pour cela, il faut modifier le fichier `.env` à la racine de l'application.
Il suffit de définir une valeur de la variable `APP_SECRET` si elle n'en a pas déjà une, par
exemple `APP_SECRET=xdRTad6qs324`.
Cette clé sera utilisée pour chiffrer les fichiers téléversés sur le serveur et permettre leur lecture par
l'application.

## Activation de la fonctionnalité

* Dans Paramètres de l'application, se rendre à la section **Paramètres de l'échange documentaire**.
* La case à cocher **Échange documentaire** permet d'activer ou non la fonctionnalité.
* Dessous, se trouve un paramètre obligatoire intitulé "Réponses appelant document". Il propose deux cases à cocher
  listant les types de réponses pour lesquelles le notaire se verra proposé d'envoyer des documents. Au moins une case
  doit être cochée sur les deux.
  Ces deux cases à cocher ont pour intitulé :
    * Réponse connue
    * Réponse ambiguë

## Exemple 1

1. En tant qu'administrateur, je me connecte à l'application.
2. Je vais dans Administration > Paramètres de l'application, section Paramètres de l'échange documentaire.
3. À l'entrée "Échange documentaire", je coche la case à cocher.
4. À l'entrée suivante "Réponses appelant document", je coche "Réponse connue".
5. Je clique sur le bouton Sauvegarder.

### Exemple 1 Test d'acceptation

Les informations de recherche sont présentes dans l'import de test.

1. En tant que notaire, je lance une recherche :
    * Cocher la case "possession de l'acte de décès".
    * Date décès : 16/06/2018.
    * Lieu de décès : Villeurbanne.
    * Date de l'acte de décès : 16/06/2018.
    * Prénom : Aimé.
    * Nom d'usage : Olmo.
    * Date de naissance : 06/12/1977.
2. La réponse est connue.
3. Le formulaire permettant d'envoyer des fichiers est présent.

## Exemple 2

1. En tant qu'administrateur, je me connecte à l'application.
2. Je vais dans Administration > Paramètres de l'application, section Paramètres de l'échange documentaire.
3. À l'entrée "Échange documentaire", je coche la case à cocher.
4. À l'entrée suivante "Réponses appelant document", je coche "Réponse ambiguë".
5. Je clique sur le bouton Sauvegarder.

### Exemple 2 Test d'acceptation

Les informations de recherche sont présentes dans l'import de test.

1. En tant que notaire, je lance une recherche :
    * Cocher la case "possession de l'acte de décès".
    * Date décès : 16/06/2018.
    * Lieu de décès : Villeurbanne.
    * Date de l'acte de décès : 16/06/2018.
    * Prénom : Clara.
    * Nom d'usage : BOURDIAU.
    * Date de naissance : 01/09/1966.
2. La réponse est ambiguë.
3. Le formulaire permettant d'envoyer des fichiers est présent.

## Conservation des fichiers téléversés

* La durée de conservation des fichiers est définie dans le fichier .env, grâce à la
  variable `APP_ECHANGE_DOCUMENTAIRE_EXPIRATION`.
* La définition de la durée s'appuie sur la
  classe [DateInterval](https://www.php.net/manual/en/dateinterval.createfromdatestring.php).
* La durée par défaut de conservation des fichiers est fixée à 18
  mois (`APP_ECHANGE_DOCUMENTAIRE_EXPIRATION="18 months"`).
* La mise en place d'une tâche cron sur vos serveurs doit être effectuée pour déclencher la commande liée à la durée de
  conservation.

```shell
0 * * * * /path/to/php /path/to/bin/console app:document:cleanup
```

Il faudra remplacer `/path/to/php` et `/path/to/bin/console` par les chemins spécifiques à votre installation.

### Exemple

```shell
0 * * * * /usr/bin/php /var/www/notaire/bin/console app:document:cleanup
```

Dans l'exemple, une exécution de la commande est effectuée toutes les heures. Vous pouvez adapter cette fréquence selon
vos besoins.

La commande peut aussi être lancée manuellement via :

```shell
./bin/console app:document:cleanup
```


