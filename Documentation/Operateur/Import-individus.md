# Import des données CSV de test

Sont décrites ici les étapes pour importer des données dans l'application via un fichier CSV.

Un fichier CSV comprenant des données d'exemples est disponible : [individus.csv](../../appli_sf/imports/individus.csv)

L'import permet de remplir la table `individu` de l'application (sur laquelle est effectuée la recherche), cette table
regroupe les demandes suivantes :

- Tous les individus bénéficiant, ou ayant bénéficié, de prestations d'ASG (Aide Social Générale),
  d'ADPA (Allocation Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap)
- Tous les individus dont la ou les demandes d'ASG (Aide Social Générale), d'ADPA (Allocation
  Départementale Personnalisée d'Autonomie) ou de PCH (Prestation de Compensation d'Handicap) sont en cours
  d'instruction.

Pour information, par rapport à la V1, cette table est la fusion des tables `demandes` et `individus`.

## Procédure d'import

Déposer le fichier à importer dans le dossier `imports` à la racine du projet.
Lancer la commande suivante (où "fichier.csv" correspond au nom du fichier à importer) :

```shell
bin/console import:individus "imports/fichier.csv"
```

Un import supprime entièrement les précédentes données importées. Il faut importer l'intégralité des données à chaque
nouvel import.

En production, il est possible que vous disposiez d'un export IODAS très volumineux (>100'000lignes). Si le temps de
traitement ci-dessus est trop long, il est possible d'importer directement via MySQL :

Copier cette requête dans un fichier .sql :

```sql
TRUNCATE individu;

LOAD DATA LOCAL INFILE 'chemin_complet_du_fichier.csv'
INTO TABLE individu
FIELDS TERMINATED BY ';'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@col1,@col2,@col3,@col4,@col5,@col6,@col7,@col8,@col9,@col10,@col11,@col12,@col13,@col14,@col15)
set num_ind=@col1, sexe=@col2, nom_usage=@col3, nom_civil=@col4, prenom=@col5, prenomd=@col6, prenomt=@col7, date_naissance=STR_TO_DATE(@col8, '%Y-%m-%d'), adresse=@col9, mdr=@col10, telephone=@col11, mail_mdr=@col12, libelle=@col13, code_aide=@col14, date_deces=STR_TO_DATE(@col15, '%Y-%m-%d');
```

Puis lancer la commande :

```shell
mysql -h IP_DU_SERVEUR -u UTILISATEUR_MYSQL -p BASE_DE_DONNEE < chemin_du_fichier.sql
```

## Détail du fichier CSV

Fichier CSV au format suivant :

* Séparateur : `;`
* Délimitation des chaines de caractères : `"`

Les différentes colonnes :

* `num_ind` : numéro individu issu de l'application tiers, propre à elle : int(15)
* `sexe` : sexe de la personne (F ou M) : varchar(1)
* `nom_usage` : nom d'usage (marital) de la personne : varchar(250)
* `nom_civil` : nom de naissance de la personne : varchar(250)
* `prenom` : premier prénom de la personne : varchar(250)
* `prenomd` : second prénom de la personne : varchar(250)
* `prenomt` : troisième prénom de la personne : varchar(250)
* `date_naissance` : date de naissance de la personne au format jj/mm/aaaa ou aaaa-mm-jj
* `adresse` : adresse du référent administratif du dossier de la personne : varchar(250)
* `mdr` : nom du référent administratif du dossier de la personne : varchar(250)
* `telephone` : téléphone du référent administratif : varchar(250)
* `mail_mdr` : mail du référent administratif : varchar(250)
* `libelle` : aide accordée à la personne (exemple : Charges exceptionnelles (mensuel), Fauteuil roulant (FDCH), Aides
  techniques (FDCH), ADPA à domicile, Aide ménagère couple PH, etc) : varchar(250)
* `code_aide` : code indiquant si l'aide est récupérable ou non (1SEXTREC : recupérable, 1SEXTNONR : non récupérable) :
  varchar(50)
* `date_deces` : date de décès de la personne au format jj/mm/aaaa ou aaaa-mm-jj

Important : Il faut bien laisser la première ligne d'entête comme dans le fichier d'exemple.
