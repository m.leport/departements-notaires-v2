# Emails

## Personnaliser le sujet d'un mail

De la même manière qu'on personnalise le contenu d'un courriel ou d'un PDF on peut désormais en personnaliser le sujet.
Il sera à placer dans le fichier `subject.html.twig`, se trouvant dans le même dossier que les templates de mails dans
le dossier `emails`.

> :warning: **Le sujet du mail n'accepte pas de balise HTML**

Sont mis hors périmètres les mails suivants :

* alerte décès
* add_user
* reset_password
* unlock_user

Les balises de modèles applicables dans le template de sujet sont :

* `userName` : le nom du notaire
* `useName` : le nom d'usage de la personne recherchée
* `civilName` : le nom civil de la personne recherchée
* `firstName` : le prénom de la personne recherchée
* `middleName` : le deuxième prénom de la personne recherchée
* `thirdName` : le troisième prénom de la personne recherchée
* `birthDate` : le date de naissance de la personne recherchée
* `deathDate` : le date de décès de la personne recherchée
* `deathLocation` : le lieu de décès de la personne recherchée
* `status` : le status de la personne recherchée (les valeurs possibles seront : `connu`, `inconnu`, `ambigu`
  , `en cours d'instruction`)
* `appName` : le nom de l'application rempli dans les paramètres
* `recipient` : le nom de l'application rempli dans les paramètres

Par défaut, si le template de sujet est vide, le sujet de mail ressemblera à :

`[appName] userName - Succession firstName useName - status`

Une fois la modification effectuée dans le template, il est nécessaire de vider les caches de l'application pour la voir
apparaître sur le site en utilisant la commande :

> ./bin/console cache:clear
