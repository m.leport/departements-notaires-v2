# Architecture technique

## Composants logiciels

L'application Départements & Notaires est composée des blocs suivants :

- une base de données (MariaDB)
- une application web (PHP Symfony)

## Architecture

La base de données et l'application web sont souvent installées sur la même machine pour des questions de simplicité.
Cette dernière se situant dans une zone démilitarisée (DMZ).

Il est possible de placer l'application web sur une machine en DMZ, et la base de données sur une autre machine, sur un
réseau interne.

## Alimentation en données métier (import SQL)

L'import des données métier (provenant d'un outil comme Solis ou Iodas) se fait toutes les nuits.

Cet import est décrit dans la
documentation [import des données](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Import-individus.md)
. Le principe est de vider la table `individu` et de la re-remplir chaque nuit.

L'extraction des données de Iodas, Solis ou autre outil métier, est hors périmètre de Départements & Notaires. Il
revient à chaque conseil départemental de produire son extraction (même avec un même outil, chaque département a sa
propre configuration).

## Flux réseau

| Hôte source   | Hôte Destination | Port Destination | Détail                                     |
|:--------------|:-----------------|:-----------------|:-------------------------------------------|
| Usager        | D&N appli web    | 80  / 443        | Application Départements & Notaire         |
| D&N appli web | D&N MariaDB      | 3306             | Base de données Départements & Notaires    |
| D&N appli web | Serveur SMTP     | 25 / 465         | Envoi de mails par Départements & Notaires |
