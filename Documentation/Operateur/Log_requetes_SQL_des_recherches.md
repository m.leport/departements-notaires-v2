# Log des requêtes SQL des recherches

Afin de faciliter les choix de configuration du moteur de recherche, les requêtes SQL des recherches d'individus
sont consignées dans des fichiers journaux ("logs").

Le log des requêtes SQL des recherche se trouve dans `var/log/sql-YYYY-MM-DD.sql` (`YYYY-MM-DD` correspondant à la
date du jour).

Le framework Symfony applique une rotation des logs, qui est configurée dans
`appli_sf/config/packages/prod/monolog.yaml`
