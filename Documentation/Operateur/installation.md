# Installation de l'application

Sont décrites ici les étapes pour configurer et tester rapidement l'application.

## Pré-requis matériels

* CPU : 2 cœurs
* RAM : 4Go

## Pré-requis logiciels

* Apache 2.4 au moins. Modules requis :
    * `proxy` (Si utilisation de PHP-FPM)
    * `proxy_fcgi` (Si utilisation de PHP-FPM)
    * `setenvif`
    * `rewrite`
* PHP 8.1.* Extensions requises :
    * `xml`
    * `mysqli`
    * `pdo`
    * `gd`
    * `intl`
    * `fileinfo`
* MariaDB 10.1 au moins.

## Configuration du Virtualhost Apache

Créer un Virtualhost Apache. Le `DocumentRoot` doit pointer vers le sous dossier `public`.

Exemple : Dossier pour le Virtualhost `/var/www/notaires`, configurer le serveur web
avec `DocumentRoot /var/www/notaires/public`

Pour une configuration avancée vous pouvez vous référer à
la [documentation de Symfony](https://symfony.com/doc/current/setup/web_server_configuration.html)

## Récupérer l'application

* Télécharger
  [Départements & Notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/packages)
* Dézipper le fichier dans le sous-dossier `/var/www/notaires` du serveur Apache

## Base de données

Créer une base de données et un utilisateur associé sur Mariadb. Exemple :

* Database : `notaires`
* Username : `notaires`
* Password : `password`

## Configurer

Créer le fichier `.env` à la racine du projet (à partir du fichier `.env.dist`).

Il est **nécessaire** d'adapter les variables suivantes :

* `APP_ENV` : type de l'environnement (dev ou prod)
* `APP_DEBUG` : affiche ou non à l'écran les messages d'erreurs de l'application (0 ou 1)
* `DATABASE_URL` : informations de connexion à la base de données
* `MAILER_DSN` : informations de connexion au smtp pour les emails, exemple `smtp://localhost:25`
* `MAILER_SENDER` : adresse mail de l'expéditeur des mails, exemple `ne-pas-repondre@depnot.departement.fr`

Il est **possible** d'adapter les variables suivantes :

* `APP_THEME` : nom du dossier où sont stockés les templates de surcharge dans le dossier templates ('default
  ' contient les templates d'origine)
* `APP_SECRET` : chaine de caractères aléatoire servant pour la sécurisation et les fonctions cryptographiques
* `TRUSTED_PROXIES` : liste de *reverse proxy* pouvant être devant l'application (séparé par des `,`)
* `TRUSTED_HOSTS` : regex permettant de filtrer le(s) nom(s) de domaine ayant accès à l'application : '^example\.com$'

## Installation des dépendances

Donner les bons droits sur les fichiers. Se placer dans le répertoire de l'appli et exécuter les commandes suivantes (
remplacer www-data par l'utilisateur et le groupe apache si différent) :

```shell
chown -R www-data:www-data .
chmod +x bin/*
```

Dossiers ayant besoin des droits d'écriture (ainsi que tous leurs sous-dossiers) :

* `public/uploads/` - images téléchargées sur le serveur
* `public/media/` - images redimensionnées par l'appli
* `var/` - cache et logs de l'appli

Lancer l'installation de la base de données et l'initialisation des classes

```shell
./bin/console doctrine:migrations:migrate
```

Créer l'utilisateur administrateur

```shell
./bin/console user:add
```

## Se connecter à l'application avec l'utilisateur créé

Ouvrir dans un navigateur l'URL configurée sur votre serveur Apache.
