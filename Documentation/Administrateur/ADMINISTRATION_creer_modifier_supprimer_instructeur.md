# Création, modification et suppression d'un instructeur

## Création d’un instructeur

La création d’un instructeur est permise uniquement par les administrateurs.

Pour y accéder, il faut se rendre dans `Administration` > `gérer les instructeurs`.

1. Cliquer sur le bouton Ajouter un instructeur
2. Liste des valeurs à renseigner :
    * Prénom de l'instructeur
    * Nom de l'instructeur
    * Courriel
    * Téléphone
    * Type de réponse
    * Initiales

La valeur **Type de réponse** permet d'envoyer à l'instructeur uniquement les aides spécifiquement choisies :

* Récupération (aide récupérable)
* Indus probable
* Ambigu (identité à vérifier)

Elles sont définies dans le lexique de l'application.

Les **initiales** permettent de faire une répartition de la charge des dossiers selon les initiales du nom d'usage ou
civil des individus. De ce fait, il est possible de créer un instructeur qui, par exemple, gère des dossiers
de A à L et un autre, de N à Z.

Les **intitulé code d'aide** sont une liste de codes provenant de toutes les aides présentes dans la base de données.
Ils permettent de répartir la charge des dossiers en fonction de la concordance du libellé d'aide avec une personne
connue ayant ce libellé dans ses aides.

## Exemples

### Création d’un instructeur qui traitera tout type de réponse et tout type de dossiers

![instructeur1](../Operateur/images/instructeur1.png)

L’agent Jean Durand sera instructeur sur l’application. Il sera notifié de toutes les réponses possibles aux recherches
des notaires. Il gère des dossiers des individus de A à Z.

### Création d’un instructeur qui traitera les réponses ambigu (entre A et L)

des dossiers des individus ayant leur nom de famille compris entre A et L

![instructeur2](../Operateur/images/instructeur2.png)

L’agent Jeanne Dupré sera notifiée uniquement des cas *ambigus* rencontrés dans les recherches pour les noms d’usages
ou civils compris entre A et L. (les dossiers de M à Z sont donc exclus, de même que les réponses *récupération* et
*Indus probable*).

### Création d’un instructeur spécifique

Exemple : Création d’un instructeur qui traitera les réponses ambigu et indu probables dossiers des individus ayant
leur nom de famille compris entre M et Z.

![instructeur3](../Operateur/images/instructeur3.png)

L’agent Pierre Delaplace sera notifié des cas **ambigus** et **Indus probables** rencontrés dans les recherches
pour les nom d’usages ou civils compris entre M et Z.(les dossiers de A à L sont donc exclus, de même que
les réponses *récupération*).

résultats des instructeurs créés

![liste](../Operateur/images/liste_instructeurs.png)

## Modifier un instructeur

1. Se rendre dans `Administration` > `Gérer les instructeurs`
2. Cliquer sur modifier l’utilisateur icône ![modifier](../Operateur/images/icone_modifier.png)
3. Modifier le champ souhaité
4. Valider

## Supprimer un instructeur

1. Se rendre dans `Administration` > `Gérer les instructeurs`
2. Cliquer sur supprimer l’utilisateur icône ![supprimer](../Operateur/images/icone_supprimer.png)
3. Valider l'opération via la fenêtre flottante.
