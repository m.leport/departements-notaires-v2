# Interface ADMINISTRATION – Paramètres de l’application

Descriptif des fonctionnalités présentes dans les paramètres.

Pour accéder au module, se connecter en tant qu’admin.

## Paramètres du site

### Nom de l’application

Possibilité de renommer l’application (utilisé dans l’application et dans les envois de mails automatisés)

Exemples :

- *Département et Notaires numéro de département*
- *Département & Notaires numéro de département*
- *nom du département + mail Notaires…*

### URL de l’application

L'application envoie une URL permettant de modifier le mot de passe via l'e-mail.
Cette URL est générée en utilisant l'adresse du site configurée sur le serveur.
Cependant, un problème se pose lorsque le site est configuré sur une adresse interne,
mais que les notaires n'ont accès qu'à une URL externe, rendant l'URL fournie aux notaires inaccessible.

Ce champ permet aux administrateurs de saisir l'URL d'accès ajouté au mail pour réinitialiser leur mot de passe.
Si cette URL n'est pas renseignée,
l'application continuera à fonctionner comme précédemment en utilisant l'adresse du site configurée sur le serveur.

### Logo de l’application

Possibilité de customiser le logo du département utilisateur qui sera visualisable :

1. sur la page avant connexion (en bas à droite)
2. dans le menu une fois connecté (en haut à droite de l’écran)

N.B. : il faudra valider l’ensemble de la page paramètres pour que le(s) logo(s) soi(en)t complètement chargé(s)

### Favicon du site

Possibilité de customiser avec un favicon personnalisé

### Nom du Département

Paramétrage du nom du département afin de récupérer automatiquement celui-ci pour l’application et mails système

### Site Web du Département

Paramétrage du nom du département afin de récupérer automatiquement celui-ci pour l’application et mails système

### Site CIL du Département

Paramétrage du site du Correspondant informatique et libertés (CIL) du Département afin de récupérer automatiquement
celui-ci pour l’application

### Nombre de tentatives de connexion avant blocage

Permet de configurer le nombre de tentatives qu’un utilisateur peut faire avant que le compte soit bloqué par
sécurité. (valeur numérique attendue)

N.B. : la fonction **mot de passe oublié** existe dans l’application.

## Paramètres de recherche

### Recherche phonétique soundex

Voir documentation [**Algorithme_de_recherche.md**] (../Utilisateur/Algorithme_de_recherche.md)

### Précision de la date de naissance

Permet de sélectionner le niveau de précision de recherche sur la date de naissance. Selon le mode choisi la réponse
donnera ambiguë ou inconnue.

Valeur par défaut : `Jour et mois différents donnent ambigu au lieu d'inconnu`

### Prénom différent donne ambigu

Dans le cas de figure où le nom saisi est différent de celui présent dans la base de données. Le résultat attendu
sera **ambigu** si la case est cochée.

Valeur par défaut : décoché

### Recherche par nom

Configuration pour spécifier si la recherche se fait par défaut sur le nom d’usage ou sur le nom d’état civil
Les deux champs peuvent être renseigné dans la recherche par la suite par l'utilisateur,
mais une priorisation se fera sur l'option choisie pour fournir le résultat.

## Paramètres de l'échange documentaire

### Échange documentaire

Active la fonctionnalité pour permettre l'échange documentaire entre le Département et les Notaires

### Réponses appellant document

Sélectionne quel type de réponse met à disposition le formulaire de dépôt de documents aux notaires.

### Alerte document

Permet la désactivation des notifications par mail d'un dépôt de document par un notaire aux instructeurs.

## Alertes Décès

Configuration ou non de l’alerte décès (+ paramétrage de l’adresse mail de destination des alertes). Une fois ces deux
paramètres configurés, une alerte est envoyée par mail sous deux conditions en plus, que :

* la personne recherchée existe dans la base de données
* la date de décès recherchée soit différente de la date de décès enregistrée en base de données.

La non saisie d'une date de décès lors de la recherche alors qu'il existe une valeur en base de données pour l'individu
recherché, ainsi que la saisie d'une date de décès alors que l'individu n'en comporte pas en base de données, sont deux
cas qui rentrent dans le cas de la deuxième condition et permettent l'envoi d'une alerte décès.

## Paramètres des mails système

### Signature

Ajout d’une signature personnalisée dans les messages mails du système

### Introduction

Ajout d’une introduction personnalisée dans les messages mails du système

### Nota Bene

Ajout d’un Nota Bene personnalisé dans les messages mails du système

## Paramètres des mails métier

### Signature métier

Ajout d’une signature personnalisée dans les messages mails des mails métiers

### Signature numérisé

Ajout d’une signature numérisé à la suite du texte automatique

## Paramètres des lettres PDF

Permet de personnaliser l’en-tête et le pied de page de la lettre PDF

En-tête :

1. **Ville destinataire** : Saisie texte du lieu d’en-tête du courrier PDF
2. **Logo en-tête** : ajout d’un fichier image
3. **Maison départementale** : Saisie texte interlocuteur
4. **Tel. Maison départementale** : Numéro de téléphone de l’interlocuteur
5. **Mail successions** : adresse mail due l’interlocuteur du suivi des successions

Pied de page :

1. **signature numérisé** : ajout d’une signature personnalisée à la suite du texte automatique
2. **Image en pied de page** : (même format que l’image d’en tête)
3. **logo service** : ajout d’un logo du service dans le pied de page

### Fichiers CSS personnalisé

Faire appel à un modèle à charger sur l’application. (le fichier css actif est également consultable depuis
l’administration de l’appli)

## Paramètres Instructeurs

Faire appel à un modèle à charger sur l’application. (le fichier css actif est également consultable depuis
l’administration de l’appli)

### Affinage libellé d'aide

Si le paramètre est activé et que des filtres libellés d'aide sont définis dans les différents instructeurs,
seuls ceux qui partagent un libellé d'aide avec une recherche d'une personne connue ayant des aides similaires recevront
le mail instructeur.
