# Destinataires des mails de l'application

## Vocabulaire

* courriel : adresse *email*, exemple `jean.dupont@mon-departement.fr`

## Depuis Administration > Paramètres de l'application

### Alerte décès

L'alerte décès a pour objet d'informer le Conseil Départemental lorsqu'une étude notariale fait une recherche sur un
bénéficiaire, bénéficiaire qui n'a pas de date de décès renseigné en base. Cela pourrait signifier que le bénéficiaire
est décédé, et donc remonter cette information au Conseil Départemental.

Dans ce cas, et si la fonctionnalité "alerte décès" est activée, alors un message est envoyé
au courriel `Mail de destination des alertes décès`.

### Paramètre des créances

Si la fonctionnalité d'estimation de la créance est activée, deux courriels sont utilisés :

* `Mail du service des créances` : courriel pour contacter le service gérant toutes les questions relatives aux créances
  (calcul du montant, qualité des héritiers, etc).
* `Mail du service de gestion` : courriel pour contacter le service traitant la recherche de la personne bénéficiaire
  (et non ses créances), par exemple lors d'une ambiguïté sur la date de décès de la personne.

### Paramètres des mails système

Les champs `Signature`, `Introduction` et `Nota Bene` peuvent être utilisés dans le corps des messages
nécessaires au fonctionnement même de l'application (et indépendamment du métier) ; exemples : création de compte,
mot de passe perdu, ré-activation d'un compte bloqué.

### Paramètres des mails métier

Le champ `Signature métier` peut être utilisé pour personnaliser la signature des messages métiers ; exemples :
recherche d'une personne connue, recherche d'une personne inconnue ou résultat de recherche ambigu.

### Paramètres des lettres PDF

Le champ `Mail successions` est le courriel de contact affiché dans les PDF.

## Destinataires des mails de résultat de recherche

### La personne est connue

Le message de type personne connue, comprenant les différents types d'aides (récupérables, non récupérables,
en cours d'instruction), est envoyé à :

- l'étude notariale à l'origine de la demande (courriel de l'utilisateur étude)
- l'instructeur qui a la charge de ce type de dossier

### La personne est inconnue

Le message de type personne inconnue est envoyé à :

- l'étude notariale à l'origine de la demande (courriel de l'utilisateur étude)

### La recherche amène à un résultat ambigu

Le message de type "réponse ambigüe" est envoyé à :

- l'étude notariale à l'origine de la demande (courriel de l'utilisateur étude)
- l'instructeur qui a la charge de ce type de dossier

## Courriel émetteur des messages

Le courriel utilisé pour l'émission des messages de Départements et Notaires est configuré dans le fichier `.env`
à l'entrée `MAILER_SENDER`.
