# Profil d'un utilisateur Notaire

## Création d'un utilisateur notaire

La création d’un profil notaire est permise uniquement par les administrateurs.

Pour y accéder, il faut se rendre dans `Administration` > `gérer les utilisateurs`.

1. Cliquer sur le bouton **Ajouter un utilisateur**
2. Les données à remplir sont les suivantes :
    * **Profil** : Etude notariale
    * **Code CRPCEN** : Code d'identification unique de l'étude notariale
    * **E-mail** : email de l'étude ou du gestionnaire de l'étude
    * **Description** : Nom de l'étude
    * les coordonnées de l'étude : **Adresse** / **Complément d'adresse** / **Code postal** / **Ville**

![creation](../Operateur/images/creation_utilisateur_notaire.png)

### Remarque

Depuis la version 2.6.0, il est possible de créer plusieurs études notariales utilisant le même courriel.

### EXEMPLE

Je souhaite créer l'étude de Maitre Dupont à Montpellier

### Etape 1 : Je saisis les informations

* **Profil** : étude notariale
* **Code CRPCEN** : 20
* **E-mail** : <etude.dupont@notaire.fr>
* **Description** : étude de Maitre Dupont
* **Adresse** : Place de la République
* **Complément d'adresse** : 2° étage
* **Code postal** : 34000
* **Ville** : Montpellier

### Etape 2 : Je valide

Le tableau des utilisateurs doit à présent comporter la ligne précédemment validée.

![affichage menu](../Operateur/images/menu_liste_utilisateurs.png)

![affichage notaire](../Operateur/images/etude_dupont_creation.png)

N.B. : le compte doit être actif.

### Etape 3 : Envoi des informations

Une fois la validation effectuée, un mail est automatiquement envoyé à l'adresse mail de l'étude qui a été
préalablement renseignée.
Le notaire pourra dès lors créer son mot de passe et faire sa première connexion.

Exemple de mail :

![affichage notaire](../Operateur/images/mail_confirmation_creation_notaire.png)

Note : les informations encadrées peuvent être personnalisées dans la configuration de l'application.

## Modification d’un profil notaire

1. Se rendre dans `Administration` > `Gérer les utilisateurs`
2. Cliquer sur modifier l’utilisateur icône ![modifier](../Operateur/images/icone_modifier.png)
3. Modifier le champ
4. Valider

![edition](../Operateur/images/edition_utilisateur_notaire.png)

## Réinitialiser un mot de passe d’un profil notaire

1. Se rendre dans `Administration` > `Gérer les utilisateurs`
2. Cliquer sur modifier l’utilisateur icône ![reinitialiser](../Operateur/images/icone_reinitialiser.png)

## Désactiver un utilisateur

1. Se rendre dans `Administration` > `Gérer les utilisateurs`
2. Cliquer sur désactiver dans la colonne Stats

![desactiver](../Operateur/images/desactiver_utilisateur.png)

N.B. : il est également possible de réactiver l’utilisateur en cliquant à nouveau sur le bouton.

## Supprimer un utilisateur

1. Se rendre dans `Administration` > `Gérer les utilisateurs`
2. Cliquer sur modifier l’utilisateur icône ![supprimer](../Operateur/images/icone_supprimer.png)
