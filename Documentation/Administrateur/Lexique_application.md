# Lexique Départements & Notaires

## Résultats de recherche d’un individu

Le moteur de recherche propose trois types de réponses:

* **Connu** : Individu connu du fichier des services du Département.
* **Inconnu** : Individu inconnu du fichier des services du Département.
* **Ambigu** : Individu avec des informations saisies dans la recherche qui ne permettent pas
  de définir avec exactitude l’identité de celui-ci.

## Aides et codes d'aides

Nous parlons d'**aide récupérable** (certains Départements utilisent le terme *ASG*, Aide Sociale Générale) pour les
aides
pouvant générer une créance au décès d'un bénéficiaire.

Nous parlons d'**aide non récupérable** (certains Départements utilisent le terme *indu*) pour les aides qui ne sont
pas récupérables, sauf si elles ont été versées après le décès du bénéficiaire. (Ce cas peut arriver si le
Département a connaissance tardivement du décès.)

Enfin, il existe des aides *en cours d'instruction*, qui est un état transitoire.

Dans l'application Départements & Notaires, le *code d'aide* décrit le type d'aide :

* `1SEXTREC` décrit une aide récupérable
* `1SEXTNONR` décrit une aide non récupérable
* `NULL` (champ vide) décrit une aide en cours d'instruction

**Créance** : Montant de l’aide récupérable par le Département dans le cadre d'une aide récupérable.

## Autres

**Alerte décès** : fonctionnalité informant le Département si une étude notariale fait une recherche sur un individu
dont le Département n'a pas la date de décès (i.e. le Département n'a pas connaissance de son décès).

**Code CRPCEN** : Code d’identification unique d’une étude notariale.
