# Installation du projet avec docker pour les développeurs

## Téléchargement

[Download](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/repository/archive.zip?ref=master)

## Configuration (facultatif - les fichiers sont créés si non présent lors de l'installation)

Créer les fichiers .env et appli_sf/.env en copiant respectivement les fichiers .env.dist et appli_sf/.env.dist et les
modifier si besoin.

Dans le fichier `appli_sf/.env` :

* `APP_ENV` : remplacer `"prod"` par `"dev"`
* `DATABASE_URL` : remplacer
    * `"mysqli://notaires:password@127.0.0.1:3306/notaires"` par
    * `"mysqli://DB_USER:DB_PASSWORD@database:3306/DB_NAME`
    * `DB_USER`, `DB_PASSWORD` et `DB_NAME` sont les mêmes que dans le `.env` à la racine
* `MAILER_DSN` : remplacer `"smtp://localhost:25"`  par `"smtp://mail:MAILER_SMTP_PORT"`
* `MAILER_SMTP_PORT` est le même que dans le `.env` à la racine
* `MAILER_SENDER` : mettre une adresse email correcte

## Installation

```shell script
make install
```

Vérifier la présence et le contenu des fichiers .env et appli_sf/.env. Relancer les conteneurs docker en cas de
modification : `make docker-restart`

## Accès aux services

Application web : `http://HTTP_HOSTNAME:HTTP_PORT` ou `http://127.0.0.1:HTTP_PORT`

Client de messagerie : `http://HTTP_HOSTNAME:MAILER_CLIENT_PORT`

\* HTTP_HOSTNAME, HTTP_PORT et MAILER_CLIENT_PORT sont définies dans le .env à la racine

## Initialiser la base de données

Créer un nouvel utilisateur :

```shell
./bin/console user:add
```

Et importer les données de test :

```shell
./bin/console import:individus imports/individus.csv
```

## Réinitialiser le projet

Suppression des conteneurs et volumes docker ainsi qu'un effacement des fichiers générés :

```shell
make clean
```

/!\ Les fichiers `.env` et `appli_sf/.env` ne sont pas supprimés

## Commandes à connaitre

### Commandes Symfony

```shell
./bin/console
```

### Vider les caches Symfony

```shell
./bin/console cache:clear
```

### Relancer une installation avec réinitialisation

```shell
make reset
```

Équivalent à `make clean` suivi de `make install`

/!\ Les fichiers `.env` et `appli_sf/.env` ne sont pas supprimés. Les supprimer manuellement avant pour une
réinitialisation complète

### Utiliser composer

```shell
./bin/composer
```

### Utiliser yarn

```shell
./bin/yarn
```

* Compiler les ressources (JS + SCSS)

```shell
./bin/yarn dev
```

* Activer le "watcher" pour les ressources (JS + SCSS)

```shell
./bin/yarn watch
```

* Démarrer les conteneurs docker

```shell
make docker-start
```

* Stopper les conteneurs docker

```shell
make docker-stop
```

* Redémarrer les conteneurs docker

```shell
make docker-restart
```

* Détruire les conteneurs docker

```shell
make docker-kill
```

## Outils d'analyse et de tests du code

### PHP_CodeSniffer

```shell
./bin/phpcs
```

### PHPStan

```shell
./bin/phpstan
```
