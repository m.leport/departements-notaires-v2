# Configurations possibles de Départements & Notaires

## Conf00

La Configuration 00 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Jour et mois différents donnent ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf01

La Configuration 01 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Jour et mois différents donnent ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf02

La Configuration 02 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Jour et mois différents donnent ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf03

La Configuration 03 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Jour différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf04

La Configuration 04 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Mois différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf05

La Configuration 05 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Année différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf06

La Configuration 06 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Date différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf07

La Configuration 07 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Jour et mois différents donnent ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf08

La Configuration 08 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Jour différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf09

La Configuration 09 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Mois différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf10

La Configuration 10 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Année différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf11

La Configuration 11 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Date différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf12

La Configuration 12 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Jour différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf13

La Configuration 13 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Mois différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf14

La Configuration 14 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Année différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf15

La Configuration 15 correspond aux paramètres suivants :

* Recherche soundex = coché
* Précision de la date de naissance = Date différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = décoché

## Conf16

La Configuration 16 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Jour différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf17

La Configuration 17 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Mois différent donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf18

La Configuration 18 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Année différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché

## Conf19

La Configuration 19 correspond aux paramètres suivants :

* Recherche soundex = décoché
* Précision de la date de naissance = Date différente donne ambigu au lieu d'inconnu
* Prénom différent donne ambigu au lieu de connu = coché
