# Test d'acceptation `Conf02-B1-N-P`

## Pré-requis

L'application doit être dans la [configuration 02](../Configurations.md#Conf02)

## Test 1

1. Se connecter en tant qu'étude 3
2. Aller sur l'onglet recherche et saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 10/02/2020
    * Prénom : Lucette
    * Nom d'usage : PERRON
    * Date de naissance : 01/01/1905
3. La page résultat de recherche contient : Cette personne est connue de nos services, un message électronique vous est
   envoyé avec les informations relatives à votre demande.
4. L'Affichage dans le tableau mes précédentes recherches contient : aide récupérable
5. Template mail généré est : search_found.html.twig
6. Le courriel reçu par le Notaire contient : est connue du département
7. Le courriel reçu par le Notaire contient :
    * 1 aide récupérable
    * 1 aide non récupérable
8. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
9. Template PDF généré est : search_found_rec.html.twig
10. Le PDF (en pièce jointe du courriel) contient :
    * 1 aide récupérable
    * 1 aide non récupérable
11. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

## Test 2

1. Se connecter en tant qu'étude 3
2. Aller sur l'onglet recherche et saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 10/02/2020
    * Prénom : Luce
    * Nom d'usage : PERRON
    * Date de naissance : 01/01/1905
3. La page résultat de recherche contient : Les éléments que vous avez saisis ne permettent pas d'établir avec certitude
   l'identité de la personne. Les services du département ont pris en charge votre demande et vous répondront dans les
   meilleurs délais.
4. L'Affichage dans le tableau mes précédentes recherches contient : ambigu
5. Template mail généré est : `search_ambigous.html.twig`
6. Le courriel reçu par le Notaire contient : "Les éléments suivants ne permettent pas d'établir avec certitude l'
   identité de la personne."
