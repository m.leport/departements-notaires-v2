# Test d'acceptation `Conf00-J1-N-P`

## Pré-requis

L'application doit être dans la [configuration 00](../Configurations.md#Conf00)

## Test

1. Se connecter en tant qu'étude 3
2. Aller sur l'onglet recherche et saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 10/02/2020
    * Prénom : Clara
    * Nom d'usage : BOURDIAU
    * Date de naissance : 01/09/1966
3. La page résultat de recherche contient : Les éléments que vous avez saisis ne permettent pas d'établir avec certitude
   l'identité de la personne.
4. L'Affichage dans le tableau mes précédentes recherches contient : ambigu
5. Template mail généré est : search_ambiguous.html.twig
6. Le courriel reçu par le Notaire contient : ambigu
7. Le courriel reçu par le Notaire contient : Les éléments suivants ne permettent pas d'établir avec certitude l'
   identité de la personne.
