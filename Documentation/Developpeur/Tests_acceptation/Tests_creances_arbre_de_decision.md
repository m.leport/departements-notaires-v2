# Tests d'acceptations des créances

Les templates d'email envoyés après une recherche sont soumis à certaines conditions qui nécessitent une visualisation
sous forme d'arbre de décision permettant de ne pas perdre des cas de tests en cours de route.

## Arbre de décision

L'arbre de décision suivant représente les cas de tests portant sur l'évolution des créances dans le
ticket [#173](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/173).

* Résultat inconnu
    * mail standard, aucun ajout
* Résultat ambigu
    * Test d’acceptation
      8.1.04 : [#193](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/193)
* Résultat connu
    * Pas de date de décès : Test d’acceptation
      8.1.02 : [#191](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/191)
    * Date de décès différente de la date de décès recherchée : Test d’acceptation
      8.1.03 : [#192](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/192)
    * Date de décès égale à la date recherchée :
        * Cas 1 : Créance récupérable affichable montant inclus : Test d'acceptation
          8.1.06  [#174](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/174)
        * Cas 2 : Créance récupérable affichable montant hors seuil : Test d'acceptation
          8.1.07 [#195](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/195)
        * Cas 3 : Créance récupérable non affichable montant hors seuil : Test d'acceptation
          8.1.08 [#196](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/196)
        * Cas 4 : Créance récupérable non affichable montant inclus : Test d'acceptation
          8.1.09 [#197](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/197)
        * Cas 5 : Créance non récupérable affichable montant hors seuil : Test d'acceptation
          8.1.10 [#198](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/198)
        * Cas 6 : Créance non récupérable affichable montant hors seuil : Test d'acceptation
          8.1.11 [#199](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/199)
