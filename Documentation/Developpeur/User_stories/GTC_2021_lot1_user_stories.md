# User stories Départements & Notaires - 5.Moteur de recherche

Les points présentés sur le l’algorithme de recherche ne font pas consensus, aussi les évolutions apportées feront
l’objet d’un paramètre afin de les activer / désactiver.

## 5.1 Chercher « Nom-A Nom-B » et aussi « Nom-B Nom-A » ===================================================

Ticket [#88 Chercher « Nom-A Nom-B » et aussi « Nom-B Nom-A »](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/88)
.

La fonctionnalité demandée consiste à chercher pour les noms composés aussi bien la requête « Nom-A Nom-B »
que « Nom-B Nom-A ».

Le CD57 informe avoir déjà fait un tel développement en v1.
Le CD06, utilisant la v1, a développé son propre
contournement qui concatène et enlève accents et apostrophes. Le CD33 formule ses requêtes avec nom de jeune
fille / nom marital. Le CD22 utilise le nom de naissance uniquement.

Note post-GTC : cette partie du code du CD57 n’a pas été vue lors de l’intégration de ses contributions.
L’ADULLACT demandera au prestataire AtolCD de reprendre ce code ou de le redévelopper selon le même principe selon
son jugement. (Cf dépôt Départements et Notaires v1,
[branche `Source-CD57-v1.x-DO-NOT-MERGE`](https://gitlab.adullact.net/departements-notaires/departements-notaires/-/tree/Source-CD57-v1.x-DO-NOT-MERGE))

### User story « Nom-A Nom-B »

* **ETQ :** notaire ou agent,
* **Je souhaite :** qu'une recherche sur le nom (d'usage ou d'état-civil) "CAMPO-PAYSAA" cherche aussi "PAYSAA CAMPO"
* **Afin de :** ne pas râter un individu ayant un nom composé saisi dans le mauvais ordre

### Test d'acceptation « Nom-A Nom-B » 01

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Se connecter en tant que Notaire
3. Faire une recherche avec les éléments suivants :
    * Prénom : Gael
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : "PAYSAA CAMPO"
    * Date de naissance : 16/04/2008
4. L'individu "Gael CAMPO-PAYSAA" doit faire partie des résultats de recherche

### Test d'acceptation « Nom-A Nom-B » 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément que deux recherches sont effectuées : "Nom-A Nom-B" et "Nom-B Nom-A"
3. La documentation explique précisément à quelle(s) étape(s) les deux recherches sont effectuées.
4. La documentation explique précisément les deux recherches avec et sans l'usage du Soundex

### Test d'acceptation « Nom-A Nom-B » 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : "CAMPO-PAYSAA"

## 5.2 Privilégier les réponses ambigües

De nombreux échanges ont eu lieu autour de la nature des résultats renvoyés dans plusieurs situations.
L’idée principale est de privilégier les réponses « ambigües » plutôt que « connues »
(peut-être à tort) ou « inconnues » (peut-être à tort).

### 5.2.1 Cas de l’année de naissance différente (tout le reste étant correct) =========================================

Ticket [#52 Si nom + prénom exacts mais erreur sur l’année de naissance, la réponse doit être AMBIGUE](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/52)
.

Dans un tel cas, l’algorithme répond de type inconnu, le CD22 souhaiterait avoir une réponse de type ambigu.

Les départements 38, 57 et 69 rappellent que le notaire est responsable de ce qu’il saisit, et qu’un excès de
réponses ambigües génère trop de recherches manuelles pour les services. Les départements 22 et 59 font valoir
qu’augmenter le nombre de réponses ambiguës n’est pas gênant, car des créances supplémentaires sont ainsi récupérées.

Il est partagé que la bonne année de naissance est donnée par les maisons de retraite ou les héritiers.

En l’absence de consensus, cette fonctionnalité fera l’objet d’un paramètre activable ou désactivable.

### User story 5.2.1 Année de naissance différente

* **ETQ :** notaire ou agent,
* **Je souhaite :**
    * sur une instance de D&N ayant activé le paramètre "année de naissance différente donne ambigu au lieu d’inconnu",
    * qu'une recherche sur un individu, correspondant en tout point aux informations en base de donnée sauf sur *
      l'année*
      de naissance, donne un résultat ambigu
* **Afin de :** d'augmenter le nombre de créances récupérées.

### Test d'acceptation 5.2.1 Année de naissance différente 01

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Avoir activé le paramètre "année de naissance différente donne ambigü au lieu de inconnu",
3. Se connecter en tant que Notaire
4. Faire une recherche avec les éléments suivants :
    * Prénom : Aimé
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : OLMO
    * Date de naissance : 06/12/1976 (l'année saisie en base de donnée étant 1977; jour et mois sont identiques)
5. La réponse est de type **ambigu**

### Test d'acceptation 5.2.1 Année de naissance différente 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément à quel(s) moment(s) et dans quelle(s) étape(s), l'année de naissance n'est pas
   prise en compte.
3. La documentation explique précisément le mécanisme avec et sans l'usage du Soundex

### Test d'acceptation 5.2.1 Année de naissance différente 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : Aimé OLMO 06/12/1976

### 5.2.2.Cas du prénom différent (tout le reste étant correct) ========================================================

Ticket [#51 Si nom + date de naissance exacte mais prénom différent, la réponse doit être AMBIGUE](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/51)
.

Dans le comportement actuel du moteur de recherche, le prénom n’est pas pris en compte. Cela permet de
notamment de répondre au besoin exprimé par le CD38 sur des requêtes où le prénom peut être autant _Giuseppe_
que _Joseph_ pour la même personne.

Par contre, le CD22 soulève le cas suivant. En base de données, la personne _Percepied Paulette_ est présente.
Le notaire cherche _Percepied P_, et à l’heure actuelle la réponse est de type connu, or la réponse souhaitée
est de type ambigu.

Un cas similaire est présenté avec Max Calvet / Maxime Calvet (qui sont deux personnes différentes).
Ticket [#110 Répondre « ambigu » plus que « inconnu » : cas Max Calvet / Maxime Calvet
(2 personnes différentes)](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/110).

Par ailleurs un bug est suspecté sur la gestion des prénoms, ticket [#105 Bug : la recherche avec la première
lettre du prénom retourne un résultat connu au lieu d’ambigu](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/105)
.
L’ADULLACT demandera à AtolCD de vérifier ce comportement.

### User story 5.2.2 prénom différent

* **ETQ :** agent ou notaire,
* **Je souhaite :**
    * sur une instance de D&N ayant activé le paramètre "prénom différent donne ambigu au lieu de connu",
    * qu'une recherche sur un individu, correspondant en tout point aux informations en base de donnée sauf sur le
      prénom, donne un résultat ambigu
* **Afin de :** limiter les erreurs sur des personnes différentes ayant des prénoms similaires

### Test d'acceptation 5.2.2 prénom différent 01

1. Dans
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
   ajouter l'individu suivant : Jeanne MILLINER née le 01/01/1930
2. Avoir activé le paramètre "prénom différent donne ambigu au lieu de connu",
3. Se connecter en tant que Notaire
4. Faire une recherche avec les éléments suivants :
    * Prénom : Jeanine (c'est bien "Jeanine" qui est demandé)
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : MILLINER
    * Date de naissance : 01/01/1930
5. La réponse est de type **ambigu**

### Test d'acceptation 5.2.2 prénom différent 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément à quel(s) moment(s) et dans quelle(s) étape(s), le prénom est pris en compte.
3. La documentation explique précisément le mécanisme avec et sans l'usage du Soundex

### Test d'acceptation 5.2.2 prénom différent 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : Jeanine MILLINER 01/01/1930

### 5.2.3 Cas de toute la date de naissance différente (le reste étant correct) ========================================

Ticket [#76 recherche avec nom et prénom exacts](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/76)
.

Une recherche sur une personne avec nom et prénom exact mais avec une date de naissance erronée, la réponse actuelle
est de type inconnu, la réponse souhaitée est de type ambigu.

### User story 5.2.3 date de naissance différente

* **ETQ :** agent ou notaire,
* **Je souhaite :**
    * sur une instance de D&N ayant activé le paramètre "date de naissance différente donne ambigu au lieu d'inconnu",
    * qu'une recherche sur un individu, correspondant en tout point aux informations en base de donnée sauf sur la
      date de naissance, donne un résultat ambigu
* **Afin de :** de ne pas rater une éventuelle créance

### Test d'acceptation 5.2.3 date de naissance différente 01

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Avoir activé le paramètre "date de naissance différente donne ambigu au lieu d'inconnu",
3. Se connecter en tant que Notaire
4. Faire une recherche avec les éléments suivants :
    * Prénom : Aimé
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : OLMO
    * Date de naissance : 01/01/1901
5. La réponse est de type **ambigu**

### Test d'acceptation 5.2.3 date de naissance différente 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément à quel(s) moment(s) et dans quelle(s) étape(s), la date de naissance est prise
   en compte.
3. La documentation explique précisément le mécanisme avec et sans l'usage du Soundex

### Test d'acceptation 5.2.3 date de naissance différente 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : Aimé OLMO 01/01/1901

### 5.2.4 Autres cas

Tickets :

* [#72 Une erreur sur le mois de naissance](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/72)
* [#73 une erreur sur jour de naissance](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/73)
* [#74 initiale du prénom](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/74)
* [#75 3 premiers caractères du prénom](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/75)

Plusieurs autres situations ont été identifiées par le CD22 où une différence sur une information
(toutes les autres étant correctes) ne renvoie pas la réponse souhaitée.

| **Différence**                  | **Réponse obtenue** | **Réponse souhaitée** |
|:--------------------------------|:--------------------|:----------------------|
| Mois de naissance               | Non trouvé          | Ambigu                |
| Jour de naissance               | Non trouvé          | Ambigu                |
| Initiale du prénom              | Non trouvé          | Ambigu                |
| 3 premiers caractères du prénom | Non trouvé          | Ambigu                |

Les cas "Initiale du prénom" et "3 premiers caractères du prénom" sont couverts par la User Story
"prénom différent"

### User story 5.2.4.1 Mois de naissance différent

* **ETQ :** notaire ou agent,
* **Je souhaite :**
    * sur une instance de D&N ayant activé le paramètre "mois de naissance différent donne ambigu au lieu d’inconnu",
    * qu'une recherche sur un individu, correspondant en tout point aux informations en base de donnée sauf sur
      le mois de naissance, donne un résultat ambigu
* **Afin de :** d'augmenter le nombre de créances récupérées.

### Test d'acceptation 5.2.4.1 Mois de naissance différent 01

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Avoir activé le paramètre "mois de naissance différent donne ambigu au lieu d’inconnu",
3. Se connecter en tant que Notaire
4. Faire une recherche avec les éléments suivants :
    * Prénom : Aimé
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : OLMO
    * Date de naissance : 06/01/1977
5. La réponse est de type **ambigu**

### Test d'acceptation 5.2.4.1 Mois de naissance différent 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément à quel(s) moment(s) et dans quelle(s) étape(s), le mois de naissance est
   pris en compte.
3. La documentation explique précisément le mécanisme avec et sans l'usage du Soundex

### Test d'acceptation 5.2.4.1 Mois de naissance différent 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : Aimé OLMO 06/01/1977

### User story 5.2.4.2 Jour de naissance différent

* **ETQ :** notaire ou agent,
* **Je souhaite :**
    * sur une instance de D&N ayant activé le paramètre "jour de naissance différent donne ambigu au lieu d’inconnu",
    * qu'une recherche sur un individu, correspondant en tout point aux informations en base de donnée sauf sur
      le jour de naissance, donne un résultat ambigu
* **Afin de :** d'augmenter le nombre de créances récupérées.

### Test d'acceptation 5.2.4.2 Jour de naissance différent 01

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Avoir activé le paramètre "jour de naissance différent donne ambigu au lieu d’inconnu",
3. Se connecter en tant que Notaire
4. Faire une recherche avec les éléments suivants :
    * Prénom : Aimé
    * Nom obligatoire (d'usage ou d'état-civil, selon le paramétrage de l'instance D&N) : OLMO
    * Date de naissance : 01/12/1977
5. La réponse est de type **ambigu**

### Test d'acceptation 5.2.4.2 Jour de naissance différent 02

1. Se rendre sur
   la [documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md)
2. La documentation explique précisément à quel(s) moment(s) et dans quelle(s) étape(s), le jour de naissance est
   pris en compte.
3. La documentation explique précisément le mécanisme avec et sans l'usage du Soundex

### Test d'acceptation 5.2.4.2 Jour de naissance différent 03

1. Se rendre sur
   les [exemples de cheminement de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Utilisateur/Algorithme_de_recherche.md#exemples-de-cheminements-de-recherche)
2. Un exemple de cheminement de recherche est présenté avec : Aimé OLMO 01/12/1977
