# Lancer les tests E2E avec Cypress

Suivre la procédure décrite [ici](HOWTO_installation_docker.md) pour l'installation du projet en local.

Les tests E2E permettent de vérifier le bon fonctionnement de l'appli. Avant de proposer une modification du code de
l'appli,
il est très fortement conseillé de lancer ces tests en local (et d'en ajouter si besoin) :

/!\ Lancer les tests écrase les données existantes en base de données

## En mode console

* Se placer à la racine du projet et lancer `bin/cypress`
* Pour lancer un test sur un fichier de spec spécifique utiliser l'option --spec : \
  `bin/cypress --spec 'cypress/integration/EmptyDB/1-login.spec.js'`

## En mode graphique

* Se placer à la racine du projet et lancer `bin/cypress-gui`

Il est possible que le navigateur lancé par Cypress rencontre une des erreurs suivantes :
"SIG_ABRT" sous Chrome, pas de réponse sous Firefox ou page blanche sous Electron.

Dans ce cas il faudra lancer les tests une nouvelle fois avec la commande `bin/cypress-gui`

> **NOTE :** Certains tests n'ont pas pu être automatisés et se retrouvent dans le
> fichier [TEST_E2E_D&N_v2.md](TEST_E2E_D&N_v2.md)
