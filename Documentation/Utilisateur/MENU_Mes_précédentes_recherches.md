# MENU Mes précédentes recherches

Ce menu est visible par les études notariales.

## Présentation des critères de recherche

Pour effectuer une recherche, il est possible ou non d'avoir recours à des filtres de recherche.

![criteres_recherche](./images/critere_recherche_etude.png)

1. **Type de réponse** permet de filtrer par réponse : soit *Récupération* / soit *Indus probable* / soit *Inconnu* /
   soit *Ambigu* (attention : pas de possibilité de cumul de plusieurs de ces critères)
2. **Nom de l’individu** permet de filtrer par nom d’un individu au préalablement recherché
3. **Date de début et fin** permet de filtrer selon la date ou selon un créneau de date d'une recherche effectuée par
   votre étude.

## Affichage des résultats

Une fois la recherche lancée, un tableau sera visible directement dans l'application.

![affichage](./images/affichage_recherche_etude.png)

*exemple ici : pas de filtre renseigné.*

Les résultats sont affichés par colonne afin de pouvoir y faire un tri par ordre croissant ou décroissant.

Pour effectuer le tri souhaité, il faudra cliquer sur le bouton ![tri](./images/Bouton_tri.png).

N.B. : La colonne Lien PDF permet au notaire de télécharger le PDF qui lui a été transmis par mail au moment de sa
recherche. Ce lien est présent sur chaque résultat sauf le cas *Ambigu*.
