# Exemples de cas de recherches de bénéficiaire d'aides multiples

## Cas B1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| B1  |      1      |        1        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Lucette
* Nom d'usage : Perron
* Date de naissance : 01/01/1905

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|  44726  | Perron    | Lucette | 01/01/1905     |  1SEXTREC |
|  44726  | Perron    | Lucette | 01/01/1905     | 1SEXTNONR |

### B1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1905'
nom_usage = 'Perron'
(prenom = 'Lucette' || prenom2 = 'Lucette' || prenom3 = 'Lucette') OU (prenom LIKE 'Luc%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### B1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Lucette Perron né·e le 01/01/1905 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 44726), et possède 1 aide(s) récupérable(s), ainsi que 1 aide(s) non récupérable(s). En voici la liste :

   - libellé : Placement HA en établissement PA ; code aide : 1SEXTREC
   - libellé : APA à domicile ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas B2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:--:|:-----------:|:---------------:|:----------------------:|
| B2 | 2 | 1 | 0 |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Janine
* Nom d'usage : Peyroul
* Date de naissance : 01/01/1910

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  45205  | Peyroul   | Janine | 01/01/1910     |  1SEXTREC |
|  45205  | Peyroul   | Janine | 01/01/1910     |  1SEXTREC |
|  45205  | Peyroul   | Janine | 01/01/1910     | 1SEXTNONR |

### B2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1910'
nom_usage = 'Peyroul'
(prenom = 'Janine' || prenom2 = 'Janine' || prenom3 = 'Janine') OU (prenom LIKE 'Jan%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### B2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Janine Peyroul né·e le 01/01/1910 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 45205), et possède 2 aide(s) récupérable(s), ainsi que 1 aide(s) non récupérable(s). En voici la liste :

   - libellé : Aide ménagère PA ; code aide : 1SEXTREC
   - libellé : Aide ménagère PA ; code aide : 1SEXTREC
   - libellé : APA à domicile ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas B3

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| B3  |      1      |        2        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Patricia
* Nom d'usage : Patate
* Date de naissance : 01/01/1911

En base de données est présent :

| num_ind | nom_usage | prenom   | date_naissance | code_aide |
|:-------:|:----------|:---------|:---------------|----------:|
|  44155  | Patate    | Patricia | 01/01/1911     |  1SEXTREC |
|  44155  | Patate    | Patricia | 01/01/1911     | 1SEXTNONR |
|  44155  | Patate    | Patricia | 01/01/1911     | 1SEXTNONR |

### B3 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1911'
nom_usage = 'Patate'
(prenom = 'Patricia' || prenom2 = 'Patricia' || prenom3 = 'Patricia') OU (prenom LIKE 'Pat%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### B3 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Patricia Patate né·e le 01/01/1911 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 44155), et possède 1 aide(s) récupérable(s), ainsi que 2 aide(s) non récupérable(s). En voici la liste :

   - libellé : Placement HA en établissement PA ; code aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code aide : 1SEXTNONR
   - libellé : CD - PCH pour charges spécifiques (mensuelle) ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas B4

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| B4  |      2      |        2        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Dalal
* Nom d'usage : Syrie
* Date de naissance : 01/01/1912

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  45662  | Syrie     | Dalal  | 01/01/1912     |  1SEXTREC |
|  45662  | Syrie     | Dalal  | 01/01/1912     |  1SEXTREC |
|  45662  | Syrie     | Dalal  | 01/01/1912     | 1SEXTNONR |
|  45662  | Syrie     | Dalal  | 01/01/1912     | 1SEXTNONR |

### B4 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1912'
nom_usage = 'Syrie'
(prenom = 'Dalal' || prenom2 = 'Dalal' || prenom3 = 'Dalal') OU (prenom LIKE 'Dal%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### B4 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Dalal Syrie né·e le 01/01/1912 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 45662), et possède 2 aide(s) récupérable(s), ainsi que 2 aide(s) non récupérable(s). En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code aide : 1SEXTREC
   - libellé : Placement Temporaire pour non Travailleurs ; code aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code aide : 1SEXTNONR
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas C1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| C1  |      0      |        1        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Maesson
* Nom d'usage : ALOUANE
* Date de naissance : 03/12/1954

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|   180   | ALOUANE   | Maesson | 03/12/1954     | 1SEXTNONR |

### Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '03/12/1954'
nom_usage = 'ALOUANE'
(prenom = 'Maesson' || prenom2 = 'Maesson' || prenom3 = 'Maesson') OU (prenom LIKE 'Mae%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Maesson ALOUANE né·e le 03/12/1954 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 180), et possède 1 aide(s) non récupérable(s). En voici la liste :

   - libellé : Aides humaines à DOMICILE ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas C2

### Le cas C2 en version 2.2.0 comporte un résultat erroné

Le résultat retourné est inconnu, dans l'attente de l'évolution sur le cas Nom-A Nom-B Nom-C qui prendra en compte les
noms à particules.

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| C2  |      0      |        2        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : Bordeau
* Date de l'acte de décès : 12/02/2020
* Prénom : Francine
* Nom d'usage : de la Rochebonnefoy
* Date de naissance : 01/01/1903

En base de données est présent :

| num_ind | nom_usage           | prenom   | date_naissance | code_aide |
|:-------:|:--------------------|:---------|:---------------|----------:|
|  41894  | de la Rochebonnefoy | Francine | 01/01/1903     | 1SEXTNONR |
|  41894  | de la Rochebonnefoy | Francine | 01/01/1903     | 1SEXTNONR |

### C2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1903'
nom_usage = 'de la Rochebonnefoy'
(prenom = 'Francine' || prenom2 = 'Francine' || prenom3 = 'Francine') OU (prenom LIKE 'Fra%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### C2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Francine de la Rochebonnefoy né·e le 01/01/1903 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 41894), et possède 2 aide(s) non récupérable(s). En voici la liste :

   - libellé : PCH - élément 1 - Aide humaine à domicile ; code aide : 1SEXTNONR
   - libellé : PCH - élément 3A-Aménagement du logement-Déménagement ; code aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas D1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| D1  |      1      |        0        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 06/12/1977

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|    1    | OLMO      | Aimé   | 06/12/1977     |  1SEXTREC |

### D1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '06/12/1977'
nom_usage = 'OLMO'
(prenom = 'Aimé' || prenom2 = 'Aimé' || prenom3 = 'Aimé') OU (prenom LIKE 'Aim%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### D1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Aimé OLMO né·e le 06/12/1977 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1), et possède 1 aide(s) récupérable(s). En voici la liste :

   - libellé : Accueil PA en struct. collective ; code aide : 1SEXTREC

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas D2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| D2  |      2      |        0        |           0            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Pierre
* Nom d'usage : Mayou
* Date de naissance : 01/01/1904

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  44413  | Mayou     | Pierre | 01/01/1904     |  1SEXTREC |
|  44413  | Mayou     | Pierre | 01/01/1904     |  1SEXTREC |

### D2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1904'
nom_usage = 'Mayou'
(prenom = 'Pierre' || prenom2 = 'Pierre' || prenom3 = 'Pierre') OU (prenom LIKE 'Pie%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### D2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Pierre Mayou né·e le 01/01/1904 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 44413), et possède 2 aide(s) récupérable(s). En voici la liste :

   - libellé : Placement PH en Maison de Retraite, Long Séjour ; code aide : 1SEXTREC
   - libellé : Placement HA en établissement PA ; code aide : 1SEXTREC

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas E1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| E1  |      0      |        0        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Berthe
* Nom d'usage : Goitreux
* Date de naissance : 01/01/1908

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  44103  | Goitreux  | Berthe | 01/01/1908     |           |

### E1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1908'
nom_usage = 'Goitreux'
(prenom = 'Berthe' || prenom2 = 'Berthe' || prenom3 = 'Berthe') OU (prenom LIKE 'Ber%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### E1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Berthe Goitreux né·e le 01/01/1908 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 44103), et possède 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas E2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| E2  |      0      |        0        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Émile
* Nom d'usage : Carabignac
* Date de naissance : 01/01/1909

En base de données est présent :

| num_ind | nom_usage  | prenom | date_naissance | code_aide |
|:-------:|:-----------|:-------|:---------------|----------:|
|  44198  | Carabignac | Émile  | 01/01/1909     |           |
|  44198  | Carabignac | Émile  | 01/01/1909     |           |

### E2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1909'
nom_usage = 'Carabignac'
(prenom = 'Émile' || prenom2 = 'Émile' || prenom3 = 'Émile') OU (prenom LIKE 'Émi%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### E2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Émile Carabignac né·e le 01/01/1909 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 44198), et possède 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Aide ménagère PA ; code d'aide : aucun code d'aide
   - libellé : Frais de repas PA ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F1  |      1      |        1        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Bastien
* Nom d'usage : Legoumier
* Date de naissance : 01/01/1919

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|  1256   | Legoumier | Bastien | 01/01/1919     |  1SEXTREC |
|  1256   | Legoumier | Bastien | 01/01/1919     | 1SEXTNONR |
|  1256   | Legoumier | Bastien | 01/01/1919     |           |

### F1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1919'
nom_usage = 'Legoumier'
(prenom = 'Bastien' || prenom2 = 'Bastien' || prenom3 = 'Bastien') OU (prenom LIKE 'Bas%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Bastien Legoumier né·e le 01/01/1919 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1256), et possède 1 aide(s) récupérable(s), 1 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Aide ménagère PA ; code d'aide : 1SEXTREC
   - libellé : CD - PCH pour aides techniques (mensuelle) ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F2  |      2      |        2        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Jean-Chi
* Nom d'usage : Moutardier
* Date de naissance : 01/01/1921

En base de données est présent :

| num_ind | nom_usage  | prenom   | date_naissance | code_aide |
|:-------:|:-----------|:---------|:---------------|----------:|
|  1258   | Moutardier | Jean-Chi | 01/01/1921     |  1SEXTREC |
|  1258   | Moutardier | Jean-Chi | 01/01/1921     |  1SEXTREC |
|  1258   | Moutardier | Jean-Chi | 01/01/1921     | 1SEXTNONR |
|  1258   | Moutardier | Jean-Chi | 01/01/1921     | 1SEXTNONR |
|  1258   | Moutardier | Jean-Chi | 01/01/1921     |           |

### F2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1921'
nom_usage = 'Moutardier'
(prenom = 'Jean-Chi' || prenom2 = 'Jean-Chi' || prenom3 = 'Jean-Chi') OU (prenom LIKE 'Jea%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Jean-Chi Moutardier né·e le 01/01/1921 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1258), et possède 2 aide(s) récupérable(s), 2 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code d'aide : 1SEXTREC
   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code d'aide : 1SEXTNONR
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F3

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F3  |      1      |        2        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Etienne
* Nom d'usage : Buset
* Date de naissance : 01/01/1922

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|  1259   | Buset     | Etienne | 01/01/1922     |  1SEXTREC |
|  1259   | Buset     | Etienne | 01/01/1922     | 1SEXTNONR |
|  1259   | Buset     | Etienne | 01/01/1922     | 1SEXTNONR |
|  1259   | Buset     | Etienne | 01/01/1922     |           |

### F3 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1922'
nom_usage = 'Buset'
(prenom = 'Etienne' || prenom2 = 'Etienne' || prenom3 = 'Etienne') OU (prenom LIKE 'Eti%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F3 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Etienne Buset né·e le 01/01/1922 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1259), et possède 1 aide(s) récupérable(s), 2 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code d'aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code d'aide : 1SEXTNONR
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F4

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F4  |      2      |        1        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Blandine
* Nom d'usage : Apatou
* Date de naissance : 01/01/1923

En base de données est présent :

| num_ind | nom_usage | prenom   | date_naissance | code_aide |
|:-------:|:----------|:---------|:---------------|----------:|
|  1260   | Apatou    | Blandine | 01/01/1923     |  1SEXTREC |
|  1260   | Apatou    | Blandine | 01/01/1923     |  1SEXTREC |
|  1260   | Apatou    | Blandine | 01/01/1923     | 1SEXTNONR |
|  1260   | Apatou    | Blandine | 01/01/1923     |           |

### F4 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1923'
nom_usage = 'Apatou'
(prenom = 'Blandine' || prenom2 = 'Blandine' || prenom3 = 'Blandine') OU (prenom LIKE 'Bla%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F4 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Blandine Apatou né·e le 01/01/1923 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1260), et possède 2 aide(s) récupérable(s), 1 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code d'aide : 1SEXTREC
   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F5

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F5  |      1      |        1        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Clémence
* Nom d'usage : Kousou
* Date de naissance : 01/01/1924

En base de données est présent :

| num_ind | nom_usage | prenom   | date_naissance | code_aide |
|:-------:|:----------|:---------|:---------------|----------:|
|  1261   | Kousou    | Clémence | 01/01/1924     |  1SEXTREC |
|  1261   | Kousou    | Clémence | 01/01/1924     | 1SEXTNONR |
|  1261   | Kousou    | Clémence | 01/01/1924     |           |
|  1261   | Kousou    | Clémence | 01/01/1924     |           |

### F5 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1924'
nom_usage = 'Kousou'
(prenom = 'Clémence' || prenom2 = 'Clémence' || prenom3 = 'Clémence') OU (prenom LIKE 'Clé%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F5 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Clémence Kousou né·e le 01/01/1924 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1261), et possède 1 aide(s) récupérable(s), 1 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F6

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F6  |      2      |        2        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Arno
* Nom d'usage : Nimousse
* Date de naissance : 01/01/1920

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  1257   | Nimousse  | Arno   | 01/01/1920     |  1SEXTREC |
|  1257   | Nimousse  | Arno   | 01/01/1920     |  1SEXTREC |
|  1257   | Nimousse  | Arno   | 01/01/1920     | 1SEXTNONR |
|  1257   | Nimousse  | Arno   | 01/01/1920     | 1SEXTNONR |
|  1257   | Nimousse  | Arno   | 01/01/1920     |           |
|  1257   | Nimousse  | Arno   | 01/01/1920     |           |

### F6 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1920'
nom_usage = 'Nimousse'
(prenom = 'Arno' || prenom2 = 'Arno' || prenom3 = 'Arno') OU (prenom LIKE 'Arn%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F6 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Arno Nimousse né·e le 01/01/1920 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1257), et possède 2 aide(s) récupérable(s), 2 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code d'aide : 1SEXTREC
   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code d'aide : 1SEXTNONR
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F7

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F7  |      1      |        2        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Bernard
* Nom d'usage : Perron
* Date de naissance : 01/01/1925

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|  1262   | Perron    | Bernard | 01/01/1925     |  1SEXTREC |
|  1262   | Perron    | Bernard | 01/01/1925     | 1SEXTNONR |
|  1262   | Perron    | Bernard | 01/01/1925     | 1SEXTNONR |
|  1262   | Perron    | Bernard | 01/01/1925     |           |
|  1262   | Perron    | Bernard | 01/01/1925     |           |

### F7 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1925'
nom_usage = 'Perron'
(prenom = 'Bernard' || prenom2 = 'Bernard' || prenom3 = 'Bernard') OU (prenom LIKE 'Ber%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F7 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Bernard Perron né·e le 01/01/1925 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1262), et possède 1 aide(s) récupérable(s), 2 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : CD - PCH pour aides humaines à domicile ; code d'aide : 1SEXTNONR
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas F8

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F8  |      2      |        1        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Anita
* Nom d'usage : Bonny
* Date de naissance : 01/01/1926

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  1263   | Bonny     | Anita  | 01/01/1926     |  1SEXTREC |
|  1263   | Bonny     | Anita  | 01/01/1926     |  1SEXTREC |
|  1263   | Bonny     | Anita  | 01/01/1926     | 1SEXTNONR |
|  1263   | Bonny     | Anita  | 01/01/1926     |           |
|  1263   | Bonny     | Anita  | 01/01/1926     |           |

### F8 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1926'
nom_usage = 'Bonny'
(prenom = 'Anita' || prenom2 = 'Anita' || prenom3 = 'Anita') OU (prenom LIKE 'Ani%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### F8 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Anita Bonny né·e le 01/01/1926 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1263), et possède 2 aide(s) récupérable(s), 1 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement de jour pour non travailleurs ; code d'aide : 1SEXTREC
   - libellé : Placement Temporaire pour non Travailleurs ; code d'aide : 1SEXTREC
   - libellé : PCH - élément 1 - Aide humaine à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas G1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| G1  |      0      |        1        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Sophie
* Nom d'usage : Perron
* Date de naissance : 01/01/1907

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
|  63303  | Perron    | Sophie | 01/01/1907     | 1SEXTNONR |
|  63303  | Perron    | Sophie | 01/01/1907     |           |

### G1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1907'
nom_usage = 'Perron'
(prenom = 'Sophie' || prenom2 = 'Sophie' || prenom3 = 'Sophie') OU (prenom LIKE 'Sop%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### G1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Sophie Perron né·e le 01/01/1907 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 63303), et possède 1 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : APA à domicile ; code d'aide : 1SEXTNONR

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas G2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| G2  |      0      |        2        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Vasco
* Nom d'usage : Boisséson
* Date de naissance : 01/01/1913

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
| 424308  | Boisséson | Vasco  | 01/01/1913     | 1SEXTNONR |
| 424308  | Boisséson | Vasco  | 01/01/1913     | 1SEXTNONR |
| 424308  | Boisséson | Vasco  | 01/01/1913     |           |

### G2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1913'
nom_usage = 'Boisséson'
(prenom = 'Vasco' || prenom2 = 'Vasco' || prenom3 = 'Vasco') OU (prenom LIKE 'Vas%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### G2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Vasco Boisséson né·e le 01/01/1913 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 424308), et possède 2 aide(s) non récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : APA à domicile ; code d'aide : 1SEXTNONR
   - libellé : CD - PCH pour aides humaines à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas G3

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| G3  |      0      |        1        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Joseph
* Nom d'usage : Couvreur
* Date de naissance : 01/01/1914

En base de données est présent :

| num_ind | nom_usage | prenom | date_naissance | code_aide |
|:-------:|:----------|:-------|:---------------|----------:|
| 1806271 | Couvreur  | Joseph | 01/01/1914     | 1SEXTNONR |
| 1806271 | Couvreur  | Joseph | 01/01/1914     |           |
| 1806271 | Couvreur  | Joseph | 01/01/1914     |           |

### G3 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1914'
nom_usage = 'Couvreur'
(prenom = 'Joseph' || prenom2 = 'Joseph' || prenom3 = 'Joseph') OU (prenom LIKE 'Jos%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### G3 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Joseph Couvreur né·e le 01/01/1914 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1806271), et possède 1 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : APA à domicile ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas G4

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| G4  |      0      |        2        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Armand
* Nom d'usage : Garan-Servier
* Date de naissance : 01/01/1915

En base de données est présent :

| num_ind | nom_usage | prenom        | date_naissance | code_aide |
|:-------:|:----------|:--------------|:---------------|----------:|
|  1252   | Armand    | Garan-Servier | 01/01/1915     | 1SEXTNONR |
|  1252   | Armand    | Garan-Servier | 01/01/1915     | 1SEXTNONR |
|  1252   | Armand    | Garan-Servier | 01/01/1915     |           |
|  1252   | Armand    | Garan-Servier | 01/01/1915     |           |

### G4 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1915'
nom_usage = 'Garan-Servier'
(prenom = 'Armand' || prenom2 = 'Armand' || prenom3 = 'Armand') OU (prenom LIKE 'Arm%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### G4 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Armand Garan-Servier né·e le 01/01/1915 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1252), et possède 2 aide(s) non récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : APA à domicile ; code d'aide : 1SEXTNONR
   - libellé : CD - PCH pour aides techniques (mensuelle) ; code d'aide : 1SEXTNONR
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas H1

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| H1  |      1      |        0        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Antoine
* Nom d'usage : Perron
* Date de naissance : 01/01/1906

En base de données est présent :

| num_ind | nom_usage | prenom  | date_naissance | code_aide |
|:-------:|:----------|:--------|:---------------|----------:|
|  47930  | Perron    | Antoine | 01/01/1906     |  1SEXTREC |
|  47930  | Perron    | Antoine | 01/01/1906     |           |

### H1 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1906'
nom_usage = 'Perron'
(prenom = 'Antoine' || prenom2 = 'Antoine' || prenom3 = 'Antoine') OU (prenom LIKE 'Ant%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### H1 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Antoine Perron né·e le 01/01/1906 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 47930), et possède 1 aide(s) récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : Placement HA en établissement PA ; code d'aide : 1SEXTREC

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas H2

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| H2  |      2      |        0        |           1            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Pierre-Henri
* Nom d'usage : Garan-Servier
* Date de naissance : 01/01/1916

En base de données est présent :

| num_ind | nom_usage     | prenom       | date_naissance | code_aide |
|:-------:|:--------------|:-------------|:---------------|----------:|
|  1253   | Garan-Servier | Pierre-Henri | 01/01/1916     |  1SEXTREC |
|  1253   | Garan-Servier | Pierre-Henri | 01/01/1916     |  1SEXTREC |
|  1253   | Garan-Servier | Pierre-Henri | 01/01/1916     |           |

### H2 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1916'
nom_usage = 'Garan-Servier'
(prenom = 'Pierre-Henri' || prenom2 = 'Pierre-Henri' || prenom3 = 'Pierre-Henri') OU (prenom LIKE 'Pie%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### H2 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Pierre-Henri Garan-Servier né·e le 01/01/1916 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1253), et possède 2 aide(s) récupérable(s), ainsi que 1 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement en Internat pour travailleurs ; code d'aide : 1SEXTREC
   - libellé : Aide ménagère PA ; code d'aide : 1SEXTREC
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas H3

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| H3  |      1      |        0        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Enguerrand
* Nom d'usage : Brousse
* Date de naissance : 01/01/1917

En base de données est présent :

| num_ind | nom_usage | prenom     | date_naissance | code_aide |
|:-------:|:----------|:-----------|:---------------|----------:|
|  1254   | Brousse   | Enguerrand | 01/01/1917     |  1SEXTREC |
|  1254   | Brousse   | Enguerrand | 01/01/1917     |           |
|  1254   | Brousse   | Enguerrand | 01/01/1917     |           |

### H3 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1917'
nom_usage = 'Brousse'
(prenom = 'Enguerrand' || prenom2 = 'Enguerrand' || prenom3 = 'Enguerrand') OU (prenom LIKE 'Eng%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### H3 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Enguerrand Brousse né·e le 01/01/1917 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1254), et possède 1 aide(s) récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement en Internat pour travailleurs ; code d'aide : 1SEXTREC
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

## Cas H4

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| H4  |      2      |        0        |           2            |

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : François
* Nom d'usage : Civrac
* Date de naissance : 01/01/1918

En base de données est présent :

| num_ind | nom_usage | prenom   | date_naissance | code_aide |
|:-------:|:----------|:---------|:---------------|----------:|
|  1255   | Civrac    | François | 01/01/1918     |  1SEXTREC |
|  1255   | Civrac    | François | 01/01/1918     |  1SEXTREC |
|  1255   | Civrac    | François | 01/01/1918     |           |
|  1255   | Civrac    | François | 01/01/1918     |           |

### H4 Recherche

La recherche suivante est lancée sur la base de données :

```text
date_naissance = '01/01/1918'
nom_usage = 'Civrac'
(prenom = 'François' || prenom2 = 'François' || prenom3 = 'François') OU (prenom LIKE 'Fra%')
```

**Résultat :** 1 individu trouvé, la personne est **connue**

### H4 Mail

Le mail reçu est le suivant :

```text
Maître,

Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de François Civrac né·e le 01/01/1918 et dont le décès est survenu le : 10/02/2020.

Cette personne est connue du département (numéro d'individu : 1255), et possède 2 aide(s) récupérable(s), ainsi que 2 aide(s) en cours d'instruction. En voici la liste :

   - libellé : Placement en Internat pour travailleurs ; code d'aide : 1SEXTREC
   - libellé : Aide ménagère PA ; code d'aide : 1SEXTREC
   - libellé : SAA/Hébergement Légal ; code d'aide : aucun code d'aide
   - libellé : BAPH/Hébergement Légal ; code d'aide : aucun code d'aide

Vous trouverez ci-joint la réponse de mes services.

Je vous prie d'agréer, Maître, mes courtoises salutations
```

