# Manuel d'utilisation de l'applicaiton

Sont décrites ici les étapes de connexion et d'utilisation de l'application pour une étude notariale.

## Première connexion

Lors de la création de votre compte, un email vous a été envoyé.

![Email](images/mail.png)

En cliquant sur le lien vous arrivez sur la page de réinitialisation de votre mot de passe.

![Réinitialisation](images/reinit.png)

Vous devez ici renseigner votre code CRPCEN, votre mot de passe et une seconde fois votre mot de passe.

Le mot de passe doit être d'une longueur minimum de 12 caractères avec au moins une majuscule, un chiffre et un
caractère spécial (?/!*%$...)."

Une fois cette étape terminée vous pouvez vous rendre sur la page de connexion et vous connecter en utilisant votre code
CRPCEN et le mot de passe choisi précédement.

![Réinitialisation](images/login.png)

## Effectuer une recherche

Une fois connecté sur l'application, la page de recherche s'affiche

![Recherche](images/recherche.png)

Il vous suffit maintenant de remplir les champs suivants (les champs comportant * sont obligatoires) :

- Certifier être en possession de l'acte de décès *
- La date du décès *
- Le lieu décès *
- La date de l'acte de décès *
- Le destinataire
- Le prénom de la personne décédée *
- Le deuxième et troisième prénom de la personne décédée
- Le nom d'usage de la personne décédée * (obligatoire ou non selon la configuration de l'application)
- Le nom d'état civil de la personne décédée * (obligatoire ou non selon la configuration de l'application)
- La date de naissance de la personne décédée *

![Recherche remplie](images/recherche-remplie.png)

Après avoir envoyé le formulaire, l'algorithme de recherche vous renverra un des résultats suivants :

- résultat de recherche trouvé
- résultat de recherche non trouvé
- résultat de recherche ambigu

![Recherche trouvee](images/recherche-trouvee.png)

Vous trouverez plus de détail dans la documentation
décrivant [le fonctionnement de l'algorithme de recherche](./Algorithme_de_recherche.md).

En parallèle du résultat affiché à l'écran, un email récapitulatif de votre recherche vous est envoyé.
Cet email est également transmis directement à votre CD.

## Alerte décès

Si l'application est configurée pour cette option, vous n'avez rien de particulier à faire.

Quand une recherche sera effectuée, si la personne n'a pas de date de décès renseignée dans l'application ou si elle est
différente que celle saisie dans la recherche, un email d'alerte sera directement envoyé au CD afin d'en être informé.
