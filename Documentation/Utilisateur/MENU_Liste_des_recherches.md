# MENU Liste des recherches

Ce menu est visible par les instructeurs.

## Présentation des critères de recherche

Pour effectuer une recherche, il est possible ou non d'avoir recours à des filtres de recherche.

![](./images/critere_recherche_instructeur.png)

1. **L’étude** permet de filtrer par le nom de l’étude
2. **Type de réponse** permet de filtrer par réponse : soit *Récupération* / soit *Indus probable* / soit *Inconnu* /
   soit *Ambigu* (attention : pas de possibilité de cumul de plusieurs de ces critères)
3. **Nom de l’individu** permet de filtrer par nom d’un individu au préalablement recherché
4. **Date de début et fin** permet de filtrer selon la date ou selon un créneau de date d'une recherche effectuée par
   une étude.

## Affichage des résultats

Une fois la recherche lancée, un tableau sera visible directement dans l'application.

![](./images/resultat_recherche_instructeur.png)

Exemple ici : filtré par étude et par type de réponse

Les résultats sont affichés par colonne afin de pouvoir y faire un tri par ordre croissant ou décroissant.

Pour effectuer le tri souhaité, il faudra cliquer sur le bouton ![](./images/Bouton_tri.png).

N.B. : La colonne Lien PDF permet à l'insctructeur de télécharger le PDF transmis au notaire au moment de sa recherche.
Ce lien est présent sur chaque résultat sauf le cas *Ambigu*.

