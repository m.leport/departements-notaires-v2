# Échange documentaire

Seuls les fichiers PDF, JPEG, GIF et PNG sont acceptés.
Afin d'accélérer le traitement du dossier, il faudra fournir l'un des documents demandés.

Trois zones de téléversement de fichiers sont proposées :

* La première permet d'envoyer l'acte de décès.
* La deuxième permet d'envoyer l'actif net successoral.
* La troisième permet d'envoyer un fichier quelconque.

## Profil notaire

1. En tant que notaire, je lance une recherche :
    * Cocher la case "possession de l'acte de décès"
    * Date décès : 16/06/2018
    * Lieu de décès : Villeurbanne
    * Date de l'acte de décès : 16/06/2018
    * Prénom : Aimé
    * Nom d'usage : Olmo
    * Date de naissance : 06/12/1977
2. La réponse est connue
3. J'ajoute un fichier PDF comme acte de décès.
4. Je clique sur le bouton envoyer

## Profil Insturcteur

Un notaire effectue une recherche et téléverse un document.
En tant qu'instructeur, je reçois un message dédié :

Che.ère instructeur.rice,

Au moins un document a été ajouté suite à la demande concernant {{ firstName }} {{ useName }} (réponse {{ status }}).

Vous pouvez le consulter en vous connectant avec vos identifiants, puis en consultant la *Liste des recherches*.

Cordialement,

## Profil Instructeur et Notaire

Dans la liste des recherches, une cellule "Documents joints" contient une liste à puces de liens avec :

* Un lien nommé "Acte de décès" pointant vers le fichier de l'acte de décès (si un acte de décès a été téléversé).
* Un lien nommé "Actif net successoral" pointant vers le fichier de l'actif net successoral (si un actif a été téléversé).
* Un lien nommé "Autre document" pointant vers le fichier "autre" téléversé (si un fichier a été téléversé).

Si aucun fichier n'a été téléversé, la cellule est vide.

En cliquant sur un lien de fichier, on déclenche le téléchargement de ce dernier.
