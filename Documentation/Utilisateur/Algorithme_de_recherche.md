# Présentation de l'algorithme de recherche

La recherche constituant une des fonctionnalités essentielles de Départements & Notaires, il est important d'en
comprendre le fonctionnement.

## Paramètres de recherche

La recherche peut être modifiée selon les besoins grâce au paramètre de recherche. Il est important de comprendre leur
fonctionnement afin de les sélectionner au plus près des besoins.

### Paramètre de recherche Soundex

Références techniques :

* [Documentation MariaDB Soundex](https://mariadb.com/kb/en/soundex/)
* Cas d'usage [Soundex sur sql.sh](https://sql.sh/fonctions/soundex)

**Remarques importantes** :

* la fonctionnalité Soundex est construite pour la langue anglaise. La documentation officielle
  de MariaDB annonce que son usage sur d'autres langues peut amener à des résultats non fiables.
  L'utilisateur de Départements & Notaires doit avoir pris connaissance de ce fait avant l'activation de l'option.
* Des prénoms ou des noms proches phonétiquement peuvent donner des résultats imprécis.

### Paramètre de précision de la date de naissance

Il est possible de permettre une marge d'erreur dans la saisie de la date de naissance qui sera prise en compte si la
recherche avec une date exacte n'aboutit pas. Les 5 possibilités couvrant tous les cas de besoins sont décrites dans la
table de vérité ci-dessous :

|              | Jour et mois différents | Jour différent | Mois différent | Année différente | Date différente |
|:------------:|:-----------------------:|:--------------:|:--------------:|:----------------:|:---------------:|
|  Jour exact  |            -            |       -        |       O        |        O         |        -        |
|  Mois exact  |            -            |       O        |       -        |        O         |        -        |
| Année exacte |            O            |       O        |       O        |        -         |        -        |

Le jour exact uniquement ainsi que le mois exact uniquement donneraient une recherche trop imprécise. La possibilité de
définir ces critères n'est donc pas possible. Le paramètre **Date différente est recommandé dans ces cas là**.

### Paramètre prénom différent donne ambigu

Sans l'activation de ce paramètre la recherche actuelle permet de recevoir un résultat connu même si le prénom n'est pas
exact dans le cas ou un seul résultat est trouvé.

Afin de permettre d'éviter ce cas si besoin, l'activation du paramètre retournera un résultat ambigu si un seul résultat
est trouvé, mais que le prénom est différent.

## Recherche et contexte : un duo inséparable

La recherche porte sur le contenu de la base de donnée à un instant t, que nous appelons contexte.

Il est important de comprendre qu'une même recherche peut donner des résultats différents selon la présence (ou non)
d'éventuels homonymes. De même si la base de donnée évolue avec le temps (avec l'apparition d'une nouvelle aide par
exemple, et donc de nouvelles personnes), il se peut qu'une même recherche ne donne plus exactement les mêmes résultats
qu'auparavant.

Cette précaution méthodologique étant posée, passons à l'algorithme lui-même.

## Étapes de l'algorithme de recherche de Départements & Notaires

La recherche d'une personne s'effectue en plusieurs étapes.

## Étape 1

La recherche est lancée avec les conditions suivantes :

* `code_aide` est non null
* ET correspondance exacte de la date de naissance
* ET
    * SOIT
        * correspondance du nom (si nom composé recherche dans les deux sens : "NomA NomB" et "NomB NomA")
        * ET correspondance exacte d'un des 3 prénoms
    * SOIT
        * SI paramètre *Prénom différent donne ambigu au lieu de connu* est activé ALORS
            * Seule la correspondance **exacte** d'un des 3 prénoms sera vérifiée
        * SINON
            * correspondance exacte du nom (Si nom composé recherche dans les deux sens)
            * ET
                * SI le prénom comporte au moins 3 lettres ALORS
                    * correspondance des 3 premières lettres du premier prénom
                * SINON
                    * Uniquement la correspondance exacte d'un des 3 prénoms

Le nombre de réponses renvoyées (N) détermine les actions à prendre :

* N > 1 : le résultat est **ambigu**, la recherche est terminée.
* N = 1 : le caractère récupérable ou non de l'aide est remonté, la recherche est terminée.
* N = 0 : on passe à l'étape 2

## Étape 2

La requête de l'étape 1 est renvoyée, **mais** la date de naissance est comparée en fonction du paramètre de date et les
premières lettres du prénom sont prises en compte **même** avec le paramètre *Prénom différent donne ambigu au lieu de
connue* activé.

| SI paramètre activé                      | ALORS                                                                                       |
|:-----------------------------------------|:--------------------------------------------------------------------------------------------|
| *Jour et mois différents donnent ambigu* | L’année saisie doit correspondre exactement à celle présente en base (paramètre par défaut) |
| *Jour différent donne ambigu*            | Le mois et l’année saisis doivent correspondre exactement à ceux présents en base           |
| *Mois différent donne ambigu*            | Le jour et l’année saisis doivent correspondre exactement à ceux présents en base           |
| *Année différente donne ambigu*          | Le jour et le mois saisis doivent correspondre exactement à ceux présents en base           |
| *Date différente donne   ambigu*         | La date de naissance complète est ignorée                                                   |

Le nombre de réponses renvoyées (N) détermine les actions à prendre :

* N >= 1 : le résultat est **ambigu**, la recherche est terminée.
* sinon : on passe à l'étape 3

## Étape 3

La requête de l'étape 1 est renvoyée (la date de naissance est exacte), mais sans aucun prénom :

* N >= 1 : le résultat est **ambigu**, la recherche est terminée.
* sinon : le résultat est **inconnu**, la recherche est terminée.

## Recherche Soundex

### Étape 1 - Soundex

La recherche Soundex d'une personne s'effectue exactement de la même manière qu'une recherche "classique". Seules les
conditions de l'étape 1 sont différentes (conditions utilisées pour les 3 étapes)

La recherche est lancée avec les conditions suivantes :

* `code_aide` est non null
* ET correspondance exacte de la date de naissance
* ET
    * SOIT
        * correspondance Soundex du nom (si nom composé recherche dans les deux sens : "NomA NomB" et "NomB NomA")
        * ET correspondance Soundex d'un des 3 prénoms
    * SOIT
        * SI paramètre *Prénom différent donne ambigu au lieu de connu* est activé ALORS
            * Seule la correspondance d'un des 3 prénoms sera vérifiée. Le paramètre **Soundex n'est pas pris en compte
              pour les prénoms**. Ceci peut fausser le résultat attendu en cas de prénoms proches phonétiquement.
        * SINON
            * correspondance Soundex du nom (Si nom composé recherche dans les deux sens)
            * ET correspondance des 3 premières lettres du premier prénom

## Exemples de recherche

* [Exemple de cheminements de recherche](Exemples_de_cheminements_de_recherche.md)

## Recherche d'individus avec des aides multiples

Un individu est représenté par son `num_ind` et celui-ci n'étant pas unique il est alors relié à plusieurs
enregistrements dans la base de données. Ces différentes lignes sont semblables en tout point sauf au niveau
du `code_aide`.

C'est par ce mécanisme d'enregistrement que nous stockons plusieurs `code_aide` par individu dans la base de données.

Le tableau suivant réparti les différents cas possibles d'aides multiples. Ils sont chacun utilisés lors de tests
automatisés.

À noter que le cas A n'a pas d'existence technique à l'heure actuelle.

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
|  A  |      0      |        0        |           0            |
| B1  |      1      |        1        |           0            |
| B2  |      2      |        1        |           0            |
| B3  |      1      |        2        |           0            |
| B4  |      2      |        2        |           0            |
| C1  |      0      |        1        |           0            |
| C2  |      0      |        2        |           0            |
| D1  |      1      |        0        |           0            |
| D2  |      2      |        0        |           0            |
| E1  |      0      |        0        |           1            |
| E2  |      0      |        0        |           2            |
| F1  |      1      |        1        |           1            |
| F2  |      2      |        2        |           1            |
| F3  |      1      |        2        |           1            |
| F4  |      2      |        1        |           1            |
| F5  |      1      |        1        |           2            |
| F6  |      2      |        2        |           2            |
| F7  |      1      |        2        |           2            |
| F8  |      2      |        1        |           2            |
| G1  |      0      |        1        |           1            |
| G2  |      0      |        2        |           1            |
| G3  |      0      |        1        |           2            |
| G4  |      0      |        2        |           2            |
| H1  |      1      |        0        |           1            |
| H2  |      2      |        0        |           1            |
| H3  |      1      |        0        |           2            |
| H4  |      2      |        0        |           2            |

### Étape 1 - aides multiples

L'algorithme de recherche ne diffère pas de celui de base. Il a simplement été modifié au niveau du résultat de
recherches, pour ne renvoyer qu'un nombre d'individus distinct lié à un numéro d'individu précis.

Cela signifie que deux enregistrements ayant le même numéro d'individu seront alors considérés comme une seule et même
personne par l'algorithme.

Les cas d'exemples de recherches d'aides multiples sont répertoriées dans le
fichier [Exemples de recherches avec aides multiples](Exemples_de_recherche_aides_multiples.md).
