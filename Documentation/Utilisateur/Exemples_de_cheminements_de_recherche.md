# Exemples de cheminements de recherche

Les exemples sont numérotés `Enn` (`nn` étant un entier) afin de les identifier rapidement.

## E01 Recherche exacte

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 06/12/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E01 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 seul résultat trouvé, la Personne est **connue**

## E02 Recherche exacte avec nom composé saisie dans le mauvais ordre lors de la recherche

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Gaël
* Nom d'usage : PAYSAA-CAMPO
* Date de naissance : 16/04/2008

En base de données est présent :

| NOM          | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-------------|:--------|:--------|:--------|------------------:|----------:|
| CAMPO-PAYSAA | Gaël    |         |         |        16/04/2008 | 1SEXTNONR |

### E02 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '16/04/2008'
NOM = ('PAYSAA CAMPO' || 'PAYSAA-CAMPO' || 'CAMPO PAYSAA' || 'CAMPO-PAYSAA')
(PRENOM1 = 'Gaël' || PRENOM2 = 'Gaël' || PRENOM3 = 'Gaël') OU (PRENOM1 LIKE 'Gae%')
```

=> 1 seul résultat trouvé, la Personne est **connue**

## E03 Recherche avec prénom inexact et le paramètre *Prénom différent donne ambigu au lieu de

connu* est désactivé - Cas 1

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : A
* Nom d'usage : OLMO
* Date de naissance : 06/12/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E03 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'A' || PRENOM2 = 'A' || PRENOM3 = 'A') OU (PRENOM1 LIKE 'A%')
```

=> 1 seul résultat trouvé, la Personne est **connue**

## E04 Recherche avec prénom inexact et le paramètre *Prénom différent donne ambigu au lieu de

connu* est désactivé - Cas 2

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Jeanine
* Nom d'usage : MILLINER
* Date de naissance : 01/01/1930

En base de données est présent :

| NOM      | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:---------|:--------|:--------|:--------|------------------:|----------:|
| MILLINER | Jeanne  |         |         |        01/01/1930 |  1SEXTREC |

### E04 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'MILLINER'
(PRENOM1 = 'Jeanine' || PRENOM2 = 'Jeanine' || PRENOM3 = 'Jeanine') OU (PRENOM1 LIKE 'Jea%')
```

=> 1 seul résultat trouvé, la Personne est **connue**

## E05 Recherche avec prénom inexact et le paramètre *Prénom différent donne ambigu au lieu de connu* est activé - Cas 1

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : A
* Nom d'usage : OLMO
* Date de naissance : 06/12/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E05 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'A' || PRENOM2 = 'A' || PRENOM3 = 'A')
```

=> aucun résultat trouvé

### E05 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '1977'
NOM = 'OLMO'
(PRENOM1 = 'A' || PRENOM2 = 'A' || PRENOM3 = 'A') OU (PRENOM1 LIKE 'A%')
```

=> 1 résultat trouvé, recherche **ambigüe**

## E06 Recherche avec prénom inexact et le paramètre *Prénom différent donne ambigu au lieu de connu* est activé - Cas 2

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Jeanine
* Nom d'usage : MILLINER
* Date de naissance : 01/01/1930

En base de données est présent :

| NOM      | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:---------|:--------|:--------|:--------|------------------:|----------:|
| MILLINER | Jeanne  |         |         |        01/01/1930 |  1SEXTREC |

### E06 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'MILLINER'
(PRENOM1 = 'Jeanine' || PRENOM2 = 'Jeanine' || PRENOM3 = 'Jeanine')
```

=> aucun résultat trouvé

### E06 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '1977'
NOM = 'MILLINER'
(PRENOM1 = 'Jeanine' || PRENOM2 = 'Jeanine' || PRENOM3 = 'Jeanine') OU (PRENOM1 LIKE 'Jea%')
```

=> 1 résultat trouvé, recherche **ambigue**

## E07 Recherche avec plusieurs prénoms

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Mickaël
* Prénom 2 : Aimé
* Nom d'usage : OLMO
* Date de naissance : 06/12/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E07 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU  (PRENOM1 LIKE 'Mic%')
```

=> 1 seul résultat trouvé, la Personne est **connue**

## E08 Recherche avec plusieurs résultats

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/05/2020
* Lieu de décès : GUINGAMP
* Date de l'acte de décès : 12/05/2020
* Prénom : Louis
* Nom d'usage : DURAND
* Date de naissance : 03/01/1911

En base de données est présent :

| NOM    | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-------|:--------|:--------|:--------|------------------:|----------:|
| DURAND | Louis   |         |         |        03/01/1911 |  1SEXTREC |
| DURAND | Louise  |         |         |        03/01/1911 |  1SEXTREC |

### E08 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '03/01/1911'
NOM = 'DURAND'
(PRENOM1 = 'Louis' || PRENOM2 = 'Louis' || PRENOM3 = 'Louis') OU (PRENOM1 LIKE 'Lou%')
```

=> 2 résultats trouvés, recherche **ambigue**

## E09 Recherche date de naissance inexacte, résultat ambigu

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 07/08/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E09 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '07/08/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé

### E09 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, recherche **ambigue**

## E10 Recherche non trouvée

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Mickaël
* Nom d'usage : DUPONT
* Date de naissance : 07/08/1986

### E10 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '07/08/1986'
NOM = 'DUPONT'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 LIKE 'Mic%')
```

=> aucun résultat trouvé

### E10 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '1986'
NOM = 'DUPONT'
(PRENOM1 = 'Mickaël' || PRENOM2 = 'Mickaël' || PRENOM3 = 'Mickaël') OU (PRENOM1 LIKE 'Mic%')
```

=> aucun résultat trouvé

### E10 Étape 3

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '07/08/1986'
NOM = 'DUPONT'
```

=> aucun résultat trouvé, la personne est **inconnue**

## E11

Paramètre *Jour et mois de naissance différents donne ambigu au lieu d'inconnu* selectionné (paramètre par défaut), Seul
l'année est correcte dans la saisie de la date de naissance

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 01/01/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E11 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '01/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé, on passe à l'étape suivante

### E11 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, la réponse est **ambigu**

## E12 Paramètres : Seuls le mois et l'année sont corrects dans la saisie de la date de naissance

Paramètres :

* *Jour de naissance différent donne ambigu au lieu d'inconnu* selectionné
* ou *Jour et mois de naissance différents donne ambigu au lieu d'inconnu* selectionné (paramètre par défaut),

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 01/12/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E12 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '01/12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé

### E12 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '12/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, la réponse est **ambigu**

## E13

Paramètre *Mois de naissance différent donne ambigu au lieu d'inconnu* selectionné ou *Jour et mois de naissance
différents donne ambigu au lieu d'inconnu* selectionné (paramètre par défaut), Seul le jour et le mois sont corrects
dans la saisie de la date de naissance

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 06/01/1977

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E13 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/01/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé

### E13 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/**/1977'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, la réponse est **ambigu**

## E14

Paramètre *Année de naissance différente donne ambigu au lieu d'inconnu* selectionné, Seul le jour et le mois sont
corrects dans la saisie de la date de naissance

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 06/12/1976

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E14 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/1976'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé

### E14 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '06/12/****'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, la réponse est **ambigu**

## E15

Paramètre *Date de naissance différente donne ambigu au lieu d'inconnu* selectionné, La date de naissance saisie est
totalement différente

Vous trouverez ci-dessous le cheminement de la recherche suivante :

* Date de décès : 10/02/2020
* Lieu de décès : VILLEFRANCHE
* Date de l'acte de décès : 12/02/2020
* Prénom : Aimé
* Nom d'usage : OLMO
* Date de naissance : 01/01/1901

En base de données est présent :

| NOM  | PRENOM1 | PRENOM2 | PRENOM3 | DATE_DE_NAISSANCE | CODE_AIDE |
|:-----|:--------|:--------|:--------|------------------:|----------:|
| OLMO | Aimé    |         |         |        06/12/1977 |  1SEXTREC |

### E15 Étape 1

La recherche suivante est lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
DATE_DE_NAISSANCE = '01/01/1901'
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> aucun résultat trouvé

### E15 Étape 2

La recherche suivante est ensuite lancée sur la base de données (conditions ET) :

```text
CODE_AIDE != NULL
NOM = 'OLMO'
(PRENOM1 = 'Aimé' || PRENOM2 = 'Aimé' || PRENOM3 = 'Aimé') OU (PRENOM1 LIKE 'Aim%')
```

=> 1 résultat trouvé, la réponse est **ambigüe**
