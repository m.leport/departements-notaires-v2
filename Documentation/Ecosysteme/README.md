# Écosystème D&N

Départements & Notaires (D&N) est un extranet permettant d'apporter une réponse en temps réel aux études notariales
chargées d’une succession et s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide
sociale.

Nous décrivons ici le fonctionnement de l'écosystème D&N.

## Quels sont les acteurs de l'écosystème ?

- Les conseils départementaux, à qui D&N permet un gain de temps
  substantiel (entre 1 et 2 équivalents-temp-plein par an).
- Les notaires, utilisateurs directs de l'extranet Départements & Notaires.
- l'ADULLACT, association loi 1901 de collectivités, dont l'objet est l'économie d'argent public grâce au logiciel
  libre. L'ADULLACT est l'animateur de la communauté D&N.
- Les prestataires de service, proposant installation, support, évolutions, etc.
- *Le* prestataire de service choisi par le Groupe de Travail Collaboratif (GTC)

## Quel est le rôle de l'ADULLACT ?

L'ADULLACT assume les rôles suivants :

- Aider les collectivités à économiser de l'argent public grâce au logiciel libre.
- Présenter le logiciel (enjeux) et réaliser des démonstrations.
- Animer les GTC (ordre du jour, animation le jour J, rédaction des comptes-rendus).
- Coordonner les développements afin de conserver une cohérence pour tous, et éviter les divergences (*fork*).

## Qu'est-ce que le Groupe de Travail Collaboratif (GTC) ?

Le GTC est une réunion annuelle dont l'objet est de :

- partager de l'expérience entre pairs,
- définir la feuille de route du logiciel pour l'année à venir.

Le GTC est composé de collectivités (typiquement les conseils départementaux), du prestataire choisi, et de l'ADULLACT
dans le rôle d'animateur.

Les [comptes-rendus de GTC](https://adullact.org/departements-et-notaires) sont publiés sur le site de l'ADULLACT.

## Qui décide de la feuille de route du logiciel D&N ?

La feuille de route est discutée en GTC. Les fonctionnalités à choisir pour l'année à venir font l'objet d'un vote.

## Qui vote au GTC ?

Seules les collectivités adhérentes à l'ADULLACT ont le droit de vote. (Les autres collectivités sont les bienvenues au
GTC, mais ne peuvent pas voter.)

## Comment est organisé le vote en GTC ?

Chaque département dispose de 100 points, à affecter aux fonctionnalités ayant le plus d'importance aux yeux de chacun
des départements.

Les fonctionnalités ayant reçu le plus de points sont celles qui seront réalisées l'année à venir.

## Qui paye pour la réalisation des fonctionnalités votées en GTC ?

C'est l'association ADULLACT.

## Qui réalise les fonctionnalités votées en GTC ?

C'est le prestataire qui a été choisi en GTC (par les conseils départementaux donc). En 2022 (et pour 3 ans), il s'agit
de l'entreprise AtolCD.

## La fonctionnalité dont j'ai vraiment besoin n'a pas recueilli assez de votes, comment faire ?

Un conseil départemental peut réaliser à ses frais une fonctionnalité, soit en interne
ou soit [en mandatant un prestataire](https://comptoir-du-libre.org/fr/softwares/115).

Dans ce cas, il est vivement recommandé d'inclure l'ADULLACT dans les échanges, afin d'éviter une divergence (*fork*).
Cela rend fort·e, épanoui·e et économise de l'argent public :)

## Où est installé D&N

Départements & Notaires est installé sur les serveurs du département. Chaque département configure son instance D&N
selon ses besoins propres.

## Je rencontre une anomalie sur D&N, qui qualifie le bug ?

Une anomalie peut avoir pour origine classiquement une configuration erronée (spécifique à l'installation dans le
département concerné) ou alors un bug du logiciel.

Cette détermination peut se faire par le département en interne ou à l'aide d'un prestataire (du choix du département).

Un bug doit être reproductible.

## Qu'est-ce qu'un bug reproductible

Afin d'être corrigé, un bug nécessite d'être reproduit. Pour cela, il est demandé de décrire les étapes pour reproduire
le bug.

Pour des anomalies sur le moteur de recherche, le bug doit être reproduit avec un individu
des [données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
(plus de 900 individus fictifs à mi-2023). Si les individus fictifs n'étaient pas suffisants à reproduire une anomalie,
il est possible d'en ajouter.

## Qui corrige mon bug ?

Le département en interne ou un prestataire (du choix du département).

## Qui intègre la correction du bug dans le tronc commun ?

Le prestataire choisi par le GTC, piloté par l'ADULLACT.


