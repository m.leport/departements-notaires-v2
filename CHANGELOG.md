# Changelog

All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 2.6.0 - 2024-07-15 ==========================================================

### Added (2.6.0)

Améliorations fonctionnelles :

* Ajout d'un paramètre pour désactiver les alertes "documents déposés"
  [#304](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/304)
* Ajout d'un paramètre pour filtrer plus finement les instructeurs sur les libellés d'aides
  [#303](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/303)
* Ajout d'un paramètre pour inclure une signature scannée (format image) dans les courriels envoyés
  [#301](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/301)
* Affichage un message de service sur la page d'accueil (Indisponibilités du service, …)
  [#270](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/270)

Amélioration de l'interface utilisateur :

* Permettre à l'utilisateur de voir son mot de passe (icône "œil")
  [#274](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/274)
* Dupliquer les boutons d'ajout en haut de liste (Administration)
  [#271](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/271)
* Filtres des recherches : ajout d'un bouton "réinitialiser"
  [#275](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/275)
* Améliorer texte et lien pour déverrouiller un compte bloqué
  [#273](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/273)

### Changed (2.6.0)

Pour les utilisateurs :

* Accepter plusieurs notaires ayant le même mail (Cas d'une étude ayant 3 numéros CRPCEN)
  [#276](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/276)

Pour les opérateurs :

* La version requise de PHP est désormais la 8.1
  [#307](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/307)

### Fixed (2.6.0)

* Mise à disposition de la totalité des champs dans les modèles de courriel
  [#232](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/232),
  [#219](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/219)
* Dans la liste des recherches, le filtre "date de fin" ne remontait pas la journée sélectionnée
  [#302](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/302)
* Tri incohérent dans les listes
  [#186](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/186),
  [#155](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/155),
  [#156](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/156)
* Les recherches sur les noms composés, les prénoms avec ou sans tiret, ont été corrigés dans la version précédente
  v2.5.1
  [#220](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/220)
  [#116](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/116)


## 2.6.0-beta.2 - 2024-07-09 ===================================================

### Added (2.6.0-beta.2)

Améliorations fonctionnelles :

* Ajout d'un paramètre pour désactiver les alertes "documents déposés"
  [#304](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/304)
* Ajout d'un paramètre pour filtrer plus finement les instructeurs sur les libellés d'aides
  [#303](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/303)
* Ajout d'un paramètre pour inclure une signature scannée (format image) dans les courriels envoyés
  [#301](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/301)
* Affichage un message de service sur la page d'accueil (Indisponibilités du service, …)
  [#270](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/270)

Amélioration de l'interface utilisateur :

* Permettre à l'utilisateur de voir son mot de passe (icône "œil")
  [#274](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/274)
* Dupliquer les boutons d'ajout en haut de liste (Administration)
  [#271](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/271)
* Filtres des recherches : ajout d'un bouton "réinitialiser"
  [#275](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/275)
* Améliorer texte et lien pour déverrouiller un compte bloqué
  [#273](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/273)

### Changed (2.6.0-beta.2)

Pour les utilisateurs :

* Accepter plusieurs notaires ayant le même mail (Cas d'une étude ayant 3 numéros CRPCEN)
  [#276](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/276)

Pour les opérateurs :

* La version requise de PHP est désormais la 8.1
  [#307](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/307)

### Fixed (2.6.0-beta.2)

* Mise à disposition de la totalité des champs dans les modèles de courriel
  [#232](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/232),
  [#219](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/219)
* Dans la liste des recherches, le filtre "date de fin" ne remontait pas la journée sélectionnée
  [#302](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/302)
* Tri incohérent dans les listes
  [#186](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/186),
  [#155](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/155),
  [#156](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/156)
* Les recherches sur les noms composés, les prénoms avec ou sans tiret, ont été corrigés dans la version précédente
  v2.5.1
  [#220](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/220)
  [#116](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/116)

## 2.5.1 - 2023-11-28 ==========================================================

### Added (2.5.1)

* Dans les PDF générés, le nom de l'étude et son adresse postale sont désormais disponibles et
  affichés par défaut en haut à droite de celui-ci
  [#261](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/261)
* Le nom du PDF généré comporte désormais l'identifiant de la personne si le résultat de la recherche est connue
  [#262](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/262)
* Possibilité de choisir le fuseau horaire dans l'application (valeur par défaut : `Europe/Paris`)
  [#283](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/283)
* La date de décès *recherchée* (celle saisie par une étude notariale) est désormais disponible dans les modèles (PDF et
  courriel) sous le nom `deathDateSearch`
  [#290](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/290)

Pour les développeuses :

* CI : téléversement automatique de l'archive construite dans le dépôt Gitlab de paquets génériques.

### Fixed (2.5.1)

* Si l'application possède une URL publique (visible sur internet) **et** une URL privée (visible en intranet) :
  Utilisation de l'URL d'accès pouvant être saisie dans les paramètres administrateurs. Cette URL sera utilisée
  dans les e-mails envoyés aux notaires. Si cette URL n'est pas renseignée, l'application continuera
  à fonctionner comme précédemment.
  [#284](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/284)
* Si les créances sont activées et que la personne connue n'a pas de date de décès renseignée, alors
  aucun PDF ne sera généré, ni dans les pièces jointes au mail, ni dans la liste des recherches (en lien avec #191)
  [#289](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/289)
* Une recherche peut contenir un nom d'usage et un nom commun et que seulement l'un deux soit un nom composé
  [#280](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/280)
* Correction d'un bug sur l'algorithme de recherche avec option "Date différente donne ambigu au lieu d'inconnu"
  [#288](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/288)

### Changed (2.5.1)

Pour les dévelopeurs :

* refactorisation des Dockers de dev (dossier `docker/`)

### Security (2.5.1)

* Ajout d'un jeton CSRF dans le formulaire mot de passe oublié
  [#281](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/281)
* Dans le modèle, une balise `<script>` est remplacée par un chargement de fichier Javascript afin de s'affranchir
  de `unsafe-inline` dans les CSP (Content Security Policy)
  [#282](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/282)

## 2.5.1-beta.3 - 2023-11-27 ===================================================

### Added (2.5.1-beta.3)

Pour les développeuses :

* CI : correction téléversement automatique

## 2.5.1-beta.2 - 2023-11-27 ===================================================

### Added (2.5.1-beta.2)

Pour les développeuses :

* CI : téléversement automatique de l'archive construite dans le dépôt Gitlab de paquets génériques.

## 2.5.1-beta.1 - 2023-11-22 ===================================================

### Added (2.5.1-beta.1)

* Dans les PDF générés, le nom de l'étude et son adresse postale sont désormais disponibles et
  affichées par défaut en haut à droite de celui-ci
  [#261](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/261)
* Le nom du PDF généré comporte désormais l'identifiant de la personne si le résultat de la recherche est connue
  [#262](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/262)
* Possibilité de choisir le fuseau horaire dans l'application (valeur par défaut : `Europe/Paris`)
  [#283](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/283)
* La date de décès *recherchée* (celle saisie par une étude notariale) est désormais disponible dans les modèles (PDF et
  courriel) sous le nom `deathDateSearch`
  [#290](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/290)

### Fixed (2.5.1-beta.1)

* Si l'application possède une URL publique (visible sur internet) **et** une URL privée (visible en intranet) :
  Utilisation de l'URL d'accès pouvant être saisie dans les paramètres administrateurs. Cette URL sera utilisée
  dans les e-mails envoyés aux notaires. Si cette URL n'est pas renseignée, l'application continuera
  à fonctionner comme précédemment.
  [#284](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/284)
* Si les créances sont activées et que la personne connue n'a pas de date de décès renseignée, alors
  aucun PDF ne sera généré, ni dans les pièces jointes au mail, ni dans la liste des recherches (en lien avec #191)
  [#289](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/289)
* Une recherche peut contenir un nom d'usage et un nom commun et que seulement l'un deux soit un nom composé
  [#280](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/280)
* Correction d'un bug sur l'algorithme de recherche avec option "Date différente donne ambigu au lieu d'inconnu"
  [#288](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/288)

### Changed (2.5.1-beta.1)

Pour les dévelopeurs :

* refactorisation des Dockers de dev (dossier `docker/`)

### Security (2.5.1-beta.1)

* Ajout d'un jeton CSRF dans le formulaire mot de passe oublié
  [#281](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/281)
* Dans le modèle, une balise `<script>` est remplacée par un chargement de fichier Javascript afin de s'affranchir
  de `unsafe-inline` dans les CSP (Content Security Policy)
  [#282](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/282)

## 2.5.0 - 2023-09-01 ==========================================================

(Cette entrée cumule toutes les informations des versions beta de la 2.5.0)

### Added (2.5.0)

* Les notaires peuvent téléverser des documents aidant à l'identification de la personne bénéficiaire ("échange
  documentaire")
  [#121](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/121),
  [#245](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/245),
  [#246](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/246),
  [#247](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/247),
  [#248](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/248),
  [#249](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/249),
  [#250](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/250),
* Aiguiller les dossiers vers un instructeur selon le *nom* de l'aide
  [#257](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/257)

### Changed (2.5.0)

* Amélioration de la conformité CNIL / RGPD : les administrateurs n'ont accès qu'aux fonctionnalités d'administration
  et plus aux fonctionnalités métier
  [#241](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/241)
* Amélioration de la recherche sur les noms composés (NomA-NomB)
  [#124](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/124)
  [#251](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/251)
* Interface Utilisateur : les tableaux utilisent toute la largeur de l'écran
  [#244](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/244)
* La
  [documentation pour les administrateurs](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Administrateur)
  est désormais distincte de
  [celle pour les opérateurs techniques](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur)

### Fixed (2.5.0)

* Dans les PDF générés, le nom de l'étude et la date de recherche sont désormais les bons
  [#187](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/187)
  [#252](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/252)
  [#253](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/253)
* Amélioration de l'expérience utilisateur de l'interface d'admin : en cas d'erreur, une mention est présentée en haut
  de page, en plus du champ en erreur.

## 2.5.0-beta.2 - 2023-08-31

### Fixed (2.5.0-beta.2)

* Amélioration de l'expérience utilisateur de l'interface d'admin : en cas d'erreur, une mention est présentée en haut
  de page, en plus du champ en erreur.
* Les pièces jointes téléversées par les notaires sont maintenant consultables par les instructeurs.
* Les pièces jointes sont bien supprimées après le délai d'expiration.

## 2.5.0-beta.1 - 2023-08-03

### Added (2.5.0-beta.1)

* Les notaires peuvent téléverser des documents aidant à l'identification de la personne bénéficiaire ("échange
  documentaire")
  [#121](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/121),
  [#245](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/245),
  [#246](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/246),
  [#247](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/247),
  [#248](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/248),
  [#249](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/249),
  [#250](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/250),
* Aiguiller les dossiers vers un instructeur selon le *nom* de l'aide
  [#257](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/257)

### Changed (2.5.0-beta.1)

* Amélioration de la conformité CNIL / RGPD : les administrateurs n'ont accès qu'aux fonctionnalités d'administration
  et plus aux fonctionnalités métier
  [#241](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/241)
* Amélioration de la recherche sur les noms composés
  [#124](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/124)
  [#251](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/251)
* Interface Utilisateur : les tableaux utilisent toute la largeur de l'écran
  [#244](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/244)
* La
  [documentation pour les administrateurs](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Administrateur)
  est désormais distincte de
  [celle pour les opérateurs techniques](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur)

### Fixed (2.5.0-beta.1)

* Dans les PDF générés, le nom de l'étude et la date de recherche sont désormais les bons
  [#187](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/187)
  [#252](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/252)
  [#253](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/253)

## 2.4.0 - 2023-04-25 ==================================================================================================

(Cette entrée cumule toutes les informations des versions rc de la 2.4.0)

### Added (2.4.0)

* [#205 Ajout des instructeurs de territoires](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/205).
  Voir
  [Documentation des Instructeurs de Territoire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Instructeur_de_territoire.md)

### Fixed (2.4.0)

* [#136 Déverrouillage impossible](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/136)
* [#209 Erreur sur les stats avec une base vide](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/209)
* [#233 Modèles : CRÉANCE Permettre la visualisation de
  *toutes* les créances dans les mails](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/233)
* [#234 Modèles : CRÉANCE Correction des mails en cours d'instruction](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/234)

### Security (2.4.0)

* Passage à Symfony 5.4 et PHP 7.4

## 2.4.0-rc.2 - 2023-04-24

### Fixed (2.4.0-rc.2)

* [#239 Les informations IDT ne s'affiche pas pour les cas en cours d'instructions](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/239)
* [#236 Jeu de données IdT : Marina Loiseau, placer prénom et nom dans les bonnes colonnes](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/236)
* [#230 Documentation Fonctionnalité Instructeur de Territoire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/230)

## 2.4.0-rc.1 - 2023-04-17

### Added (2.4.0-rc.1)

* [#205 Ajout des instructeurs de territoires](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/205)

### Fixed (2.4.0-rc.1)

* [#136 Déverrouillage impossible](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/136)
* [#209 Erreur sur les stats avec une base vide](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/209)
* [#233 Modèles : CRÉANCE Permettre la visualisation de
  *toutes* les créances dans les mails](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/233)
* [#234 Modèles : CRÉANCE Correction des mails en cours d'instruction](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/234)

### Security (2.4.0-rc.1)

* Passage à Symfony 5.4 et PHP 7.4

## 2.3.2 - 2023-02-14 ==================================================================================================

### Fixed (2.3.2)

* [#217 Modèles : Permettre la personnalisation de
  *tous* les fichiers](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/217)

## 2.3.1 - 2023-01-24 ==================================================================================================

### Fixed (2.3.1)

* [#216 Modèle : Formule de politesse présente deux fois](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/216)
* [#207 Communication de la créance : test d'acceptation 8.1-06 ne passe pas](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/?sort=updated_desc&state=opened&milestone_title=v2.3.1&first_page_size=20)
* [#206 Correction du numéro de version](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/206)

## 2.3.0 - 2022-12-16 ==================================================================================================

(Cette entrée cumule toutes les informations des versions beta de la 2.3.0)

### Added (2.3.0)

* Communication d'une estimation du montant de la
  créance [#173](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/173)

Pour les opérateurs :

- Ajout de plusieurs paramètres dans le backoffice permettant la gestion des créances :
    - activation de la gestion des créances
    - seuil des créances récupérables
    - seuil des créances non récupérables
    - mail du service des créances
    - mail du service de gestion
- Ajout d'un nouvel import pour l'ajout de créances, documenté
  dans [Opérateur > Import créances](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Import-creances.md)
- Ajout d'un nouveau modèle de
  mail [`claims.html.twig`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/templates/default/emails/claims.html.twig)
  appelé depuis le modèle `search_found.html.twig`

Pour les développeurs :

- Ajout de nouvelles tables `creance` et `creances_individus`.
- Nouveaux tests E2E Cypress portant sur les
  créances : [`9-claims.spec.js`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/tests/E2E/cypress/integration/FullDB/9-claims.spec.js)
- Ajout de nouvelles entrées dans le jeu de test (fichiers `individus.csv` et `creances.csv`).

## 2.3.0-beta.3 - 2022-12-12

### Added (2.3.0-beta.3)

Pour les utilisateurs :

* Ajout du nouvel affichage des créances également dans les modèles de
  pdf [`search_found_notrec.html.twig`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/templates/default/pdf/search_found_notrec.html.twig)
  et [`search_found_rec.html.twig`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/templates/default/pdf/search_found_rec.html.twig)
  en fonction des mêmes conditions que pour les mails.

## 2.3.0-beta.2 - 2022-12-05

### Fixed (2.3.0-beta.2)

* Améliorations des tests E2E
* DOC : ajout d'un arbre de décision présentant les différents cas de figure des créances

## 2.3.0-beta.1 - 2022-11-28

### Added (2.3.0-beta.1)

Pour les utilisateurs :

* Communication d'une estimation du montant de la
  créance [#173](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/173)

Pour les opérateurs :

- Ajout de plusieurs paramètres dans le backoffice permettant la gestion des créances :
    - activation de la gestion des créances
    - seuil des créances récupérables
    - seuil des créances non récupérables
    - mail du service des créances
    - mail du service de gestion
- Ajout d'un nouvel import pour l'ajout de créances, documenté
  dans [Opérateur > Import créances](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Import-creances.md)
- Ajout d'un nouveau modèle de
  mail [`claims.html.twig`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/templates/default/emails/claims.html.twig)
  contenant un lot de modèle par défaut en fonction de conditions de créance. Ce modèle est appelé depuis le modèle
  `search_found.html.twig` (dans le cas où la fonctionnalité "créances" est activée).

Pour les développeurs :

- Ajout de nouvelles tables `creance` et `creances_individus`.
- Nouveaux tests Cypress portant sur les
  créances : [`9-claims.spec.js`](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/appli_sf/tests/E2E/cypress/integration/FullDB/9-claims.spec.js)
- Ajout de nouvelles entrées dans les fichiers`individus.csv` et `creances.csv`.

### Changed (2.3.0-beta.1)

Pour les opérateurs :

- Les modèles `search_found.twig.html` et `search_ambigous.html.twig` ont été modifiés pour prendre en compte la
  fonctionnalité "créance"

## 2.2.0 - 2022-09-22 ==================================================================================================

(Cette entrée cumule toutes les informations des versions beta de la 2.2.0)

### Added (2.2.0)

#### Added (2.2.0) : Pour les utilisateurs

- Aides multiples : Gestion des individus avec un code d’aide vide (en cours d’instruction)
  [#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149)
- Aides multiples : Différencier les courriers selon le type d’aide
  [#148](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/148)
- Extraction CSV à partir de la consultation des recherches
  [#137](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/137)
- Personnalisation signature mail métier depuis le backoffice
  [#128](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/128)

#### Added (2.2.0) : Pour les opérateurs

- Aides multiples : gérer les bénéficiaires de plusieurs aides à l'import
  [#147](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/147)
- Pouvoir personnaliser le *sujet* d’un mail
  [#127](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/127)
- Mails et PDF : disposer dans les *templates* de balise pour tous les champs de la BD
  [#129](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/129)
- Logguer les requêtes SQL des recherches
  [#151](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/151)
- Migration v1/v2 importer historique de recherche + statistiques
  [#83](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/83)

#### Added (2.2.0) : Pour les développeurs

- Création d'un fichier pour le suivi des tests d'acceptation à lancer manuellement
  [Tests_acceptations_manuels.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Developpeur/Tests_acceptation/Tests_acceptations_manuels.md)

### Changed (2.2.0)

#### Changed (2.2.0) : Pour les utilisateurs

Modification des statistiques : le nombre de demandes n'est plus égal au nombre de réponses, du fait des aides
multiples (une demande peut désormais contenir plusieurs réponses).

#### Changed (2.2.0) : Pour les opérateurs

:warning: Merci de prendre connaissance du fichier
[Documentation > Opérateur > Mise à jour](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/mise-a-jour.md)

* Les fichiers de *template* suivants ont été renommés :
    - `search_find.html.twig` => `search_found.html.twig`
    - `search_notfind.html.twig` => `search_notfound.html.twig`,
    - `search_find_rec.html.twig` => `search_found_rec.html.twig`
    - `search_find_notrec.html.twig` => `search_found_notrec.html.twig`
    - Ce changement est aussi valable sur toutes les variables et constantes y faisant référence.
* Pour plus de simplicité quant au traitement des différents statuts d'aides, et plus de clarté quant au nommage des
  différents templates de mail, le template `search_hasrequest.html.twig` a été fusionné avec `search_found.html.twig`.
  Le traitement particulier des demandes en *cours d'instruction* n'a plus de sens d'être séparé des autres demandes.
  De ce fait, les personnes *en cours d'instruction* sont désormais reconnues comme **connues**.

#### Changed (2.2.0) : Pour les développeurs

* Le champ `num_ind` de la table `individu` **n'est plus** considéré comme unique, et ne comporte plus d'index
  unique à son nom. Ce changement implique qu'il est désormais possible de stocker plusieurs lignes par individu, et
  donc plusieurs `code_aide`.
* Le champ `type_reponse` dans la table `log_recherche` a **changé de type**. En effet, afin de permettre la gestion
  des aides multiples, il est nécessaire que nous puissions stocker toutes les valeurs de types de réponses associées
  à une recherche. Le champ `type_reponse` n'est donc plus un `int` mais un type `json`. Il aura le format
  `[1,1,2,5]` ou `1,1,2,5` représente un code d'aide associé à la recherche. Pour permettre la migration des anciennes
  valeurs stockées dans la table `log_recherche` au format `int` un `UPDATE SET` a été fait dans la migration relative
  à ce développement pour permettre la transformation. Toutes les valeurs seront alors transformées (ex: `1` => `[1]`).
* Pour un meilleur suivi le fichier de test cypress `4-search.spec.js` a été divisé en deux :
    - `4.1-search.spec.js` pour les tests sur les aides uniques
    - `4.2-search-multiple-helpcode.spec.js` pour les aides multiples.

### Fixed (2.2.0)

- Formulaire de recherche : utilisation coche obligatoire conforme à la v1
  [#80](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/80)
- Recherche sur prénom composé sans ou sans tiret ; en base, prénom AVEC tiret (Marie-Thérèse)
  [#122](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/122)
- Recherche sur prénom composé sans ou sans tiret ; en base, prénom sans tiret (Marie Thérèse)
  [#123](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/123)
- Message d'erreur quand on supprime le mail destinataire "alerte décès"
  [#140](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/140)
- Erreurs techniques non bloquantes dans les logs de l'application `/stats`
  [#118](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/118)
- Correction sur le nom du fichier PDF (bug lié au test d'intrusion)
  [#93](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/93)
- Erreurs techniques non bloquantes dans les logs de l'application `/admin/params`
  [#117](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/117)
- Suppression logos présents sur le PDF impossible
  [#141](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/141)
- Statistiques - Anomalie sur Graphique
  [#145](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/145)
- Champ d'adresse mail pour alerte décès trop permissif
  [#138](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/138)
- Absence d'information (icône "i") sur prénom et nom d'état civil
  [#135](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/135)

## 2.2.0-beta.4 - 2022-09-21

### Added (2.2.0-beta.4)

Pour les développeurs :

- Création d'un fichier pour le suivi des tests à lancer
  manuellement [Tests_acceptations_manuels.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Developpeur/Tests_acceptation/Tests_acceptations_manuels.md)
  dans le dossier `Developpeur/Tests_acceptations`
- La mise à disposition de variables reprenant tous les champs de la table `INDIVIDU` afin de les utiliser dans les
  modèles de courriel ou courrier PDF. La liste exhaustive de ces variables se trouve dans le
  fichier [Personnalisation.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Personnalisation.md)
  sous la rubrique *Paramètres
  disponibles...* ([#129](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/129))

Pour les opérateurs :

- La mise à disposition de logs pour les requêtes SQL des
  recherches ([#151](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/151))
- La possibilité d'affecter à un instructeur la gestion des réponses *aide en cours d'instruction*, éventuellement
  combinée aux autres résultats (Récupération, Indus, Ambigu) afin de répartir la charge de travail entre
  instructeurs ([#147](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/147))
- Migration v1/v2 permettant d'importer l'historique de recherche et les
  statistiques ([#83](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/83))

Pour les utilisateurs :

- La possibilité de voir dans l'historique de recherche les recherches ayant pour résultat *aide en cours d'instruction*
  et pour une recherche donnant plusieurs résultats, tous les résultats de ladite
  recherche. ([#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149))
- La possibilité d'extraction CSV à partir de la consultation des
  recherches ([#137](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/137))
- Ajoute l'affichage de tous les types de réponses dans la liste des
  recherches. ([#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149))

### Changed (2.2.0-beta.4)

Pour les développeurs :

- Correction sur le `Jenkinsfile`
- Correction d'un message d'erreur lors de la suppression du mail destinataire de paramètre *alerte
  décès* ([#140](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/140))

Pour les utilisateurs :

- Modification des statistiques, le nombre de demandes n'est plus égale au nombre de réponses, du fait des aides
  multiples. Une demande peut contenir plusieurs réponses.

### Fixed (2.2.0-beta.4)

- Correction d'un bug mineur affichés dans les logs applicatifs de
  stats ([#118](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/118))
- Correction sur le nom du fichier
  PDF ([#93](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/93))
- Correction de l'erreur technique non bloquante dans les logs de l'application *
  /admin/params* ([#117](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/117))
- Correction de la suppression impossible des logos présents sur les pdf, dans les paramètres de
  l'application. ([#141](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/141))

## 2.2.0-beta.3 - 2022-08-26

### Changed (2.2.0-beta.3)

- Les fichiers suivants ont été renommés :
    - `search_find.html.twig` => `search_found.html.twig`
    - `search_notfind.html.twig` => `search_notfound.html.twig`,
    - `search_find_rec.html.twig` => `search_found_rec.html.twig`
    - `search_find_notrec.html.twig` => `search_found_notrec.html.twig`

Ce changement est aussi valable sur toutes les variables et constantes y faisant référence.

Pour les développeurs :

- Pour un meilleur suivi le fichier de test cypress `4-search.spec.js` a été divisé en deux `4.1-search.spec.js` pour
  les tests d'aide simple et `4.2-search-multiple-helpcode.spec.js` pour les aides multiples.
- Rajout des tests unitaires au pre-commit

### Fixed (2.2.0-beta.3)

- Les tests unitaires
- Les tests cypress

## 2.2.0-beta.2 - 2022-08-24

### Added (2.2.0-beta.2)

Pour les développeurs :

- La possibilité d'importer des individus bénéficiaires de plusieurs aides afin de répondre au besoin métier
  [#147](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/147)

Pour les utilisateurs :

- La nouvelle prise en compte des individus en cours d'instruction en tant que personne connue. Ces personnes seront
  maintenant traitées comme des personnes connues avec simplement une ou plusieurs aides en cours d'instruction à
  leur actif. La seule différence est que si une personne a uniquement une ou plusieurs aides en cours d'instruction,
  elle ne recevra pas de pdf.
  [#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149)

### Fixed (2.2.0-beta.2)

- Correction du nombre de connexions sur le graphique qui présentait un décalage de
  mois [#145](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/145)
- Le changement de type du champ d'adresse mail pour alerte décès dans les paramètres pour le rendre moins
  permissif [#138](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/138)
- Rajout des icônes d'information manquantes sur le formulaire de
  recherche [#135](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/135)

## 2.2.0-beta.1 - 2022-08-05

### Added (2.2.0-beta.1)

Pour les développeurs :

- Ajout d'un fichier de compte rendu du GTC pour le lot 2 de la version 2.2.0. Ce fichier est prérempli avec le contenu
  des issues déjà traitées.
- Ajout du pre-commit hook dans le `Makefile`
- [#127 Pouvoir personnaliser le sujet d’un mail](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/127)
- [#128 Personnalisation signature mail métier depuis le backoffice](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/128)
- [#129 Mails et PDF : disposer de balise pour tous les champs de la BD](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/129)
- [#148 Différencier les courriers selon le type d’aide](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/148)
- [#80 Formulaire de recherche : utilisation coche obligatoire conforme à la v1](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/80)

Pour les opérateurs :

- Ajout d'une documentation pour la personnalisation du sujet de mail
  dans [Personnalisation_mail.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Personnalisation_mail.md)
- Ajout de la description des balises de modèles applicables dans les templates de mail/pdf
  dans [Personnalisation.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Personnalisation.md)
- Ajout des paramètres disponibles dans les templates de mail/pdf
  dans [Personnalisation.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Personnalisation.md)

### Changed (2.2.0-beta.1)

Pour les opérateurs :

- Déplacement du
  fichier [Personnalisation.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur/Personnalisation.md)
  dans le dossier `Operateur` (situé dans `Utilisateur` avant)

### Fixed (2.2.0-beta.1)

- Correction du test cypress `Personne connue,prénom composée écrit dans le mauvais sens, aide non récupérable`
- Mauvais chemin vers le fichier `cs-cache` de `phpcs`
- Correction de l'entièreté du projet avec `phpcs`
- Suppression des chemins `libs` et `tests` pour les tests `phpcs` et `phpstan`
- Correction d'un paramètre manquant dans la recherche du statut de mail, afin de prendre en compte si le code
  d'aide est `null`
- Correction du script Jenkins

## 2.1.0 - 2021-11-04 ==================================================================================================

(Cette entrée cumule toutes les informations des versions beta de la 2.2.0)

### Added (2.1.0)

* Moteur de recherche : chercher « Nom-A Nom-B » cherche aussi « Nom-B Nom-A »
  [#130](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/130),
  [#88](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/88)
* Moteur de recherche : ajout d'un paramètre "Année différente donne ambigu au lieu d'inconnu"
  [#131](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/131),
  [#52](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/52)
* Moteur de recherche : ajout d'un paramètre "mois de naissance différent donne ambigu au lieu d’inconnu"
  [#134](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/134),
  [#72](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/72)
* Moteur de recherche : ajout d'un paramètre "jour de naissance différent donne ambigu au lieu d’inconnu"
  [#134](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/134),
  [#73](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/73)
* Moteur de recherche : ajout d'un paramètre "date de naissance différente donne ambigu au lieu d'inconnu"
  [#133](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/133),
  [#76](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/76)
* Moteur de recherche : ajout d'un paramètre "Prénom différent donne ambigu au lieu de connu "
  [#132](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/132),
  [#51](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/52),
  [#74](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/74),
  [#75](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/75),
  [#105](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/105),
  [#110](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/110)

### Changed (2.1.0)

## 2.1.0-beta.2 - 2021-10-28

### Added (2.1.0-beta.2)

Pour les développeurs :

- Ajout d'un README dans la racine du projet

### Changed (2.1.0-beta.2)

Pour les développeurs :

- Modification du readme pour publier une version
- Modification de lien dans le Readme obsolète

## 2.1.0-beta.1 - 2021-10-26

### Added (2.1.0-beta.1)

Pour les utilisateurs :

- Ajouts de paramètres de recherche afin d'affiner les résultats
    - [#88 Chercher « Nom-A Nom-B » et aussi « Nom-B Nom-A »](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/88)
    - [#52 Une erreur sur l'année de naissance](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/52)
    - [#51 Cas du prénom différent](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/51)
    - [#76 recherche avec nom et prénom exacts](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/76)
    - [#72 Une erreur sur le mois de naissance](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/72)
    - [#73 Une erreur sur jour de naissance](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/73)
    - [#74 Initiale du prénom](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/74)
    - [#75 3 premiers caractères du prénom](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/75)

Pour les développeurs :

- Ajouts de tests E2E

### Changed (2.1.0-beta.1)

- Documentation de l'algorithme de recherche
- Ajouts d'exemples de cheminements de recherche
- Modification de l'interface d'administration afin de regrouper les différentes options de recherche

## 2.0.1 - 2021-06-09 ==================================================================================================

### Added (2.0.1)

Pour les utilisateurs :

- Ajout d'une documentation pour un import volumineux (requête SQL)

### Changed (2.0.1)

- Documentation de mise à jour

### Fixed (2.0.1)

- [#94 Corrige lien "aide en ligne" dans l'entête](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/94)
- [#79 Corrige réponse inconue - mauvais type de retour d'une fonction](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/79)
- Documentation format CSV import des utilisateurs
- Corrige problème de chemin vers fichier sous environnement Windows

## 2.0.0 - 2020-09-15 ==================================================================================================

Déploiement version 2.0.0, identique à la version 2.0.0-beta.5

## 2.0.0-beta.5 - 2020-08-18

### Added (2.0.0-beta.5)

Pour les utilisateurs :

- [#70 Alerte décès](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/70)
- Migration comptes utilisateurs v1 vers v2
- Documentation migration v1 vers v2
- Manuel utilisateur

Pour les développeurs :

- [#67 Automatiser les tests E2E](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/67)
- [#5 Créer un utilisateur sans interaction](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/5)
- Documentation installation docker
- Documentation pour publier une version de l'appli
- Documentation pour lancer les tests E2E avec Cypress

### Fixed (2.0.0-beta.5)

- [#71 Mise à jour documentation upgrade beta.3 vers beta.4](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/71)

## 2.0.0-beta.4 - 2020-07-16

### Added (2.0.0-beta.4)

Pour les utilisateurs :

- [#11 Exemple de CSS personnalisée](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/11)
- [#66 Détail des cheminements des recherches](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/66)

Pour les développeurs :

- [#53 Ajout de tests E2E](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/53)
- [#61 Ajout de tests unitaires dans CI Gitlab](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/61)
- [#69 Ajout de tests E2E sur les intructeurs](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/69)

### Fixed (2.0.0-beta.4)

- [#55 Suppression d'un message d'erreur lors d'une recherche lorsque la personne est connue](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/55)
- [#63 Correction de l'impossibilité de réinitialiser le mot de passe](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/63)
- [#64 Modification du titre de l'email de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/64)

## 2.0.0-beta.3 - 2020-07-01

### Added (2.0.0-beta.3)

- Template (capacité à modifier l'apparence de l'application)

### Fixed (2.0.0-beta.3)

Pour les utilisateurs :

- [#41 DOC algorithme de recherche: documentation complète](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/41)
- [#58 DOC Imprécision dans la documentation de l'algorithme de recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/58)
- [#59 Amélioration le phrasé du mail quand le cas est connu](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/59)
- [#57 Informations sur la personne recherchée différentes dans le mail et le PDF](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/57)
- [#50 Dans le mail et le courrier PDF adressés aux notaires ne figure pas le nom d'état civil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/50)
- [#49 Création d'une étude notariale : le complément d'adresse devrait être optionnel](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/49)
- [#47 Utilisation de liens symboliques sur des dossiers dans l'application](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/47)
- [#46 Rendre possible la recherche sur le nom d'usage (et non celui d'état civil)](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/46)
- [#45 Erreur à la création d'utilisateur au profil notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/45)
- [#43 Lien en dur vers le Rhône sur la page d'accueil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/43)
- [#40 Le prénom3 n'apparait pas dans les mails envoyés au notaire](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/40)
- [#19 Impossible de rafraichir les stats apres saisies des dates de selection](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/19)

Pour les développeurs :

- [#60 TEST avoir un jeu de test pour les noms d'état civil](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/60)
- [#38 Ajout de tests unitaires sur la recherche](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/38)

## 2.0.0-beta.2 - 2020-06-12

### Added (2.0.0-beta.2)

- Génération et envoi automatique des PDFs associés aux mails après une recherche
- Extraction PDFs des listes des recherches et des statistiques
- Import des données (individus)
- Possibilité de personnaliser les courriers électroniques et PDF
- Ajout du schéma de la base de données

### Changed (2.0.0-beta.2)

- Changelog now based on [Keep a Changelog](https://keepachangelog.com/fr/1.0.0/)
- Ajout de prénom3 dans la recherche
- Ajout d'un paramètre pour choisir si la recherche doit porter sur le nom d'usage ou le nom d'état civil
- Mise à jour des libellés des paramètres
- Ajout du nom d'état civil dans les logs de recherche

### Fixed (2.0.0-beta.2)

- [#39](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/39) Le 3° prénom ne semble
  pas pris en compte
- [#37](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/37) Les données de tests ne
  produisent pas les résultats attendus
- [#35](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/35) L'ajout d'un logo de
  département mène à une erreur 500
- [#34](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/34) Erreur 500 sur ajout
  d'un utilisateur
- [#31](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/31) Une recherche sur base
  vide se termine en erreur 500
- [#30](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/30) L'export PDF des stats
  génère une erreur 500
- [#29](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/29) Import CSV: une ligne
  vide génère une erreur
- [#20](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/20) Envoi du mail à un
  instructeur (cas ambigu)
- [#14](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/14) Initiales vides pour un
  instructeur
- [#6](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/6) Affichage du logo SVG
- [#1](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/1),
  [#16](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/16) Accessibilité de la
  page d'accueil

## 2.0.0-beta.1 - 2020-04-30

### Added (2.0.0-beta.1)

- Réglages des paramètres de l'application
- Consultation de l'historique des accès
- Gestion des comptes utilisateurs
- Gestion des instructeurs (pour le routage des emails)
- Recherche
- Liste des recherches
- Statistiques
- Recherche soundex (activable ou non dans les paramètres)
- Responsive design
