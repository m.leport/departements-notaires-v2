document.addEventListener("DOMContentLoaded", function () {
    formUserUpdate();
});

/**
 * Mise à jour des champs du formulaire ajout/édition d'un utilisateur en fonction du rôle sélectionné
 */
function formUserUpdate() {
    let roles = document.getElementById('user_roles');

    roles.addEventListener('change', function () {
        let form = this.closest('form');
        let urlEncodedDataPairs = [];
        let urlEncodedData = '';

        // Récupération des paramètres POST à envoyer
        for ( let i = 0; i < form.elements.length; i++ ) {
            let e = form.elements[i];
            urlEncodedDataPairs.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value));
        }

        urlEncodedDataPairs.push("updateForm=1");
        urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

        // Appel ajax
        let httpRequest = new XMLHttpRequest();
        httpRequest.open(form.getAttribute('method'), '', true);
        httpRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        httpRequest.onreadystatechange = function () {
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                // Affichage du formulaire avec les champs selon le rôle sélectionné
                let parser = new DOMParser();
                let html = parser.parseFromString(this.responseText, 'text/html');
                form.innerHTML = html.getElementById('form_user').innerHTML;

                // On relance la fonction pour prendre en compte la modification du DOM
                formUserUpdate();
            }
        };
        httpRequest.send(urlEncodedData);
    });
}
