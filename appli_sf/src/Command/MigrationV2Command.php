<?php
/**
 * Pour lancer la migration des membres
 * déposer le fichier CSV dans imports (fichier d'exemple utilisateurs-v1.csv)
 * bin/console import:utilisateurs-v1 "imports/utilisateurs.csv".
 */

namespace App\Command;

use App\Entity\User;
use App\Helper\UserHelper;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrationV2Command extends Command
{
    public const ROLESV1 = [
        1 => UserHelper::ROLE_NOTARY,
        3 => UserHelper::ROLE_ADMIN,
        4 => UserHelper::ROLE_AGENT,
    ];
    /**
     * @var SymfonyStyle
     */
    private $io;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('import:utilisateurs-v1')
            ->setDescription('Migrer les utilisateurs de la v1 vers la v2')
            ->setHelp(
                'Permet d\'importer un export CSV des utilisateurs de la v1 avec le chemin du fichier en paramètre.'
            )
            ->addArgument('file', InputArgument::REQUIRED, 'Export CSV des utilisateurs de la v1');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $noInteraction = $input->getOption('no-interaction');
        $path = realpath(__DIR__.'/../../').'/'.$input->getArgument('file');
        $hasHeaders = false;

        if (!is_file($path)) {
            $this->io->newLine();
            $this->io->error('Le fichier '.$path.' n\'a pas été trouvé.');

            return Command::FAILURE;
        }

        $this->io->title("Migration de l'application en version 2");
        $this->io->caution(
            'Il est conseillé de faire une sauvegarde de la base de données avant de lancer ce script '.
            'si elle contient des données à conserver.'
        );
        $continue = $this->io->confirm(
            'Ce script va effacer toutes les données de la table utilisateurs. '.
            'Voulez-vous continuez ?',
            false
        );

        if ($continue || $noInteraction) {
            $this->io->section("Préparation de l'import des utilisateurs");
            $this->io->writeln('Fichier à importer : '.realpath($path));

            $contentFile = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            $nbUsersToImport = count($contentFile);

            if (false !== strpos($contentFile[0], 'login')) {
                --$nbUsersToImport;
                $hasHeaders = true;
            }

            $this->io->writeln($nbUsersToImport.' utilisateurs à importer.');
            $this->deleteAllUsers();
            $this->io->success('Suppression des utilisateurs existants');

            $this->io->section('Import des utilisateurs');
            $this->io->progressStart($nbUsersToImport);

            foreach ($contentFile as $lineNb => $line) {
                if (0 === $lineNb && $hasHeaders) {
                    continue;
                }

                $this->addUser($line);
                $this->io->progressAdvance();
            }

            $this->io->progressFinish();
            $this->io->success('Import des utilisateurs');
        }

        return Command::SUCCESS;
    }

    /**
     * Ajout d'un utilisateur à partir d'une ligne CSV (format v1).
     *
     * @param string $line
     */
    protected function addUser($line)
    {
        $user = new User();
        $lineValues = str_getcsv($line, ';');

        if ('id' != $lineValues[0]) {
            $user
                ->setUsername(trim($lineValues[1]))
                ->setName(trim($lineValues[2]))
                ->setPassword('')
                ->setRoles([self::ROLESV1[trim($lineValues[4])]])
                ->setEmail(trim($lineValues[6]))
                ->setAddress(trim($lineValues[8]))
                ->setAdditionalAddress(trim($lineValues[9]))
                ->setPostalCode(trim($lineValues[11]))
                ->setTown(trim($lineValues[12]));

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * Suppressions de tous les utilisateurs.
     */
    private function deleteAllUsers(): bool
    {
        $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            $this->entityManager->remove($user);
        }

        $this->entityManager->flush();

        return true;
    }
}
