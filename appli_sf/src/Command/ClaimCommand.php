<?php

// Pour lancer l'import des créances
// déposer le fichier import.csv dans imports (fichier d'exemple creances.csv)
// bin/console import:creances "imports/creances.csv"

namespace App\Command;

use App\Entity\Claim;
use App\Repository\PersonRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClaimCommand extends Command
{
    private $doctrine;

    /**
     * @var SymfonyStyle
     */
    private $io;

    private $personRepository;

    public function __construct(
        ManagerRegistry $doctrine,
        PersonRepository $personRepository
    ) {
        parent::__construct();
        $this->doctrine = $doctrine;
        $this->personRepository = $personRepository;
    }

    protected function configure()
    {
        $this->setName('import:creances')
             ->setDescription('Importer un fichier csv créances')
             ->setHelp('Permet d\'importer un fichier csv créances avec le chemin du fichier en paramètre.')
             ->addOption('status', 's', InputOption::VALUE_REQUIRED, 'Statut récupérable/non récupérable des créances')
             ->addOption('drop_table', 'd', InputOption::VALUE_REQUIRED, 'Suppression de toutes les créances de la table')
             ->addArgument('file', InputArgument::REQUIRED, 'Le chemin du fichier à importer');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $noInteraction = $input->getOption('no-interaction');
        $output->setFormatter(new OutputFormatter(true));
        $hasHeaders = false;

        $this->io->section('Préparation de l\'import des créances');
        $path = realpath(__DIR__.'/../../').'/'.$input->getArgument('file');
        $this->io->writeln('Fichier à importer : '.$path);

        if (!is_file($path)) {
            $this->io->newLine();
            $this->io->error('Le fichier '.$path.' n\'a pas été trouvé.');

            return Command::FAILURE;
        }

        $contentFile = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $nbAImport = count($contentFile);

        if (!is_array($contentFile) || 0 == count($contentFile)) {
            $this->io->warning('Aucune créance à importer !');

            return Command::FAILURE;
        }

        if (false !== strpos($contentFile[0], 'num_ind')) {
            --$nbAImport;
            $hasHeaders = true;
        }

        $this->io->writeln($nbAImport.' individus à importer');

        $continue = $this->io->confirm(
            'Voulez-vous continuez ?',
            false
        );

        if ($continue || $noInteraction) {
            $status = $input->getOption('status');

            if (null === $status) {
                $status = $this->io->confirm(
                    'Les créances importées sont-elles de type récupérables ?',
                    false
                );
                $input->setOption('status', $status);
            }

            // Ajout d'un paramètre pour effacer la table de créances car l'import est découpé en plusieurs fichiers
            $removeAll = $input->getOption('drop_table');

            if (null === $removeAll) {
                $removeAll = $this->io->confirm(
                    'Voulez-vous supprimer les données de la table de créances avant votre import ?',
                    false
                );
                $input->setOption('drop_table', $removeAll);
            }

            if ($removeAll) {
                $this->deleteAllClaims();
            }

            $this->io->success('Suppression des créances existantes');
            $this->io->newLine();

            $table = new Table($output);

            $this->io->section('Import des créances');
            $this->io->progressStart($nbAImport);

            foreach ($contentFile as $lineNb => $contentFileLine) {
                if (0 === $lineNb && $hasHeaders) {
                    continue;
                }

                $array_value = [];
                $contentFileLineDetail = str_getcsv($contentFileLine, ';');

                $array_value['num_ind'] = (int) trim($contentFileLineDetail[0]);
                $array_value['date_deb_periode'] = trim($contentFileLineDetail[1]);
                $array_value['date_fin_periode'] = trim($contentFileLineDetail[2]);
                $array_value['montant'] = trim($contentFileLineDetail[3]);
                $array_value['date_calcul'] = trim($contentFileLineDetail[4]);
                $array_value['affichable'] = trim($contentFileLineDetail[5]);
                $array_value['recuperable'] = $status;
                try {
                    $this->addClaim($array_value);
                } catch (\TypeError|\Exception $e) {
                    $retour = $e->getMessage();
                    $table->addRow(
                        [
                            'Individu <error>#'.$array_value['num_ind'].'</error>',
                            '<error>'.$retour.'</error>',
                        ]
                    );
                }
                $this->io->progressAdvance();
            }

            $this->io->progressFinish();
            $this->io->newLine();
            $table->render();
            $this->io->success('Fin de l\'import');
        }

        return Command::SUCCESS;
    }

    private function deleteAllClaims()
    {
        $entityManager = $this->doctrine->getManager();
        $repository = $entityManager->getRepository(Claim::class);
        $entities = $repository->findAll();

        foreach ($entities as $entity) {
            $entityManager->remove($entity);
        }
        $entityManager->flush();

        return true;
    }

    private function convertDate($date)
    {
        if (false === strpos($date, '/')) {
            return $date;
        } else {
            $date_p = explode('/', $date);

            return $date_p[2].'-'.$date_p[1].'-'.$date_p[0];
        }
    }

    private function getConvertedDate($date)
    {
        return null != $date ? new \DateTime($this->convertDate($date)) : null;
    }

    private function addClaim($array_value)
    {
        $entityManager = $this->doctrine->getManager();

        $person = $this->personRepository->findOneBy(['personNum' => $array_value['num_ind']]);

        $claim = new Claim();
        $claim->addPerson($person)
              ->setNumInd($person->getPersonNum())
              ->setStartDate($this->getConvertedDate($array_value['date_deb_periode']))
              ->setEndDate($this->getConvertedDate($array_value['date_fin_periode']))
              ->setAmount($array_value['montant'])
              ->setCalculatedDate($this->getConvertedDate($array_value['date_calcul']))
              ->setDisplayable((bool) $array_value['affichable'])
              ->setRecoverable((bool) $array_value['recuperable']);

        $entityManager->persist($claim);
        $entityManager->flush();

        return $claim->getId();
    }
}
