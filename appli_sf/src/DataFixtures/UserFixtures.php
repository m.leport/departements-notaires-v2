<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Helper\FixturesHelper;
use App\Helper\UserHelper;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends \Doctrine\Bundle\FixturesBundle\Fixture implements FixtureGroupInterface
{
    /**
     * @var UserPasswordHasherInterface
     */
    private $encode;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encode = $encoder;
    }

    public static function getGroups(): array
    {
        return [
            FixturesHelper::GROUP_WITHOUT_PERSONS,
            FixturesHelper::GROUP_USE_NAME,
            FixturesHelper::GROUP_CIVIL_NAME,
        ];
    }

    public function load(ObjectManager $manager)
    {
        // Utilisateur avec le role admin
        $adminUser = new User();

        $adminUser->setUsername('admin');
        $adminUser->setEmail('admin@notaires.test');
        $password = $this->encode->hashPassword($adminUser, 'P@ssword12345');
        $adminUser->setPassword($password);
        $adminUser->setRoles([UserHelper::ROLE_ADMIN]);
        $adminUser->setName('admin');
        $adminUser->setDisabled(false);

        $manager->persist($adminUser);

        // Utilisateur avec le role agent
        $agentUser = new User();

        $agentUser->setUsername('agent');
        $agentUser->setEmail('agent@notaires.test');
        $password = $this->encode->hashPassword($agentUser, 'P@ssword12345');
        $agentUser->setPassword($password);
        $agentUser->setRoles([UserHelper::ROLE_AGENT]);
        $agentUser->setName('agent du département');
        $agentUser->setService('service test');
        $agentUser->setDisabled(false);

        $manager->persist($agentUser);

        // Utilisateur avec le role étude notariale
        $notaryUser = new User();

        $notaryUser->setUsername('notaire');
        $notaryUser->setEmail('notaire@notaires.test');
        $password = $this->encode->hashPassword($notaryUser, 'P@ssword12345');
        $notaryUser->setPassword($password);
        $notaryUser->setRoles([UserHelper::ROLE_NOTARY]);
        $notaryUser->setName('Etude notariale');
        $notaryUser->setAddress('5 rue des tests');
        $notaryUser->setPostalCode('42000');
        $notaryUser->setTown('Testville');
        $notaryUser->setDisabled(false);

        $manager->persist($notaryUser);

        $manager->flush();
    }
}
