<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Helper\SearchHelper;
use Doctrine\Persistence\ObjectManager;

abstract class AbstractPersonFixtures extends \Doctrine\Bundle\FixturesBundle\Fixture
{
    protected $nameToUse = SearchHelper::SEARCH_BY_USE_NAME;

    public function load(ObjectManager $manager)
    {
        $header = null;
        $filename = __DIR__.'/../../imports/individus.csv';
        $data = [];

        if (false !== ($handle = fopen($filename, 'r'))) {
            while (false !== ($row = fgetcsv($handle, 1000, ';'))) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        foreach ($data as $personData) {
            $person = new Person();

            $person->setPersonNum(intval($personData['num_ind']));
            $person->setGender(trim($personData['sexe']));

            // On inverse les champs nom_civil et nom_usage
            switch ($this->nameToUse) {
                case SearchHelper::SEARCH_BY_CIVIL_NAME:
                    $person->setUseName(trim($personData['nom_civil']));
                    $person->setCivilName(trim($personData['nom_usage']));
                    break;
                default:
                    $person->setUseName(trim($personData['nom_usage']));
                    $person->setCivilName(trim($personData['nom_civil']));
            }

            $person->setFirstName(trim($personData['prenom']));
            $person->setMiddleName(trim($personData['prenomd']));
            $person->setThirdName(trim($personData['prenomt']));
            $person->setBirthDate(\DateTime::createFromFormat('Y-m-j', trim($personData['date_naissance'])));
            $person->setAddress(trim($personData['adresse']));
            $person->setDepartmentalHouseName(trim($personData['mdt']));
            $person->setDepartmentalHousePhone(trim($personData['mdt_telephone']));
            $person->setDepartmentalHouseMail(trim($personData['mdt_courriel']));
            $person->setDepartmentalHouseAddress(trim($personData['mdt_adresse']));
            $person->setLabel(trim($personData['libelle']));
            $person->setHelpCode(trim($personData['code_aide']));

            if (array_key_exists('date_deces', $personData)) {
                $person->setDeathDate(
                    \DateTime::createFromFormat('Y-m-j', trim($personData['date_deces'])) ?: null
                );
            }

            $manager->persist($person);
        }

        $manager->flush();
    }
}
