<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ClaimFixtures extends AbstractClaimFixtures implements FixtureGroupInterface, DependentFixtureInterface
{
    public static function getGroups(): array
    {
        return ['claims'];
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [PersonUseNameFixtures::class];
    }
}
