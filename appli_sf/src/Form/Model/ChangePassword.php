<?php

namespace App\Form\Model;

use Rollerworks\Component\PasswordStrength\Validator\Constraints as RollerworksPassword;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ChangePassword
{
    /**
     * @SecurityAssert\UserPassword
     *
     * @var string
     */
    protected $oldPassword;

    /**
     * @RollerworksPassword\PasswordRequirements(minLength=12, requireCaseDiff=true, requireNumbers=true,
     *                                                         requireSpecialCharacter=true)
     *
     * @var string
     */
    protected $password;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    public function setOldPassword(string $oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->oldPassword === $this->password) {
            $context->buildViolation($this->translator->trans('changePassword.newPasswordMustBeDifferent'))
                ->atPath('password')
                ->addViolation();
        }
    }
}
