<?php

namespace App\Form;

use App\Entity\Instructor;
use App\Form\Transformer\ArrayToCommaSeparatedStringTransformer;
use App\Helper\SearchHelper;
use App\Repository\PersonRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class InstructorType extends AbstractType
{
    private $transformer;

    /** @var PersonRepository */
    private $personRepository;

    public function __construct(ArrayToCommaSeparatedStringTransformer $transformer, PersonRepository $personRepository)
    {
        $this->transformer = $transformer;
        $this->personRepository = $personRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                TextType::class,
                [
                    'label' => 'addInstructor.firstName',
                ]
            )
            ->add(
                'lastName',
                TextType::class,
                [
                    'label' => 'addInstructor.lastName',
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'addInstructor.email',
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => 'addInstructor.phone',
                    'required' => false,
                ]
            )
            ->add(
                'responseType',
                ChoiceType::class,
                [
                    'choices' => SearchHelper::getResponseTypesChoicesForInstructor(),
                    'label' => 'addInstructor.responseType',
                    'multiple' => true,
                    'expanded' => true,
                    'constraints' => [new NotBlank()],
                ]
            )
            ->add(
                'initials',
                TextType::class,
                [
                    'label' => 'addInstructor.initials',
                    'required' => false,
                    'help' => 'fieldHelp.initials',
                ]
            )
            ->add(
                'helpLabel',
                ChoiceType::class,
                [
                    'label' => 'addInstructor.helpLabel',
                    'required' => false,
                    'multiple' => true,
                    'expanded' => false,
                    'choices' => $this->choicesHelpLabel(),
                    'placeholder' => 'Choose an option',
                    'help' => 'fieldHelp.helpLabel',
                ]
            )
            ->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'addUser.send',
                ]
            );

        $builder->get('initials')->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Instructor::class,
            ]
        );
    }

    private function choicesHelpLabel(): array
    {
        $array = [];
        foreach ($this->personRepository->findUniqueLabel() as $label) {
            $array[$label['label']] = $label['label'];
        }


        return $array;
    }
}
