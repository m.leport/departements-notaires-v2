<?php

namespace App\Data;

use App\Entity\AbstractPerson;
use App\Entity\SearchLog;

class SearchData extends AbstractPerson
{
    protected $deathDate;

    protected $deathLocation;

    protected $deathCertificateDate;

    protected $recipient;

    protected $firstName;

    protected $middleName;

    protected $thirdName;

    protected $useName;

    protected $civilName;

    protected \DateTime $birthDate;

    public function getDeathDate(): ?\DateTime
    {
        return $this->deathDate;
    }

    public function setDeathDate(\DateTime $deathDate): SearchData
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getDeathLocation(): ?string
    {
        return $this->deathLocation;
    }

    public function setDeathLocation(string $deathLocation): SearchData
    {
        $this->deathLocation = $deathLocation;

        return $this;
    }

    public function getDeathCertificateDate(): ?\DateTime
    {
        return $this->deathCertificateDate;
    }

    public function setDeathCertificateDate(\DateTime $deathCertificateDate): SearchData
    {
        $this->deathCertificateDate = $deathCertificateDate;

        return $this;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setRecipient(?string $recipient): SearchData
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): SearchData
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): SearchData
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getThirdName(): ?string
    {
        return $this->thirdName;
    }

    public function setThirdName(?string $thirdName): SearchData
    {
        $this->thirdName = $thirdName;

        return $this;
    }

    public function getUseName(): ?string
    {
        return $this->useName;
    }

    public function setUseName(string $useName): SearchData
    {
        $this->useName = $useName;

        return $this;
    }

    public function getCivilName(): ?string
    {
        return $this->civilName;
    }

    public function setCivilName(?string $civilName): SearchData
    {
        $this->civilName = $civilName;

        return $this;
    }

    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTime $birthDate): SearchData
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getAllNamesWithoutHyphens(): array
    {
        $names = [];

        if ($this->useName) {
            $names[] = $this->useName;
            $names[] = str_replace('-', ' ', $this->useName);
        }

        if ($this->civilName) {
            $names[] = $this->civilName;
            $names[] = str_replace('-', ' ', $this->civilName);
        }

        return $names;
    }

    public function getSingleNameWithoutHyphens(): string
    {
        $names = $this->getAllNamesWithoutHyphens();
        $singleName = reset($names);

        return str_replace('-', '', $singleName);
    }

    public function inverseComposeName($nameParts): string
    {
        return implode(' ', array_reverse($nameParts));
    }

    public function generateCombinations($nameParts): array
    {
        $combinations = [];

        for ($i = 0; $i < count($nameParts); ++$i) {
            for ($j = $i + 1; $j < count($nameParts); ++$j) {
                $combinations[] = $nameParts[$i].'-'.$nameParts[$j];
                $combinations[] = $nameParts[$i].' '.$nameParts[$j];
                $combinations[] = $nameParts[$j].'-'.$nameParts[$i];
                $combinations[] = $nameParts[$j].' '.$nameParts[$i];
            }
        }

        return $combinations;
    }

    public function getComposeName(): array
    {
        $composeNames = [];

        if ($this->useName) {
            $nameParts = explode(' ', str_replace('-', ' ', $this->useName));
            if (count($nameParts) >= 2) {
                $composeNames = array_merge($composeNames, $this->generateCombinations($nameParts));
                $composeNames[] = implode('-', $nameParts);
                $composeNames[] = implode(' ', $nameParts);
                $composeNames[] = $this->inverseComposeName($nameParts);
            }
        }

        if ($this->civilName) {
            $nameParts = explode(' ', str_replace('-', ' ', $this->civilName));
            if (count($nameParts) >= 2) {
                $composeNames = array_merge($composeNames, $this->generateCombinations($nameParts));
                $composeNames[] = implode('-', $nameParts);
                $composeNames[] = implode(' ', $nameParts);
                $composeNames[] = $this->inverseComposeName($nameParts);
            }
        }

        return $composeNames;
    }

    /**
     * @param string $name Nom d'usage ou civil composée voulant être lu dans les deux sens
     */
    public function reverseComposeName(string $name): string
    {
        $tmpName = explode(' ', str_replace('-', ' ', $name));

        if (count($tmpName) > 1) {
            return $tmpName[1].' '.$tmpName[0];
        } else {
            return $tmpName[0];
        }
    }

    public function isComposeName(): bool
    {
        $useName = is_string($this->useName) ? explode(' ', $this->useName) : [];
        $civilName = is_string($this->civilName) ? explode(' ', $this->civilName) : [];

        if (count($useName) > 1 || count($civilName) > 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllNames(): array
    {
        $names = [];

        if ($this->useName) {
            $names[] = $this->useName;
        }

        if ($this->civilName) {
            $names[] = $this->civilName;
        }

        return $names;
    }

    public function getAllFirstNames(): array
    {
        $names = [];

        if ($this->firstName) {
            $names[] = $this->firstName;
        }

        if ($this->middleName) {
            $names[] = $this->middleName;
        }

        if ($this->thirdName) {
            $names[] = $this->thirdName;
        }

        return $names;
    }

    public function getFirstNameStart(): false|string
    {
        return substr($this->firstName, 0, 3);
    }

    /**
     * @throws \Exception
     */
    public function toSearchLog(): SearchLog
    {
        $searchLog = new SearchLog();

        $searchLog->setUseName($this->useName);
        $searchLog->setCivilName($this->civilName);
        $searchLog->addFirstName($this->firstName);
        if ($this->middleName) {
            $searchLog->addFirstName($this->middleName);
        }
        if ($this->thirdName) {
            $searchLog->addFirstName($this->thirdName);
        }
        $searchLog->setBirthDate($this->birthDate);
        $searchLog->setDeathDate($this->deathDate);
        $searchLog->setDeathLocation($this->deathLocation);
        $searchLog->setDeathCertificateDate($this->deathCertificateDate);

        return $searchLog;
    }
}
