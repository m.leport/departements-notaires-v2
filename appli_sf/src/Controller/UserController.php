<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Helper\MessageHelper;
use App\Repository\UserRepository;
use App\Service\MailService;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Doctrine\ORM\EntityManagerInterface;
use Hackzilla\PasswordGenerator\Exception\CharactersNotFoundException;
use Hackzilla\PasswordGenerator\Exception\ImpossibleMinMaxLimitsException;
use Hackzilla\PasswordGenerator\Generator\ComputerPasswordGenerator;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @Route("/admin")
 */
class UserController extends BaseController
{
    /**
     * @var int
     */
    protected $passwordLength;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * UserController constructor.
     */
    public function __construct(
        SettingsDataService $settingsDataService,
        int $passwordLength,
        TranslatorInterface $translator,
        ThemeService $themeService,
        protected UserRepository $userRepository,
        protected EntityManagerInterface $entityManager,
    ) {
        parent::__construct($settingsDataService, $themeService);

        $this->passwordLength = $passwordLength ?: 12;
        $this->translator = $translator;
    }

    /**
     * Liste les utilisateurs.
     *
     */
    #[Route('/user/list', name: 'admin_user_list')]
    public function listAction()
    {
        $users = $this->userRepository->findAll();

        return $this->render(
            'admin/user/list.html.twig',
            [
                'users' => $users,
            ]
        );
    }

    /**
     * Affiche le formulaire d'ajout d'un compte.
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordEncoder
     * @param MailService $mailService
     * @param TokenGeneratorInterface $tokenGenerator
     * @return Response
     *
     * @throws CharactersNotFoundException
     * @throws ImpossibleMinMaxLimitsException
     */
    #[Route('/user/add', name: 'admin_user_add')]
    public function addAction(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        MailService $mailService,
        TokenGeneratorInterface $tokenGenerator
    ): Response {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        // Si sélection d'un autre rôle on recharge juste le formulaire sans le valider
        $updateForm = (bool) $request->request->get('updateForm');

        if (!$updateForm && $form->isSubmitted() && $form->isValid()) {
            // Génération du mot de passe
            $generator = new RequirementPasswordGenerator();

            $generator
                ->setLength($this->passwordLength)
                ->setUppercase(true)
                ->setLowercase(true)
                ->setNumbers(true)
                ->setSymbols(true)
                ->setMinimumCount(ComputerPasswordGenerator::PARAMETER_SYMBOLS, 1)
                ->setMaximumCount(ComputerPasswordGenerator::PARAMETER_SYMBOLS, 3)
                ->setAvoidSimilar(true);

            $plainPassword = $generator->generatePassword();
            $password = $passwordEncoder->hashPassword($user, $plainPassword);
            $user->setPassword($password);

            // Création du token pour l'envoi de mail
            $token = $tokenGenerator->generateToken();

            $user->setTokenReset($token);

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $subject = $this->translator->trans(
                'addUser.confirmEmail.subject',
                ['%appName%' => $this->settingsData->getAppName()]
            );

            $this->sendResetPasswordMail($user, $mailService, $subject);

            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render(
            'admin/user/add.html.twig',
            [
                'userForm' => $form->createView(),
            ]
        );
    }

    /**
     * Affiche le formulaire d'édition d'un compte.
     *
     * @param User $user
     * @param Request $request
     * @param UserPasswordHasherInterface $passwordEncoder
     * @return Response
     */
    #[Route('/user/{id}/edit', name: 'admin_user_edit')]
    public function editAction(
        User $user,
        Request $request,
        UserPasswordHasherInterface $passwordEncoder
    ): Response {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        $updateForm = (bool) $request->request->get('updateForm');

        if (!$updateForm && $form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userEdit.updated'));

            return $this->redirectToRoute(
                'admin_user_edit',
                [
                    'id' => $user->getId(),
                ]
            );
        }

        return $this->render(
            'admin/user/edit.html.twig',
            [
                'userForm' => $form->createView(),
            ]
        );
    }

    /**
     * Supprime un compte à partir de son id.
     *
     *
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    #[Route('/user/{id}/remove', name: 'admin_user_remove')]
    public function removeAction(Request $request, User $user): RedirectResponse
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-user', $submittedToken)) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userDelete.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userDelete.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Désactive un compte à partir de son id.
     *
     *
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    #[Route('/user/{id}/disable', name: 'admin_user_disable')]
    public function disableAction(Request $request, User $user): RedirectResponse
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('disable-user', $submittedToken)) {
            $user->setDisabled(true);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userDisable.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userDisable.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Active un compte à partir de son id.
     *
     * @return RedirectResponse
     */
    #[Route('/user/{id}/enable', name: 'admin_user_enable')]
    public function enableAction(Request $request, User $user): RedirectResponse
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('enable-user', $submittedToken)) {
            $user->setDisabled(false);
            $user->setNbLoginAttempts(0);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userEnable.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userEnable.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Réinitialisation du mot de passe d'un compte à partir de son id.
     *
     *
     * @return RedirectResponse
     *
     */
    #[Route('/user/{id}/reset_password', name: 'admin_user_reset_password')]
    public function resetPasswordAction(
        Request $request,
        User $user,
        MailService $mailService,
        TokenGeneratorInterface $tokenGenerator
    ): RedirectResponse {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('resetPassword-user', $submittedToken)) {
            // Création du token pour l'envoi de mail
            $token = $tokenGenerator->generateToken();

            $user->setTokenReset($token);
            $this->entityManager->flush();

            $subject = $this->translator->trans(
                'adminResetPassword.mailSubject',
                ['%appName%' => $this->settingsData->getAppName()]
            );

            $this->sendResetPasswordMail($user, $mailService, $subject);

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $this->translator->trans('userResetPassword.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $this->translator->trans('userResetPassword.error'));
        }

        return $this->redirectToRoute('admin_user_list');
    }

    /**
     * Envoie du mail de réinitialisation du mot de passe.
     *
     */
    private function sendResetPasswordMail(User $user, MailService $mailService, string $subject): void
    {
        $url = $this->generateUrl(
            'init_password',
            ['token' => $user->getTokenReset()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $mailRecipients = [$user->getEmail()];

        $mailHtml = $this->getTemplateWithActifTheme(
            'emails/add_user.html.twig'
        );
        $mailParameters = [
            'name' => $user->getName(),
            'initUrl' => $url,
        ];

        try {
            $mailService->sendHtmlMail($mailRecipients, $subject, $mailHtml, $mailParameters);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }
    }
}
