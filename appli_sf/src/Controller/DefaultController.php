<?php

namespace App\Controller;

use App\Helper\UserHelper;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController
{
    /**
     * Contenu par défaut du site (redirection sur la page de login si non connecté).
     */
    #[Route('/', name: 'index')]
    public function indexAction(Security $security): Response
    {
        if ($security->isGranted(UserHelper::ROLE_ADMIN)) {
            return $this->redirect('admin');
        }

        return $this->redirect('search');
    }

    /**
     * Affiche l'aide.
     */
    #[Route('/help', name: 'help')]
    public function helpAction(): Response
    {
        return $this->render('help.html.twig');
    }

    /**
     * Affiche les CGU.
     */
    #[Route('/terms_of_use', name: 'terms_of_use')]
    public function termsOfUseAction(): Response
    {
        return $this->render('terms_of_use.html.twig');
    }
}
