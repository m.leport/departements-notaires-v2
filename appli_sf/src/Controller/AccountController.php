<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Form\Model\ChangePassword;
use App\Helper\MessageHelper;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AccountController extends BaseController
{
    public function __construct(
        protected EntityManagerInterface $entityManager,
        ThemeService $service,
        SettingsDataService $settingsDataServiceService,
    ) {
        parent::__construct($settingsDataServiceService, $service);
    }

    /**
     * Affiche les infos sur le compte courant.
     */
    #[Route('/account/infos', name: 'account_infos')]
    public function infosAction(): Response
    {
        $user = $this->getUser();

        return $this->render(
            'account/infos.html.twig',
            ['user' => $user]
        );
    }

    /**
     * Change le mot de passe de l'utilisateur courant.
     */
    #[Route('/account/change-password', name: 'account_change_password')]
    public function changePasswordAction(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        TranslatorInterface $translator
    ): RedirectResponse|Response {
        /** @var User $user */
        $user = $this->getUser();

        $changePassword = new ChangePassword($translator);
        $form = $this->createForm(ChangePasswordType::class, $changePassword);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPlainPassword = $form->get('password')['first']->getData();

            $newEncodedPassword = $passwordEncoder->hashPassword($user, $newPlainPassword);
            $user->setPassword($newEncodedPassword);

            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('changePassword.success'));

            return $this->redirectToRoute('account_infos');
        }

        return $this->render(
            'account/change_password.html.twig',
            [
                'changePasswordForm' => $form->createView(),
            ]
        );
    }
}
