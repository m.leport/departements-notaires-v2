<?php

namespace App\Controller;

use App\Entity\Settings;
use App\Form\SettingsType;
use App\Helper\MessageHelper;
use App\Repository\UserLogRepository;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin")
 */
class AdminController extends BaseController
{
    /**
     * Page d'accès aux menu administrateur.
     */
    #[Route('/', name: 'admin')]
    public function indexAction(): Response
    {
        return $this->render('admin/index.html.twig', []);
    }

    /**
     * Modifie les paramètres du site.
     *
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    #[Route('/params', name: 'admin_settings')]
    public function editParamsAction(
        Request $request,
        EntityManagerInterface $em,
        TranslatorInterface $translator
    ): Response {
        $form = $this->createForm(SettingsType::class, $this->settingsData);
        $arraysSettingsFiles = ['appLogo', 'appFavicon', 'pdfLogo1', 'pdfLogo2', 'pdfServiceLogo', 'pdfSignature', 'pdfCSS', 'mailBusinessImgSignature'];
        $form->handleRequest($request);
        $picturesIsDeleted = [];
        foreach ($form as $row) {
            if (str_contains($row->getname(), 'Deleted')) {
                $picturesIsDeleted[$row->getname()] = $row->getData();
            }
        }
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $settingsEntities = $this->settingsData->toEntities();
                foreach ($settingsEntities as $settingEntity) {
                    $curSettings = $em->getRepository(Settings::class)->findOneBy(['name' => $settingEntity->getName()]);
                    if (!$curSettings) {
                        $em->persist($settingEntity);
                    } elseif ($settingEntity->getValue()) {
                        $curSettings->setValue($settingEntity->getValue());
                    } elseif ('' == $settingEntity->getValue() && !in_array($settingEntity->getName(), $arraysSettingsFiles)) {
                        $curSettings->setValue($settingEntity->getValue());
                    }
                    foreach ($picturesIsDeleted as $key => $pictureIsDeleted) {
                        if ($pictureIsDeleted) {
                            $curSettings = $em->getRepository(Settings::class)->findOneBy(['name' => str_replace('Deleted', '', $key)]);
                            $curSettings->setValue(null);
                        }
                    }
                    $em->flush();
                }
                $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('settings.updated'));

                return $this->redirect($request->getUri());
            } else {
                $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('settings.error_updated'));
            }
        }

        return $this->render(
            'admin/settings.html.twig',
            [
                'settingsForm' => $form->createView(),
            ]
        );
    }

    /**
     * Affiche des logs d'accès.
     */
    #[Route('/logs', name: 'admin_logs')]
    public function logsAction(UserLogRepository $userRepository): Response
    {
        $logs = $userRepository->findAll();

        return $this->render(
            'admin/logs.html.twig',
            [
                'logs' => $logs,
            ]
        );
    }
}
