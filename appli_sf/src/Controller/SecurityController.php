<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\InitPasswordType;
use App\Helper\MessageHelper;
use App\Service\MailService;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;

class SecurityController extends BaseController
{
    /**
     * SecurityController constructor.
     */
    public function __construct(
        SettingsDataService $settingsDataServiceService,
        protected RequestStack $requestStack,
        ThemeService $service,
        protected EntityManagerInterface $entityManager,
    ) {
        parent::__construct($settingsDataServiceService, $service);
    }

    /**
     * Affiche le formulaire de login.
     */
    #[Route('/login', name: 'app_login')]
    public function login(
        AuthenticationUtils $authenticationUtils,
        TranslatorInterface $translator
    ): Response {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $lastUsername]);

        if ($error) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans($error->getMessageKey()));

            if ($user) {
                if ($user->getNbLoginAttempts() < $this->settingsData->getConnectionAttempts()) {
                    $user->incrementLoginAttempts();
                    $this->entityManager->flush();
                }
            }
        }
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout()
    {
    }

    /**
     * Affiche le formulaire d'initialisation du mot de passe.
     */
    #[Route('/password-init/{token}', name: 'init_password')]
    public function initPassword(
        Request $request,
        string $token,
        UserPasswordHasherInterface $passwordEncoder,
        TranslatorInterface $translator
    ): Response {
        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['tokenReset' => $token]);

        if (null === $user) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, 'security.passwordReset.badResetLink');

            return $this->redirectToRoute('app_login');
        }

        $username = $user->getUsername();
        $form = $this->createForm(InitPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getUsername() !== $username) {
                $this->addFlash(
                    MessageHelper::MESSAGE_ERROR,
                    $translator->trans('security.passwordReset.badUsername')
                );
            } else {
                $user->setTokenReset(null);
                $user->setPassword($passwordEncoder->hashPassword($user, $user->getPassword()));
                $this->entityManager->flush();

                $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('security.passwordReset.success'));

                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render(
            'security/init_password.html.twig',
            [
                'initForm' => $form->createView(),
                'token' => $token,
            ]
        );
    }

    /**
     *  Affiche le formulaire de mot de passe oublié.
     *
     * @throws LoaderError
     */
    #[Route('/password-forgot', name: 'forgot_password')]
    public function forgotPassword(
        Request $request,
        TokenGeneratorInterface $tokenGenerator,
        TranslatorInterface $translator,
        MailService $mailService
    ): Response {
        $form = $this->createFormBuilder()
            ->add(
                'username',
                TextType::class,
                [
                    'label' => 'security.forgotPassword.login',
                    'required' => true,
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'security.forgotPassword.send',
                ]
            )->getForm();

        $form->handleRequest($request);
        $submittedToken = $request->request->get('_csrf_token');

        if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('forgot_password', $submittedToken)) {
            $data = $form->getData();

            /* @var $user User */
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $data['username']]);

            if (!$user) {
                $this->addFlash(
                    MessageHelper::MESSAGE_INFO,
                    $translator->trans('security.forgotPassword.sendMessage')
                );

                return $this->redirectToRoute('app_login');
            }

            // Création du token pour l'envoi de mail
            $token = $tokenGenerator->generateToken();

            try {
                $user->setTokenReset($token);
                $this->entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans($e->getMessage()));

                return $this->redirectToRoute('app_login');
            }

            if ($this->settingsData->getAppUrl()) {
                $externalUrl = rtrim($this->settingsData->getAppUrl(), '/');  // Supprime le slash final s'il y en a un
                $url = $externalUrl.$this->generateUrl('reset_password', ['token' => $token]);
            } else {
                $url = $this->generateUrl('reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
            }

            $mailRecipients = [$user->getEmail()];
            $mailSubject = $translator->trans(
                'security.forgotPassword.mailSubject',
                ['%appName%' => $this->settingsData->getAppName()]
            );
            $mailHtml = $this->getTemplateWithActifTheme(
                'emails/reset_password.html.twig'
            );
            $mailParameters = [
                'name' => $user->getName(),
                'resetUrl' => $url,
            ];

            try {
                $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $mailParameters);
            } catch (TransportExceptionInterface $exception) {
                $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
            } catch (RfcComplianceException $exception) {
                $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
            }

            $this->entityManager->persist($user);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_INFO, $translator->trans('security.forgotPassword.sendMessage'));

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'security/forgot_password.html.twig',
            [
                'forgotForm' => $form->createView(),
            ]
        );
    }

    /**
     * Envoie par mail de la clé de déblocage du compte.
     *
     * @throws LoaderError
     */
    #[Route('/send-unlock-key/{token}', name: 'send_unlock_key')]
    public function sendUnlockKey(
        string $token,
        MailService $mailService,
        TranslatorInterface $translator
    ) {
        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['tokenReset' => $token]);

        if (null === $user) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('security.unlockUser.badToken'));

            return $this->redirectToRoute('app_login');
        }

        // Code de déverrouillage
        $unlockKey = mt_rand();
        $this->requestStack->getSession()->set('unlockKey', $unlockKey);

        // Envoi du mail
        $mailRecipients = [$user->getEmail()];
        $mailSubject = $translator->trans(
            'security.unlockUser.mailSubject',
            [
                '%appName%' => $this->settingsData->getAppName(),
                '%username%' => $user->getUsername(),
            ]
        );
        $mailHtml = $this->getTemplateWithActifTheme(
            'emails/unlock_user.html.twig'
        );

        $mailParameters = [
            'nbLoginAttempts' => $user->getNbLoginAttempts(),
            'unlockKey' => $unlockKey,
        ];

        try {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $mailParameters);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }

        return $this->redirectToRoute('unlock_user', ['token' => $token]);
    }

    /**
     * Débloque un compte à partir d'un token.
     */
    #[Route('/unlock-account/{token}', name: 'unlock_user')]
    public function unlockUser(
        Request $request,
        string $token,
        TranslatorInterface $translator
    ): Response {
        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['tokenReset' => $token]);

        if (null === $user) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('security.unlockUser.badToken'));

            return $this->redirectToRoute('app_login');
        }

        $form = $this->createFormBuilder()
            ->add(
                'unlockKey',
                IntegerType::class,
                [
                    'label' => 'unlockUser.unlockKey',
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'unlockUser.send',
                ]
            )->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $unlockKey = $this->requestStack->getSession()->get('unlockKey');

            if ($data['unlockKey'] === $unlockKey) {
                $user->setDisabled(false);
                $user->setNbLoginAttempts(0);
                $user->setTokenReset(null);
                $this->entityManager->flush();

                $this->addFlash(
                    MessageHelper::MESSAGE_INFO,
                    $translator->trans('security.unlockUser.success', ['%username%' => $user->getUsername()])
                );

                return $this->redirectToRoute('app_login');
            } else {
                $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('security.unlockUser.badKey'));
            }
        }

        return $this->render(
            'security/unlock_user.html.twig',
            [
                'unlockForm' => $form->createView(),
                'token' => $token,
            ]
        );
    }

    /**
     * Affiche le formulaire de réinitialisation du mot de passe.
     */
    #[Route('/password-reset/{token}', name: 'reset_password')]
    public function resetPassword(
        Request $request,
        string $token,
        UserPasswordHasherInterface $passwordEncoder,
        TranslatorInterface $translator
    ): Response {
        /* @var $user User */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['tokenReset' => $token]);

        if (null === $user) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('security.passwordReset.badResetLink'));

            return $this->redirectToRoute('app_login');
        }

        $form = $this->createFormBuilder($user)
            ->add(
                'password',
                RepeatedType::class,
                [
                    'type' => PasswordType::class,
                    'invalid_message' => $translator->trans('security.passwordsMissMatch'),
                    'first_options' => [
                        'label' => 'resetPassword.password',
                        'help' => 'fieldHelp.password',
                    ],
                    'second_options' => ['label' => 'resetPassword.repeatPassword'],
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'resetPassword.send',
                ]
            )->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setTokenReset(null);
            $user->setPassword($passwordEncoder->hashPassword($user, $user->getPassword()));
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('security.passwordReset.success'));

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'security/reset_password.html.twig',
            [
                'resetForm' => $form->createView(),
                'token' => $token,
            ]
        );
    }
}
