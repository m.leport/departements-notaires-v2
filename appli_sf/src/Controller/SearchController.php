<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Instructor;
use App\Entity\Person;
use App\Entity\SearchLog;
use App\Entity\User;
use App\Form\DocumentaryType;
use App\Form\SearchLogsFiltersType;
use App\Form\SearchType;
use App\Helper\FileHelper;
use App\Helper\MessageHelper;
use App\Helper\SearchHelper;
use App\Helper\UserHelper;
use App\Repository\InstructorRepository;
use App\Repository\PersonRepository;
use App\Repository\SearchLogRepository;
use App\Service\MailService;
use App\Service\PdfService;
use App\Service\PersonService;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Doctrine\ORM\EntityManagerInterface;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SearchController extends BaseController
{
    /**
     * UserController constructor.
     */
    public function __construct(
        protected SettingsDataService $settingsDataService,
        protected TranslatorInterface $translator,
        ThemeService $themeService,
        protected string $appTerritoryInstructor,
        protected EntityManagerInterface $entityManager,
        protected InstructorRepository $instructors,
        protected SearchLogRepository $searchLogRepository,
        protected PersonRepository $personRepository,
    ) {
        parent::__construct($settingsDataService, $themeService);
    }

    /**
     * Recherche d'un individu.
     *
     * @throws Html2PdfException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws \Exception
     */
    #[Route('/search', name: 'search')]
    public function searchAction(
        Request $request,
        MailService $mailService,
        PdfService $pdfService,
        PersonService $personService
    ): Response {
        $searchData = new SearchData();
        $persons = null;

        $form = $this->createForm(SearchType::class, $searchData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $useSoundex = $this->settingsData->hasSoundexSearch();
            $ignoreDifferentFirstNameSearch = $this->settingsData->hasIgnoreDifferentFirstNameSearch();
            $acceptErrorInBirthDate = $this->settingsData->getAcceptErrorInBirthDate();
            $sendMailToInstructor = true;
            [$searchStatus, $person] = $this->searchPerson(
                $this->personRepository,
                $searchData,
                $useSoundex,
                $acceptErrorInBirthDate,
                $ignoreDifferentFirstNameSearch
            );

            // ----------------------
            // REPONSE_INCONNU : élargissement aux demandes possible dans plusieurs cas, le traitement est mutualisé ici
            // ----------------------

            if (in_array(SearchHelper::SEARCH_STATUS_NOTFOUND, $searchStatus)) {
                $countPersons = $this->personRepository->countBySearch(
                    $searchData,
                    $useSoundex,
                    $acceptErrorInBirthDate,
                    false,
                    $ignoreDifferentFirstNameSearch,
                    true
                );
                if (1 === $countPersons) {
                    $this->sendMail([SearchHelper::SEARCH_STATUS_HAS_REQUEST], $mailService, $searchData);
                    $sendMailToInstructor = false;
                }
            }

            $this->addFlash(
                MessageHelper::MESSAGE_INFO,
                $this->translator->trans('searchResult.responseType.'.$searchStatus[0])
            );

            // Envoie de l'alerte décès si la date de décès ne correspond pas à ce qui se trouve en base
            $this->sendDeathAlert($person, $searchData, $mailService);

            $searchLog = $this->addSearchLog($searchData, $searchStatus, $person);

            if ($sendMailToInstructor) {
                $validInstructors = $this->findInstructorsForPerson($searchLog, $searchStatus, $personService);

                $this->sendMail(
                    $searchStatus,
                    $mailService,
                    $searchData,
                    $validInstructors,
                    $searchLog,
                    $pdfService,
                    $personService
                );
            }

            asort($searchStatus);
            $searchStatus = array_shift($searchStatus);

            $request->getSession()->getFlashBag()->add('searchLog', $searchLog);

            return $this->redirectToRoute('search_result');
        }

        return $this->render(
            'search/search.html.twig',
            [
                'searchForm' => $form->createView(),
                'persons' => $persons,
            ]
        );
    }

    /**
     * Affiche le résultat d'une recherche.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    #[Route('/search/result', name: 'search_result')]
    public function resultAction(
        Request $request,
        PersonService $personService,
        MailService $mailService,
        SearchLogRepository $searchLogRepository
    ): Response {
        /** @var SearchLog $searchLog */
        $searchLog = $request->getSession()->getFlashBag()->get('searchLog')[0] ?? new SearchLog();
        $showForm = false;

        $form = $this->createForm(DocumentaryType::class, $searchLog);
        $form->handleRequest($request);

        if ($this->settingsData->isDocumentaryExchange()) {
            if (in_array($searchLog->getResponseType()[0] ?? null, explode(',', $this->settingsData->getAnswerCallDocument()))) {
                $showForm = true;
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $this->updateSearchLog($form);

            /** @var SearchLog $searchLog */
            $searchLog = $searchLogRepository->find($form->get('id')->getData());
            $validInstructors = $this->findInstructorsForPerson($searchLog, $searchLog->getResponseType()[0], $personService);

            if (!empty($validInstructors) && $this->settingsData->isSendMailDocumentaryExchange()) {
                $this->sendDocumentMailToInstructors(
                    $searchLog,
                    $validInstructors,
                    $mailService
                );
            }

            $this->addFlash(
                MessageHelper::MESSAGE_INFO,
                $this->translator->trans('searchResult.documentSend')
            );

            return $this->redirectToRoute('search_result');
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $showForm = true;
        }

        return $this->render('search/result.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
        ]);
    }

    /**
     * Affiche les logs de recherche.
     */
    #[Route('/search_logs', name: 'search-logs')]
    public function logsAction(
        Request $request,
        Security $security,
        PersonService $personService,
        SearchLogRepository $searchLogRepository
    ): Response {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(SearchLogsFiltersType::class);
        $form->handleRequest($request);
        $searchLogs = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $currentUser = null;

            if (!$security->isGranted(UserHelper::ROLE_AGENT)) {
                $currentUser = $user;
            }

            $searchLogs = $searchLogRepository->findByFilter($data, $currentUser);
            if ($this->settingsDataService->getData()->hasClaims()) {
                foreach ($searchLogs as $searchLog) {
                    if ($searchLog->getPerson()) {
                        if (!$personService->getPerson($searchLog->getPerson())->getDeathDate()) {
                            $searchLog->setHasPDF(false);
                        }
                    }
                }
            }
        }

        return $this->render(
            'search/logs.html.twig',
            [
                'logsFiltersForm' => $form->createView(),
                'searchLogs' => $searchLogs,
            ]
        );
    }

    /**
     * Retourne le document déchiffrer.
     */
    #[Route('/consultation-de-document/{id}/{type}', name: 'get_document')]
    public function getDocumentAction(FileHelper $fileHelper, SearchLog $searchLog, int $type): Response
    {
        $file = $fileHelper->decryptFile($searchLog, $type);
        if ($file) {
            $response = new Response($file[0]);
            $extension = $file[1];
            $disposition = $response->headers->makeDisposition(
                'pdf' === $extension ? ResponseHeaderBag::DISPOSITION_INLINE : ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                SearchLog::$TYPE_DOCUMENT[$type].'.'.$extension
            );
            if ('pdf' == $extension) {
                $response->headers->set('Content-type', 'application/pdf');
            }
            $response->headers->set('Content-Disposition', $disposition);

            return $response;
        }

        return $this->redirectToRoute('search-logs');
    }

    /**
     * @param Person $person
     */
    public function getPersonDbParameters(Person $person): array
    {
        return [
            'firstNameBdd' => $person->getFirstName(),
            'nameBdd' => strtoupper($person->getName($this->settingsData)),
            'birthDateBdd' => $person->getBirthDate() ? date_format($person->getBirthDate(), 'd/m/Y') : null,
            'deathDateBdd' => $person->getDeathDate() ? date_format($person->getDeathDate(), 'd/m/Y') : null,
            'genderBdd' => $person->getGender(),
            'useNameBdd' => $person->getUseName(),
            'civilNameBdd' => $person->getCivilName(),
            'middleNameBdd' => $person->getMiddleName(),
            'thirdNameBdd' => $person->getThirdName(),
            'addressBdd' => $person->getAddress(),
            'mdtAddressBdd' => $person->getDepartmentalHouseAddress(),
            'mdtBdd' => $person->getDepartmentalHouseName(),
            'mdtPhoneBdd' => $person->getDepartmentalHousePhone(),
            'mdtMailBdd' => $person->getDepartmentalHouseMail(),
            'labelBdd' => $person->getLabel(),
            'helpCodeBdd' => $person->getHelpCode(),
            'personNumBdd' => $person->getPersonNum(),
        ];
    }

    public function getClaimsDbParameters(Person $person): array
    {
        $result = [];
        $result['totalAmountRecoverable'] = 0;
        $result['totalAmountNonRecoverable'] = 0;

        foreach ($person->getClaims() as $claim) {
            $result[$claim->getRecoverable()][] = [
                'numInd' => $claim->getPerson()
                    ->getPersonNum(),
                'startDate' => $claim->getStartDate() ? date_format($claim->getStartDate(), 'd/m/Y') : null,
                'endDate' => $claim->getEndDate() ? date_format($claim->getEndDate(), 'd/m/Y') : null,
                'calculated' => $claim->getCalculatedDate() ? date_format($claim->getCalculatedDate(), 'd/m/Y') : null,
                'amount' => $claim->getAmount(),
                'displayable' => $claim->getDisplayable(),
                'recoverable' => $claim->getRecoverable(),
            ];
            if ($claim->getRecoverable() && $claim->getDisplayable() && $claim->getAmount() > $this->settingsData->getClaimsRecoverableStep()) {
                $result['totalAmountRecoverable'] += $claim->getAmount();
            } elseif (!$claim->getRecoverable() && $claim->getDisplayable() && $claim->getAmount() > $this->settingsData->getClaimsNonRecoverableStep()) {
                $result['totalAmountNonRecoverable'] += $claim->getAmount();
            }
        }

        return $result;
    }

    /**
     * Affiche le PDF associé à une recherche.
     *
     * @throws Html2PdfException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    #[Route('/search_logs/{id}/letter', name: 'letter_pdf')]
    public function showLetterAction(
        SearchLog $searchLog,
        PdfService $pdfService,
        PersonService $personService
    ): ?Response {
        if ($this->isGranted(UserHelper::ROLE_AGENT) || $searchLog->getUser() === $this->getUser()) {
            $this->getPdf($searchLog, 'I', $pdfService, $personService);

            return null;
        } else {
            return $this->render(
                'utils/not_found.html.twig'
            );
        }
    }

    /**
     * Génère le PDF envoyé après une recherche.
     *
     * @throws Html2PdfException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function getPdf(
        SearchLog $searchLog,
        string $dest,
        PdfService $pdfService,
        PersonService $personService
    ): array {
        $pdfTemplate = $this->getPdfTemplate($searchLog->getResponseType());

        $pdfContent = '';
        $fileNameSanitized = FileHelper::sanitizeFileName(
            $searchLog->getId().'_'.($searchLog->getPerson() ? $searchLog->getPerson().'_' : '').
            $searchLog->getFirstName().'_'.$searchLog->getName($this->settingsData)
        );

        $safeFilename = \transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $fileNameSanitized
        );

        $fileName = $safeFilename.'-'.uniqid().'.pdf';

        if ($pdfTemplate) {
            $pdfParameters = [];
            $pdfParameters['reference'] = SearchHelper::getReference($searchLog->getUser(), $searchLog);
            $pdfParameters['appTerritoryInstructor'] = $this->appTerritoryInstructor;
            $pdfParameters['dateSearch'] = date_format($searchLog->getSearchDate(), 'd/m/Y');
            $pdfParameters['officeName'] = $searchLog->getUser()->getName();
            $pdfParameters['officeAddress'] = $searchLog->getUser()->getAddress();
            $pdfParameters['officeAdditionalAddress'] = $searchLog->getUser()->getAdditionalAddress();
            $pdfParameters['officePostalCode'] = $searchLog->getUser()->getPostalCode();
            $pdfParameters['officeTown'] = $searchLog->getUser()->getTown();
            $pdfParameters['deathDateSearch'] = date_format($searchLog->getDeathDate(), 'd/m/Y');

            if ($this->appTerritoryInstructor
                && !in_array(SearchHelper::SEARCH_STATUS_NOTFOUND, $searchLog->getResponseType())
                && !in_array(SearchHelper::SEARCH_STATUS_AMBIGUOUS, $searchLog->getResponseType())
            ) {
                $person = $personService->getPerson($searchLog->getPerson());
                $pdfParameters['mdt'] = $person->getDepartmentalHouseName();
                $pdfParameters['mdtTelephone'] = $person->getDepartmentalHousePhone();
                $pdfParameters['mdtCourriel'] = $person->getDepartmentalHouseMail();
                $pdfParameters['mdtAdresse'] = $person->getDepartmentalHouseAddress();
            }
            if (in_array(SearchHelper::SEARCH_STATUS_NOTFOUND, $searchLog->getResponseType())) {
                $pdfParameters['firstName'] = $searchLog->getFirstName();
                $pdfParameters['name'] = strtoupper($searchLog->getName($this->settingsData));
                $pdfParameters['birthDate'] = $searchLog->getBirthDate() ? date_format($searchLog->getBirthDate(), 'd/m/Y') : null;
                $pdfParameters['responseType'] = $searchLog->getResponseType()[0];
            } else {
                $person = $personService->getPerson($searchLog->getPerson());
                $pdfParametersBdd = $this->getPersonDbParameters($person);
                $pdfParameters = array_merge($pdfParameters, $pdfParametersBdd);
                $pdfParameters['claims'] = $this->getClaimsDbParameters($person);
                $pdfParameters['gender'] = $person->getGender();
                $pdfParameters['firstName'] = $person->getFirstName();
                $pdfParameters['name'] = strtoupper($person->getName($this->settingsData));
                $pdfParameters['birthDate'] = $person->getBirthDate() ? date_format($person->getBirthDate(), 'd/m/Y') : null;
                $pdfParameters['deathDate'] = $person->getDeathDate() ? date_format($person->getDeathDate(), 'd/m/Y') : null;
                $pdfParameters['responseType'] = $searchLog->getResponseType()[0];
                $pdfParameters['helpCodeLabels'] = $this->getHelpCodeLabels($searchLog->getResponseType());
            }

            $instructor = $this->findInstructorsForPerson($searchLog, $searchLog->getResponseType(), $personService);
            if ($instructor) {
                $pdfParameters['instructor'] = reset($instructor);
            }

            $pdfContent = $this->renderView(
                $pdfTemplate,
                $pdfParameters
            );
        }

        $pdfService->create();

        return [$pdfService->generatePdf($pdfContent, $fileName, $dest), $fileName];
    }

    /**
     * Définit le template de pdf en fonction des status de code d'aides.
     */
    private function getPdfTemplate(array $helpCodesStatus): string
    {
        if (in_array(SearchHelper::SEARCH_STATUS_NOTFOUND, $helpCodesStatus)) {
            return 'pdf/search_notfound.html.twig';
        }
        if (in_array(SearchHelper::SEARCH_STATUS_FOUND_REC, $helpCodesStatus)) {
            return 'pdf/search_found_rec.html.twig';
        }

        return 'pdf/search_found_notrec.html.twig';
    }

    /**
     * Ajoute une recherche dans les logs.
     *
     * @param array|int $searchStatus
     *
     * @throws \Exception
     */
    private function addSearchLog(SearchData $searchData, $searchStatus, ?Person $person): SearchLog
    {
        /** @var User $user */
        $user = $this->getUser();

        $searchLog = $searchData->toSearchLog();
        $searchLog->setUser($user);
        $searchLog->setSearchDate(new \DateTime());
        $searchLog->setResponseType($searchStatus);

        if ($person) {
            $searchLog->setPerson($person->getPersonNum());
        }

        $this->entityManager->persist($searchLog);
        $this->entityManager->flush();

        return $searchLog;
    }

    /**
     * Mets à jour une recherche dans les logs pour y ajouter les documents.
     */
    private function updateSearchLog(FormInterface $form): void
    {
        /** @var SearchLog $searchLog */
        $searchLog = $this->searchLogRepository->find($form->get('id')->getData());
        $this->searchLogRepository->update($form, $searchLog);
        $this->entityManager->flush();
    }

    /**
     * Envoie du mail en résultat d'une recherche.
     *
     * @throws Html2PdfException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function sendMail(
        array|int $searchStatus,
        MailService $mailService,
        SearchData $searchData,
        array $instructors = null,
        SearchLog $searchLog = null,
        PdfService $pdfService = null,
        PersonService $personService = null
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $helpCodesLabels = $this->getHelpCodeLabels($searchStatus);
        asort($searchStatus);
        $searchStatus = array_shift($searchStatus);
        $hasPDF = true;

        $mailTemplate = '';
        $mailParameters = [
            'firstName' => $searchData->getFirstName(),
            'middleName' => $searchData->getMiddleName(),
            'thirdName' => $searchData->getThirdName(),
            'useName' => $searchData->getUseName(),
            'civilName' => $searchData->getCivilName(),
            'deathLocation' => $searchData->getDeathLocation(),
            'name' => $searchData->getName($this->settingsData),
            'birthDate' => date_format($searchData->getBirthDate(), 'd/m/Y'),
            'deathDate' => date_format($searchData->getDeathDate(), 'd/m/Y'),
            'libelle_notaire' => $user->getName(),
            'mail_notaire' => $user->getEmail(),
            'recipient' => $searchData->getRecipient(),
        ];

        $subjectParameters = [
            'appName' => $this->settingsData->getAppName(),
            'firstName' => $mailParameters['firstName'],
            'useName' => $mailParameters['useName'],
            'recipient' => $mailParameters['recipient'],
            'middleName' => $mailParameters['middleName'],
            'thirdName' => $mailParameters['thirdName'],
            'civilName' => $mailParameters['civilName'],
            'birthDate' => $mailParameters['birthDate'],
            'deathDate' => $mailParameters['deathDate'],
            'deathLocation' => $mailParameters['deathLocation'],
        ];

        switch ($searchStatus) {
            case SearchHelper::SEARCH_STATUS_FOUND_REC:
            case SearchHelper::SEARCH_STATUS_FOUND_NOTREC:
            case SearchHelper::SEARCH_STATUS_HAS_REQUEST:
                $person = $personService->getPerson($searchLog->getPerson());
                $mailPersonParametersBdd = $this->getPersonDbParameters($person);
                $mailParameters = array_merge($mailParameters, $mailPersonParametersBdd);
                $mailParameters['claims'] = $this->getClaimsDbParameters($person);

                if (SearchHelper::hasLetterPdf($searchStatus) && $this->settingsDataService->getData()->hasClaims() && !$person->getDeathDate()) {
                    $hasPDF = false;
                }

                $mailParameters['rows_person'] = $this->personRepository
                    ->findBy(['personNum' => $person->getPersonNum()]);
                $mailParameters['helpCodeLabels'] = $helpCodesLabels;
                $mailTemplate = 'emails/search_found.html.twig';
                break;
            case SearchHelper::SEARCH_STATUS_NOTFOUND:
                $mailTemplate = 'emails/search_notfound.html.twig';
                break;
            case SearchHelper::SEARCH_STATUS_AMBIGUOUS:
                $mailTemplate = 'emails/search_ambiguous.html.twig';
                break;
        }

        $status = $this->translator->trans('status.'.$searchStatus);
        $mailParameters['hasPDF'] = $hasPDF;

        /** @var User $user */
        $user = $this->getUser();

        if ($searchData->getRecipient()) {
            $mailSubject = $this->translator->trans(
                'searchResultMail.subject.default',
                [
                    '%appName%' => $this->settingsData->getAppName(),
                    '%recipient%' => $searchData->getRecipient(),
                    '%firstName%' => $searchData->getFirstName(),
                    '%name%' => $searchData->getName($this->settingsData),
                    '%status%' => $status,
                ]
            );
        } else {
            $mailSubject = $this->translator->trans(
                'searchResultMail.subject.norecipient',
                [
                    '%appName%' => $this->settingsData->getAppName(),
                    '%firstName%' => $searchData->getFirstName(),
                    '%name%' => $searchData->getName($this->settingsData),
                    '%status%' => $status,
                ]
            );
        }

        $subjectParameters['status'] = $status;
        $subjectParameters['userName'] = $user->getName();

        $mailRecipients = [];
        $mailRecipients[] = $user->getEmail();

        // On envoie le mail aux instructeurs concernés
        if (!empty($instructors)) {
            /** @var Instructor $instructor */
            foreach ($instructors as $instructor) {
                $mailRecipients[] = $instructor->getEmail();
            }
        } elseif (null !== $instructors && SearchHelper::SEARCH_STATUS_NOTFOUND !== $searchStatus) {
            $this->addFlash(
                MessageHelper::MESSAGE_WARNING,
                $this->translator->trans('searchResult.noInstructorFound')
            );
        }

        if ($this->appTerritoryInstructor && isset($person) && '' !== $person->getDepartmentalHouseMail()) {
            $mailRecipients[] = $person->getDepartmentalHouseMail();
            $mailParameters['mdtCourriel'] = $person->getDepartmentalHouseMail();
            $mailParameters['mdtAdresse'] = $person->getDepartmentalHouseAddress();
            $mailParameters['mdtTelephone'] = $person->getDepartmentalHousePhone();
            $mailParameters['mdt'] = $person->getDepartmentalHouseName();
            $mailParameters['appTerritoryInstructor'] = $this->appTerritoryInstructor;
            $mailParameters['showIDT'] = true;
            if (1 === count($helpCodesLabels) and 5 === key($helpCodesLabels)) {
                $mailParameters['showIDT'] = false;
            }
        }

        $mailHtml = $this->getTemplateWithActifTheme(
            $mailTemplate
        );

        $pdfAttachments = [];

        if (SearchHelper::hasLetterPdf($searchStatus) && !$this->settingsDataService->getData()->hasClaims() && $hasPDF) {
            [$pdfContent, $fileName] = $this->getPdf($searchLog, 'S', $pdfService, $personService);
            $pdfAttachments[$fileName] = $pdfContent;
        }

        try {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $mailParameters, $pdfAttachments, $subjectParameters);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }
    }

    /**
     * Envoie du mail de notification en cas de téléversement d'un document.
     *
     * @throws LoaderError
     */
    private function sendDocumentMailToInstructors(
        SearchLog $searchLog,
        array $instructors,
        MailService $mailService
    ): void {
        $mailTemplate = 'emails/has_document.html.twig';
        $mailRecipients = [];
        $url = $this->generateUrl('search-logs', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $mailSubject = $this->translator->trans(
            'documentaryExchange.mail.subject.instructor',
            [
                '%appName%' => $this->settingsData->getAppName(),
                '%firstName%' => $searchLog->getFirstName(),
                '%useName%' => $searchLog->getUseName(),
                '%userName%' => $searchLog->getUser()->getName(),
                '%status%' => $searchLog->getResponseType()[0],
            ]
        );

        $status = '';
        switch ($searchLog->getResponseType()[0]) {
            case SearchHelper::SEARCH_STATUS_FOUND_REC:
                $status = 'connue';
                break;
            case SearchHelper::SEARCH_STATUS_AMBIGUOUS:
                $status = 'ambigüe';
                break;
        }

        $mailParameters = [
            'firstName' => $searchLog->getFirstName(),
            'useName' => $searchLog->getUseName(),
            'status' => $status,
            'url' => $url,
        ];

        // On envoie le mail aux instructeurs concernés
        if (!empty($instructors)) {
            /** @var Instructor $instructor */
            foreach ($instructors as $instructor) {
                $mailRecipients[] = $instructor->getEmail();
            }
        }
        $mailHtml = $this->getTemplateWithActifTheme(
            $mailTemplate
        );

        try {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $mailParameters);
        } catch (TransportExceptionInterface $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        } catch (RfcComplianceException $exception) {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $exception->getMessage());
        }
    }

    /**
     * Création de l'affichage des status pour le rendu mail.
     */
    private function getHelpCodeLabels(array $searchStatus): array
    {
        $helpCodeLabels = [];

        $numberOfStatus = array_count_values($searchStatus);
        foreach (array_unique($searchStatus) as $status) {
            $helpCodeLabels[$status] = $numberOfStatus[$status].' '.$this->translator->trans('responseStatus.'.$status);
        }

        return $helpCodeLabels;
    }

    /**
     * Recherche d'un instructeur en fonction du résultat de la recherche :
     *  - Type de réponse
     *  - Première lettre du nom de l'individu.
     *  - Libéllé d'aide
     */
    private function findInstructorsForPerson(
        SearchLog $searchLog,
        array|int $searchStatus,
        PersonService $personService,
    ): array {
        $validInstructors = [];

        $label = [];
        $name = '';
        if ($searchLog->getPerson() > 0) {
            $persons = $personService->getSamePersons($searchLog->getPerson());
            foreach ($persons as $person) {
                $name = $person->getName($this->settingsData);
                if ('' !== $person->getLabel()) {
                    $label[] = $person->getLabel();
                }
            }
        } else {
            $name = $searchLog->getName($this->settingsData);
        }

        foreach ($this->instructors->findAll() as $instructor) {
            if (is_array($searchStatus)) {
                $affectedInstructor = 1 <= count(array_intersect($searchStatus, $instructor->getResponseType()));
            } else {
                $affectedInstructor = in_array($searchStatus, $instructor->getResponseType());
            }
            // Si l'instructeur est affecté pour ce type de réponse
            if ($affectedInstructor) {
                // L'instructeur est configuré juste pour un Type de réponse (pas d'initiales)

                if (empty($instructor->getInitials())) {
                    $validInstructors[] = $instructor;

                    continue;
                }

                // On test si la personne commence par une des initiales affectées à l'instructeur
                foreach ($instructor->getInitials() as $initial) {
                    if (!$initial) {
                        $validInstructors[] = $instructor;

                        break;
                    }

                    if (preg_match("/^[{$initial}]/i", $name)) {
                        $validInstructors[] = $instructor;

                        break;
                    }
                }
                if ($label) {
                    if ($instructor->getHelpLabel()) {
                        if (empty(array_intersect($instructor->getHelpLabel(), $label))) {
                            $index = array_search($instructor, $validInstructors);
                            if ($this->settingsDataService->getData()->hasInstructorFilter()) {
                                if (false !== $index) {
                                    unset($validInstructors[$index]);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $validInstructors;
    }

    /**
     * @param int  $acceptErrorInBirthDate         Paramètre erreur dans la date de naissance autorisée
     * @param bool $ignoreDifferentFirstNameSearch Paramètre prénom différent donne ambigu au lieu de connue
     */
    private function searchPerson(
        PersonRepository $personRepository,
        SearchData $searchData,
        bool $useSoundex,
        int $acceptErrorInBirthDate,
        bool $ignoreDifferentFirstNameSearch
    ): array {
        // Les doublons de status ne doivent pas être enlevé pour pouvoir garder l'information du nombre d'aides dans le template de mail
        $searchStatus = [];
        $person = null;

        // *************************************************************************
        // Passe 1 : Recherche du nombre de réponses seulement avec les conditions :
        // - précise sur noms et prénoms avec suppression des traits d'unions ou si prénom composé lecture dans les deux sens
        //     OU précise sur les noms et 3 premières lettres du prénom (Si paramètre prénom différent donne ambigu au lieu de connue est désactivé)
        // - contrainte sur date de naissance complète
        // *************************************************************************

        $countPersons = $personRepository->countBySearch(
            $searchData,
            $useSoundex,
            $acceptErrorInBirthDate,
            false,
            $ignoreDifferentFirstNameSearch
        );
        if ($countPersons > 1) {
            $searchStatus[] = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
        } elseif (1 === $countPersons) {
            $persons = $personRepository->findBySearch(
                $searchData,
                $useSoundex,
                $acceptErrorInBirthDate,
                false,
                $ignoreDifferentFirstNameSearch
            );
            foreach ($persons as $person) {
                if (SearchHelper::HELP_CODE_REC === $person->getHelpCode()) {
                    // Si le code est 1SEXTREC alors il y a possibilité de récupération
                    $searchStatus[] = SearchHelper::SEARCH_STATUS_FOUND_REC;
                } elseif (SearchHelper::HELP_CODE_NOTREC === $person->getHelpCode()) {
                    // Si le code est 1SEXTNONR alors il n'y a pas possibilité de récupération
                    $searchStatus[] = SearchHelper::SEARCH_STATUS_FOUND_NOTREC;
                } elseif (empty($person->getHelpCode())) {
                    $searchStatus[] = SearchHelper::SEARCH_STATUS_HAS_REQUEST;
                }
            }

            $person = $persons[0];
        } elseif (0 === $countPersons) {
            // *********************************************************************
            // Passe 2 : La requête est relancée avec filtre sur la date en fonction du paramètre et les 3 premières lettres du prénom (Même si paramètre prénom différent donne ambigu au lieu de connue est activé)
            // *********************************************************************

            $countPersons = $personRepository->countBySearch(
                $searchData,
                $useSoundex,
                $acceptErrorInBirthDate,
                true,
                $ignoreDifferentFirstNameSearch
            );

            if ($countPersons >= 1) {
                $searchStatus[] = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
            } else {
                // ******************************************************************************************
                // Passe 3 : La requête est relancée sans les prénoms mais avec la date de naissance précise
                // ******************************************************************************************
                $countPersons = $personRepository->countBySearch(
                    $searchData,
                    $useSoundex,
                    $acceptErrorInBirthDate,
                    false,
                    $ignoreDifferentFirstNameSearch,
                    true
                );

                if ($countPersons >= 1) {
                    $searchStatus[] = SearchHelper::SEARCH_STATUS_AMBIGUOUS;
                } else {
                    $searchStatus[] = SearchHelper::SEARCH_STATUS_NOTFOUND;
                }
            }
        }

        if (empty($searchStatus)) {
            $searchStatus[] = SearchHelper::SEARCH_STATUS_NOTFOUND;
        }

        return [$searchStatus, $person];
    }

    /**
     * Envoie le mail d'alerte décès.
     *
     * @throws TransportExceptionInterface
     */
    private function sendDeathAlertMail(Person $person, SearchData $searchData, MailService $mailService): void
    {
        /** @var User $user */
        $user = $this->getUser();
        $mailSubject = $this->translator->trans(
            'searchDeathAlertMail.subject.default',
            [
                '%appName%' => $this->settingsData->getAppName(),
            ]
        );

        $mailTemplate = 'emails/death_alert.html.twig';
        $mailParameters = [
            'userName' => $user->getName(),
            'userEmail' => $user->getEmail(),
            'gender' => $person->getGender(),
            'firstName' => $person->getFirstName(),
            'useName' => $person->getUseName(),
            'civilName' => $person->getCivilName(),
            'birthDate' => date_format($person->getBirthDate(), 'd/m/Y'),
            'deathDate' => date_format($searchData->getDeathDate(), 'd/m/Y'),
            'deathLocation' => $searchData->getDeathLocation(),
        ];

        $mailRecipients = [];
        $mailRecipients[] = $this->settingsData->getDeathAlertMail();

        $mailHtml = $this->getTemplateWithActifTheme(
            $mailTemplate
        );

        if (count($mailRecipients) > 0) {
            $mailService->sendHtmlMail($mailRecipients, $mailSubject, $mailHtml, $mailParameters);
        }
    }

    /**
     * @throws TransportExceptionInterface
     */
    private function sendDeathAlert(?Person $person, SearchData $searchData, ?MailService $mailService = null): bool
    {
        if ($person && $this->settingsData->hasDeathAlert() && $this->settingsData->getDeathAlertMail()
            && $searchData->getDeathDate() != $person->getDeathDate()) {
            if ($mailService) {
                $this->sendDeathAlertMail($person, $searchData, $mailService);
            }

            return true;
        }
        return false;
    }
}
