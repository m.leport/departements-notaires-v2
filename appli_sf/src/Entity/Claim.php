<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;

#[ORM\Entity(repositoryClass: "App\Repository\ClaimRepository")]
#[ORM\Table(name: 'creance')]
class Claim
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(name: 'num_ind', type: 'integer')]
    private $numInd;

    #[ORM\Column(name: 'date_deb_periode', type: 'date')]
    private $startDate;

    #[ORM\Column(name: 'date_fin_periode', type: 'date')]
    private $endDate;

    #[ORM\Column(name: 'montant', type: 'float')]
    private $amount;

    #[ORM\Column(name: 'date_calcul', type: 'date')]
    private $calculatedDate;

    #[ORM\Column(name: 'affichable', type: 'boolean')]
    private $displayable;

    #[ORM\Column(name: 'recuperable', type: 'boolean')]
    private $recoverable;

    #[ORM\ManyToMany(targetEntity: "App\Entity\Person", inversedBy: 'claims')]
    #[ORM\JoinTable(name: 'creances_individus')]
    #[JoinColumn(name: 'id_creance', referencedColumnName: 'id')]
    #[InverseJoinColumn(name: 'id_individu', referencedColumnName: 'id')]
    private $persons;

    public function __construct()
    {
        $this->persons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Person
     *
     * Be carefull here : we only send the first element because each element's num_ind refer the same person
     */
    public function getPerson(): Person
    {
        return $this->persons->first();
    }

    public function addPerson(Person $person): self
    {
        if (!$this->persons->contains($person)) {
            $this->persons[] = $person;
        }

        return $this;
    }

    public function removePerson(Person $person): self
    {
        if ($this->persons->contains($person)) {
            $this->persons->removeElement($person);
        }

        return $this;
    }

    public function getNumInd(): int
    {
        return $this->numInd;
    }

    public function setNumInd(int $numInd): self
    {
        $this->numInd = $numInd;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCalculatedDate(): ?\DateTimeInterface
    {
        return $this->calculatedDate;
    }

    public function setCalculatedDate(?\DateTimeInterface $calculatedDate): self
    {
        $this->calculatedDate = $calculatedDate;

        return $this;
    }

    public function getDisplayable(): ?bool
    {
        return $this->displayable;
    }

    public function setDisplayable(bool $displayable): self
    {
        $this->displayable = $displayable;

        return $this;
    }

    public function getRecoverable(): ?bool
    {
        return $this->recoverable;
    }

    public function setRecoverable(bool $recoverable): self
    {
        $this->recoverable = $recoverable;

        return $this;
    }
}
