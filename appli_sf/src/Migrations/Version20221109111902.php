<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221109111902 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add new Claim entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE creance (id INT AUTO_INCREMENT NOT NULL, num_ind INT NOT NULL, date_deb_periode DATE DEFAULT NULL, date_fin_periode DATE DEFAULT NULL, montant DOUBLE PRECISION(10,2) DEFAULT NULL, date_calcul DATE DEFAULT NULL, affichable TINYINT(1) NOT NULL, recuperable TINYINT(1) NOT NULL, PRIMARY KEY (id))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE creance');
    }
}
