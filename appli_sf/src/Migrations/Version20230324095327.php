<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230324095327 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update Person table for integrate Instructor columns';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE individu ADD mdt_adresse VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN mdr mdt varchar(255)');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN telephone mdt_telephone varchar(255)');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN mail_mdr mdt_courriel varchar(255)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE individu DROP mdt_adresse');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN mdt mdr varchar(255)');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN mdt_telephone telephone varchar(255)');
        $this->addSql('ALTER TABLE individu CHANGE COLUMN mdt_courriel mail_mdr varchar(255)');
    }
}
