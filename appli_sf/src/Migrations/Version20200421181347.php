<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200421181347 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('ALTER TABLE instructeur CHANGE type_reponse type_reponse LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', CHANGE initiales initiales LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('ALTER TABLE instructeur CHANGE type_reponse type_reponse INT NOT NULL, CHANGE initiales initiales VARCHAR(20) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
    }
}
