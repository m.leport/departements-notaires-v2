<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220704140428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remove the unicity on num_ind column in the individu table';
    }

    public function up(Schema $schema): void
    {
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('DROP INDEX UNIQ_5EE42FCE1961CB7B ON individu');
    }

    public function down(Schema $schema): void
    {
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('CREATE UNIQUE INDEX UNIQ_5EE42FCE1961CB7B ON individu (num_ind)');
    }
}
