<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200408164927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('CREATE TABLE log_recherche (id INT AUTO_INCREMENT NOT NULL, id_membre INT NOT NULL, id_individu INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATE NOT NULL, date_deces DATE NOT NULL, lieu_deces VARCHAR(255) NOT NULL, date_acte_deces DATE NOT NULL, type_reponse INT NOT NULL, lien_pdf VARCHAR(255) NOT NULL, date_recherche DATETIME NOT NULL, INDEX IDX_A22587C8D0834EC4 (id_membre), INDEX IDX_A22587C8E3FC35B (id_individu), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8D0834EC4 FOREIGN KEY (id_membre) REFERENCES membre (id)');
        $this->addSql('ALTER TABLE log_recherche ADD CONSTRAINT FK_A22587C8E3FC35B FOREIGN KEY (id_individu) REFERENCES individu (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('DROP TABLE log_recherche');
    }
}
