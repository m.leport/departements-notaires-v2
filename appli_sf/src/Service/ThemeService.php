<?php

namespace App\Service;

/**
 * Class ThemeService.
 */
class ThemeService
{
    /**
     * @var string
     */
    protected $appTheme;

    public function __construct(string $appTheme = 'default')
    {
        // Le défaut n'est pas pris en compte dans le cas d'une chaîne de caractère vide dans le .env (exemple APP_THEME=)
        $this->appTheme = '' === $appTheme ? 'default' : $appTheme;
    }

    /**
     * Retourne le thème courant de l'application.
     */
    public function getTheme(): string
    {
        return $this->appTheme;
    }
}
