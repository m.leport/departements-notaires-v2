<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Récupère un individu à partir de son num_ind.
     */
    public function getPerson(int $personNum): Person
    {
        /** @var Person|null $person */
        $person = $this->entityManager->getRepository(Person::class)->findOneBy(['personNum' => $personNum]);

        return $person;
    }

    /**
     * Récupère les individus commun à partir de leurs num_ind.
     */
    public function getSamePersons(int $personNum): array
    {
        return $this->entityManager->getRepository(Person::class)->findBy(['personNum' => $personNum]);
    }
}
