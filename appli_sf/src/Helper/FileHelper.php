<?php

namespace App\Helper;

use App\Entity\SearchLog;

class FileHelper
{
    /** @var string */
    private $encryptionKey;

    public function __construct(string $encryptionKey)
    {
        $this->encryptionKey = $encryptionKey;
    }

    /**
     * Retourne un nom de fichier sans caractères spéciaux.
     */
    public static function sanitizeFileName(string $filename): string
    {
        $dangerous_characters = [' ', '"', "'", '&', '/', '\\', '?', '#'];

        return str_replace($dangerous_characters, '_', $filename);
    }

    public function decryptFile(SearchLog $searchLog, int $type): ?array
    {
        $cipher = 'aes-256-cbc';
        $ivLenght = openssl_cipher_iv_length($cipher);

        $path = '';

        switch ($type) {
            case 1:
                $path = $searchLog->getDeathDocument();
                break;
            case 2:
                $path = $searchLog->getNetDocument();
                break;
            case 3:
                $path = $searchLog->getOtherDocument();
                break;
            default:
                return null;
        }

        $fpSource = fopen($path, 'rb');
        $tempFilePath = tempnam(sys_get_temp_dir(), 'decrypted_file');

        $fpDest = fopen($tempFilePath.'.'.pathinfo($path, PATHINFO_EXTENSION), 'w');
        $iv = fread($fpSource, $ivLenght);

        while (!feof($fpSource)) {
            $ciphertext = fread($fpSource, $ivLenght * (10000 + 1));
            $plaintext = openssl_decrypt($ciphertext, $cipher, $this->encryptionKey, OPENSSL_RAW_DATA, $iv);
            $iv = substr($plaintext, 0, $ivLenght);

            fwrite($fpDest, $plaintext);
        }

        return [file_get_contents($tempFilePath.'.'.pathinfo($path, PATHINFO_EXTENSION)), pathinfo($path, PATHINFO_EXTENSION)];
    }
}
