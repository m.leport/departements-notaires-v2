<?php

namespace App\Helper;

use App\Entity\SearchLog;
use App\Entity\User;

class SearchHelper
{
    public const HELP_CODE_REC = '1SEXTREC';
    public const HELP_CODE_NOTREC = '1SEXTNONR';

    public const SEARCH_STATUS_FOUND_REC = 1;
    public const SEARCH_STATUS_FOUND_NOTREC = 2;
    public const SEARCH_STATUS_NOTFOUND = 3;
    public const SEARCH_STATUS_AMBIGUOUS = 4;
    public const SEARCH_STATUS_HAS_REQUEST = 5;

    public const SEARCH_BY_USE_NAME = 1;
    public const SEARCH_BY_CIVIL_NAME = 2;

    public const SEARCH_DIFFERENT_DAY_AND_MONTH = 1;
    public const SEARCH_DIFFERENT_DAY = 2;
    public const SEARCH_DIFFERENT_MONTH = 3;
    public const SEARCH_DIFFERENT_YEAR = 4;
    public const SEARCH_DIFFERENT_BIRTHDAY = 5;

    /**
     * Retourne la liste des types de réponse (format pour les select HTML).
     *
     * @return array|int[]
     */
    public static function getResponseTypesChoices(): array
    {
        return [
            'responseType.'.self::SEARCH_STATUS_FOUND_REC => self::SEARCH_STATUS_FOUND_REC,
            'responseType.'.self::SEARCH_STATUS_FOUND_NOTREC => self::SEARCH_STATUS_FOUND_NOTREC,
            'responseType.'.self::SEARCH_STATUS_NOTFOUND => self::SEARCH_STATUS_NOTFOUND,
            'responseType.'.self::SEARCH_STATUS_AMBIGUOUS => self::SEARCH_STATUS_AMBIGUOUS,
            'responseType.'.self::SEARCH_STATUS_HAS_REQUEST => self::SEARCH_STATUS_HAS_REQUEST,
        ];
    }

    /**
     * Retourne la liste des types de réponse pour les instructeurs (format pour les select HTML).
     *
     * @return array|int[]
     */
    public static function getResponseTypesChoicesForInstructor(): array
    {
        return [
            'responseType.'.self::SEARCH_STATUS_FOUND_REC => self::SEARCH_STATUS_FOUND_REC,
            'responseType.'.self::SEARCH_STATUS_FOUND_NOTREC => self::SEARCH_STATUS_FOUND_NOTREC,
            'responseType.'.self::SEARCH_STATUS_AMBIGUOUS => self::SEARCH_STATUS_AMBIGUOUS,
            'responseType.'.self::SEARCH_STATUS_HAS_REQUEST => self::SEARCH_STATUS_HAS_REQUEST,
        ];
    }

    /**
     * Retourne la référence utilisé dans les PDF.
     */
    public static function getReference(User $user, SearchLog $searchLog): string
    {
        return $user->getUsername().' '.$user->getName().'/Succession '.$searchLog->getFirstName().' '.
            $searchLog->getUseName();
    }

    /**
     * Détermine si le type de réponse nécessite l'envoie d'un PDF.
     *
     * @param array|int $responseType
     */
    public static function hasLetterPdf($responseType): bool
    {
        $pdfAcceptedStatus = [
            self::SEARCH_STATUS_FOUND_REC,
            self::SEARCH_STATUS_FOUND_NOTREC,
            self::SEARCH_STATUS_NOTFOUND,
        ];

        if (!is_array($responseType)) {
            return in_array($responseType, $pdfAcceptedStatus);
        }

        return 1 <= count(array_intersect($pdfAcceptedStatus, $responseType));
    }
}
