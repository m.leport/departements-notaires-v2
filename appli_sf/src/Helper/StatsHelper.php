<?php

namespace App\Helper;

use Symfony\Contracts\Translation\TranslatorInterface;

class StatsHelper
{
    /**
     * Transforme le tableau de stats pour l'affichage en HTML
     * TODO : Peut-être trouver un truc plus "joli".
     */
    public static function verticalToHorizontalStatsArray(array $stats, TranslatorInterface $translator): array
    {
        $horizontalStats = [
            0 => [$translator->trans('stats.nbRequests')],
            1 => [$translator->trans('stats.nbResponses')],
            2 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FOUND_REC)],
            3 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_FOUND_NOTREC)],
            4 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_HAS_REQUEST)],
            5 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_AMBIGUOUS)],
            6 => [$translator->trans('stats.nbResponsesByType.'.SearchHelper::SEARCH_STATUS_NOTFOUND)],
            7 => [$translator->trans('stats.nbConnections')],
            8 => [$translator->trans('stats.nbUniqueUsers')],
        ];

        foreach ($stats as $monthStat) {
            $horizontalStats[0][] = $monthStat['nbRequests'];
            $horizontalStats[1][] = $monthStat['nbResponses'];
            $horizontalStats[2][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_FOUND_REC];
            $horizontalStats[3][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_FOUND_NOTREC];
            $horizontalStats[4][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_HAS_REQUEST];
            $horizontalStats[5][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_AMBIGUOUS];
            $horizontalStats[6][] = $monthStat['nbResponsesByType'][SearchHelper::SEARCH_STATUS_NOTFOUND];
            $horizontalStats[7][] = $monthStat['nbConnections'];
            $horizontalStats[8][] = $monthStat['nbUniqueUsers'];
        }

        return $horizontalStats;
    }
}
