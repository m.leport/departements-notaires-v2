<?php

namespace App\Helper;

class MessageHelper
{
    public const MESSAGE_ERROR = 'danger';
    public const MESSAGE_WARNING = 'warning';
    public const MESSAGE_SUCCESS = 'success';
    public const MESSAGE_INFO = 'info';
}
