<?php

namespace App\Helper;

use Graph;
use JpGraph\JpGraph;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Utilitaire pour générer le graphique des statistiques de recherche et connexion.
 */
class GraphHelper
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        JpGraph::load();
        JpGraph::module('bar');
        JpGraph::module('line');

        $this->translator = $translator;
    }

    /**
     * A partir des données brutes renvoyées par getStatsRecherch() et getStatsConnexion(), renvoie les séries sous
     * forme de tableaux indépendants. Si $dateFrom est supérieur à $dateTo, le tri se fait du mois le plus récent au
     * mois le plus ancien.
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param array     $stats    [ ["mois" => "2018-01", "reponse"=>1, "nombre"=>50] [...] ]
     *
     * @return array tableau contenant les données pour chaque série, les séries étant identifiées par les index
     *               "month", "requests", "foundRec", "foundNotRec", "hasRequest", "notFound", "ambiguous", "connections", "uniqueUsers"
     *
     * @throws \Exception
     */
    public function getStatsSeries($dateFrom, $dateTo, $stats)
    {
        $months = [];
        $monthsIndex = [];
        $monthFormater = new \IntlDateFormatter(
            null,
            \IntlDateFormatter::NONE,
            \IntlDateFormatter::NONE
        );
        $monthFormater->setPattern('MMMM-yy');

        $oneMonthInterval = new \DateInterval('P1M1D'); // 1 mois on enlève un jour car les années bisextiles casse l'affichage
        // Déduit les valeurs des mois en fonction des 2 bornes de date
        $i = 0;
        $nbMonths = 0;

        // Si la date de départ est plus récente, il faut inverser l'ordre des tableaux et le sens du parcours
        if ($dateFrom > $dateTo) {
            // On affiche d'abord le mois le plus récent
            $stats = array_reverse($stats);
            $dateCur = clone $dateFrom; // on part de la date la plus récente, ne pas altérer l'original

            while ($dateCur >= $dateTo) {
                $monthsIndex[$dateCur->format(
                    'Y-m'
                )] = $i; // on associe pour chaque mois l'index à utiliser dans chaque tableau
                $months[] = $monthFormater->format($dateCur); // ex : janvier-18

                // mois précédent
                $dateCur->sub($oneMonthInterval);
                ++$i;
                ++$nbMonths;
            }

            // Inverser les indices des mois
            foreach ($monthsIndex as $key => $value) {
                $monthsIndex[$key] = $nbMonths - $value - 1;
            }
        } else {
            // On parcourt du mois le plus ancien au plus récent
            $dateCur = clone $dateFrom; // ne pas altérer l'original
            while ($dateCur <= $dateTo) {
                $monthsIndex[$dateCur->format(
                    'Y-m'
                )] = $i; // on associe pour chaque mois l'index à utiliser dans chaque tableau
                $months[] = $dateCur->format('M-y'); // ex : janvier-18
                // mois suivant
                $dateCur->add($oneMonthInterval);
                ++$i;
                ++$nbMonths;
            }
        }

        // Dans $all, on utilise l'indice 0 pour stocker le nombre total de demandes, les indices 1 à 5 stockent le nombre de
        // réponses par type recuperables, indus, en cours d'instruction, inconnus, ambigus
        // Transformation du tableau $stat :
        // mois("2022-04") => ["nbResponsesByType" => [typeRéponse => nbRéponse]] en typeRéponse => [mois(0) => nbRéponses]
        $cloneStats = $stats;
        $all = [];
        foreach ($cloneStats as $month => $responseType) {
            if (array_key_exists($month, $monthsIndex)) {
                $monthIndex = $monthsIndex[$month];
                $all[0][$monthIndex] = $responseType['nbRequests']; // toutes les demandes
                foreach ($responseType as $responseTypeLabel => $responseTypeValue) {
                    if (is_array($responseTypeValue)) {
                        foreach ($responseTypeValue as $responseTypeId => $value) {
                            $all[$responseTypeId][$monthIndex] = $value;
                        }
                    }
                }
            }
        }

        // Parcourt les stats connexion pour renseigner les tableaux $connexions et $utilisateurs
        $connections = [];
        $uniqueUsers = [];
        // $month are stored from 0 to 4 in $monthsIndex
        for ($i = 0; $i <= sizeof($months) - 1; ++$i) {
            $connections[$i] = 0;
            $uniqueUsers[$i] = 0;
        }

        foreach ($stats as $month => $ligne) {
            $monthLine = $month; // yyyy-mm
            $requestsLine = $ligne['nbConnections'];
            $uniqueUsersLine = $ligne['nbUniqueUsers'];

            if (array_key_exists($monthLine, $monthsIndex)) {
                $monthIndex = $monthsIndex[$monthLine];
                $connections[$monthIndex] += $requestsLine;
                $uniqueUsers[$monthIndex] += $uniqueUsersLine;
            }
        }

        $requests = $all[0];
        $foundRec = $all[1];
        $notFoundRec = $all[2];
        $notFound = $all[3];
        $ambiguous = $all[4];
        $hasRequest = $all[5];

        // Il faut inverser les indices dans le cas d'un affichage du mois le plus récent au plus ancien
        if ($dateFrom > $dateTo) {
            $requests = self::arrayReverse($requests);
            $notFound = self::arrayReverse($notFound);
            $ambiguous = self::arrayReverse($ambiguous);
            $notFoundRec = self::arrayReverse($notFoundRec);
            $hasRequest = self::arrayReverse($hasRequest);
            $foundRec = self::arrayReverse($foundRec);
            $connections = self::arrayReverse($connections);
            $uniqueUsers = self::arrayReverse($uniqueUsers);
        }
        return [
            'month' => $months,
            'requests' => $requests,
            'notFound' => $notFound,
            'ambiguous' => $ambiguous,
            'foundNotRec' => $notFoundRec,
            'hasRequest' => $hasRequest,
            'foundRec' => $foundRec,
            'connections' => $connections,
            'uniqueUsers' => $uniqueUsers,
        ];
    }

    /**
     * La fonction native array_reverse ne change pas les indices numériques malgré ce qui est écrit dans la doc.
     *
     * @param array $tab
     *
     * @return array les valeurs avec les indices inversés
     */
    private static function arrayReverse($tab)
    {
        $ret = [];
        $c = count($tab);
        for ($i = 0; $i < $c; ++$i) {
            $ret[$i] = $tab[$c - $i - 1];
        }

        return $ret;
    }

    /**
     * Génère l'image des satistiques.
     * Si $dateFrom est supérieur à $dateTo, l'affichage se fait du mois le plus récent (à gauche) au mois le plus
     * ancien
     * (à droite).
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param array     $stats
     * @param bool      $outputInFile
     *
     * @return bool|false|string Nom du ficher img ou true si rendu direct
     *
     * @throws \Exception
     */
    public function render($dateFrom, $dateTo, $stats, $outputInFile = false)
    {
        $series = self::getStatsSeries($dateFrom, $dateTo, $stats);

        return $this->renderProcess(
            $series['month'],
            $series['requests'],
            $series['notFound'],
            $series['ambiguous'],
            $series['foundNotRec'],
            $series['hasRequest'],
            $series['foundRec'],
            $series['connections'],
            $outputInFile
        );
    }

    /**
     * Génère l'image dans un fichier ou sur le stream directement.
     *
     * @param array $month        [ "janvier-19", "février-19", ...]
     * @param array $requests     de taille identique à $mois, total du nombre de demandes pour chaque mois
     * @param array $notFound     de taille identique à $mois, total du nombre de réponses "inconnu" pour chaque mois
     * @param array $ambiguous    de taille identique à $mois, total du nombre de réponses "ambigu" pour chaque mois
     * @param array $notFoundRec  de taille identique à $mois, total du nombre de réponses "indu" pour chaque mois
     * @param array $notFoundRec  de taille identique à $mois, total du nombre de réponses "en cours d'instruction" pour chaque mois
     * @param array $foundRec     de taille identique à $mois, total du nombre de réponses "récupérable" pour chaque
     *                            mois
     * @param array $connections  de taille identique à $mois, nombre de connexions utilisateur
     * @param bool  $outputInFile
     *
     * @return bool|string
     */
    private function renderProcess(
        $month,
        $requests,
        $notFound,
        $ambiguous,
        $notFoundRec,
        $hasRequest,
        $foundRec,
        $connections,
        $outputInFile = false
    ) {
        /** @var \Graph $graph */
        $graph = new \Graph(1024, 510, 'auto');
        $graph->SetScale('textlin');
        $graph->SetColor('#F2F2F2');
        $graph->SetY2Scale('lin', 0, 900);
        $graph->SetY2OrderBack(false);

        $graph->SetMargin(35, 50, 20, 5);

        $theme_class = new \UniversalTheme();
        $graph->SetTheme($theme_class);

        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels(['A', 'B', 'C', 'D']);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);
        // Setup month as labels on the X-axis
        $graph->xaxis->SetTickLabels($month);

        // Create the bar plots
        // Récupérables
        $b1plot = new \BarPlot($foundRec);
        // Indus Probables
        $b2plot = new \BarPlot($notFoundRec);
        // Aides en cours d'instruction
        $b3plot = new \BarPlot($hasRequest);
        // Ambigus
        $b4plot = new \BarPlot($ambiguous);
        // Demandes
        $b5plot = new \BarPlot($requests);
        // Inconnus
        $b6plot = new \BarPlot($notFound);

        // Connexions
        $lplot = new \LinePlot($connections);

        // Create the grouped bar plot
        $gbplot = new \AccBarPlot([$b1plot, $b2plot, $b3plot, $b4plot, $b6plot]);
        $gbbplot = new \GroupBarPlot([$b5plot, $gbplot]);

        // ...and add it to the graPH
        $graph->Add($gbbplot);
        $graph->AddY2($lplot);

        // Récupérables
        $b1plot->SetColor('#1D6F99');
        $b1plot->SetFillColor('#1D6F99');
        $b1plot->SetLegend($this->translator->trans('stats.graph.foundRec'));

        // Indus Probables
        $b2plot->SetColor('#FFBEB5');
        $b2plot->SetFillColor('#FFBEB5');
        $b2plot->SetLegend($this->translator->trans('stats.graph.notFoundRec'));
        // Aides En cours d'instruction
        $b3plot->SetColor('#B1DCCB');
        $b3plot->SetFillColor('#B1DCCB');
        $b3plot->SetLegend($this->translator->trans('stats.graph.hasRequest'));
        // Ambigus
        $b4plot->SetColor('#CC646C');
        $b4plot->SetFillColor('#CC646C');
        $b4plot->SetLegend($this->translator->trans('stats.graph.ambiguous'));
        // Demandes
        $b5plot->SetColor('#d2bf64');
        $b5plot->SetFillColor('#d2bf64');
        $b5plot->SetLegend($this->translator->trans('stats.graph.requests'));
        $b5plot->value->SetFormat('%d');
        $b5plot->value->Show();
        $b5plot->value->SetColor('#d2bf64');
        // Inconnus
        $b6plot->SetColor('#64A8CC');
        $b6plot->SetFillColor('#64A8CC');
        $b6plot->SetLegend($this->translator->trans('stats.graph.notFound'));
        // Connexions
        $lplot->SetBarCenter();
        $lplot->SetColor('black');
        $lplot->SetLegend($this->translator->trans('stats.graph.connections'));
        $lplot->mark->SetType(MARK_SQUARE, '', 1.0);
        $lplot->mark->SetWeight(2);
        $lplot->mark->SetWidth(5);
        $lplot->mark->setColor('black');
        $lplot->mark->setFillColor('black');
        $lplot->value->SetFormat('%d');
        $lplot->value->Show();
        $lplot->value->SetColor('black');

        $graph->legend->SetFrameWeight(1);
        $graph->legend->SetColumns(7);
        $graph->legend->SetColor('#9b9b9b', 'black');
        $graph->legend->Pos(0.5, 0.9, 'center', 'top');

        $band = new \PlotBand(VERTICAL, BAND_RDIAG, 11, 'max', 'khaki4');
        $band->ShowFrame(true);
        $band->SetOrder(DEPTH_BACK);
        $graph->Add($band);

        $graph->title->Set($this->translator->trans('stats.graph.title'));

        // Génère
        if ($outputInFile) {
            $tempName = tempnam(sys_get_temp_dir(), 'graph-stats_').'.png';
            $graph->Stroke($tempName);

            return $tempName;
        } else {
            $graph->Stroke();

            return true;
        }
    }
}
