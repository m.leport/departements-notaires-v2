<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function boot(): void
    {
        parent::boot();
        $uploadsDir = $this->getProjectDir() . '/public/uploads/settings/images';
        if (!is_dir($uploadsDir)) {
            mkdir($uploadsDir, 0755, true);
        }
        in_array($this->getContainer()->getParameter('timezone'), timezone_identifiers_list()) ?
            date_default_timezone_set($this->getContainer()->getParameter('timezone'))
            : date_default_timezone_set('Europe/Paris');
    }
}
