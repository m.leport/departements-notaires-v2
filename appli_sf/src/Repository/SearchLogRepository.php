<?php

namespace App\Repository;

use App\Entity\SearchLog;
use App\Entity\User;
use App\Helper\UploaderHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\FormInterface;

/**
 * @method SearchLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method SearchLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method SearchLog[]    findAll()
 * @method SearchLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchLogRepository extends ServiceEntityRepository
{
    private UploaderHelper $uploaderHelper;

    public function __construct(ManagerRegistry $registry, UploaderHelper $uploaderHelper)
    {
        parent::__construct($registry, SearchLog::class);
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * Liste les noms uniques (civil ou usage).
     */
    public function findNames(?User $user): array
    {
        // Liste des noms d'usage uniques
        $queryBuilder = $this->createQueryBuilder('s')
            ->groupBy('s.useName')
            ->orderBy('s.useName', 'ASC');

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                ->setParameter(':user', $user);
        }

        $useNames = $queryBuilder->getQuery()->getResult();

        // Liste des noms civil uniques
        $queryBuilder = $this->createQueryBuilder('s')
            ->groupBy('s.civilName')
            ->orderBy('s.civilName', 'ASC');

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                ->setParameter(':user', $user);
        }

        $civilNames = $queryBuilder->getQuery()->getResult();

        return array_merge($useNames, $civilNames);
    }

    /**
     * Liste les logs selon les critères de filtrage.
     *
     * @return int|mixed|string
     */
    public function findByFilter(array $filterData, ?User $user)
    {
        $queryBuilder = $this->createQueryBuilder('s');

        if (!$user && array_key_exists('user', $filterData) && $filterData['user'] instanceof User) {
            $user = $filterData['user'];
        }

        if ($user) {
            $queryBuilder->andWhere('s.user = :user')
                         ->setParameter(':user', $user);
        }

        if ($filterData['responseType']) {
            $queryBuilder->andWhere('s.responseType LIKE :firstPattern OR s.responseType LIKE :secondPattern OR s.responseType LIKE :thirdPattern OR s.responseType LIKE :fourthPattern')
                         ->setParameter('firstPattern', '['.$filterData['responseType'].',%')
                         ->setParameter('secondPattern', '%,'.$filterData['responseType'].']')
                         ->setParameter('thirdPattern', '%,'.$filterData['responseType'].',%')
                         ->setParameter('fourthPattern', '['.$filterData['responseType'].']');
        }

        if ($filterData['name']) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('s.useName', ':name'),
                    $queryBuilder->expr()->eq('s.civilName', ':name')
                )
            )->setParameter('name', $filterData['name']);
        }

        if ($filterData['startDate'] instanceof \DateTime) {
            $queryBuilder->andWhere('s.searchDate >= :startDate')
                ->setParameter('startDate', $filterData['startDate']);
        }

        if ($filterData['endDate'] instanceof \DateTime) {
            $queryBuilder->andWhere('s.searchDate <= :endDate')
                ->setParameter('endDate', $filterData['endDate']->setTime(23, 59, 59));
        }
        $queryBuilder->orderBy('s.searchDate', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Récupère les stats de recherche (nombre de recherche pas type de réponse)
     * pour chaque mois dans un interval de temps.
     *
     * @return array
     */
    public function findSearchStats(array $nbResponsesByTypeArray): array
    {
        $query = $this->createQueryBuilder('s')
            ->select("DATE_FORMAT(s.searchDate, '%Y-%m') as month", 's.responseType')
            ->orderBy('month', 'DESC')
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($query as $monthAndResponseLog) {
            $result[$monthAndResponseLog['month']][] = array_count_values($monthAndResponseLog['responseType']);
        }

        foreach ($result as $month => $responses) {
            $typeResponses = $nbResponsesByTypeArray;
            foreach ($responses as $response) {
                foreach ($response as $responseType => $responseCount) {
                    $typeResponses[$responseType] += $responseCount;
                }
            }
            $result[$month] = $typeResponses;
        }

        return $result;
    }

    /**
     * Récupère les nombres de recherche
     * pour chaque mois dans un interval de temps.
     */
    public function findNumberSearchStats(): array
    {
        $query = $this->createQueryBuilder('s')
            ->select("DATE_FORMAT(s.searchDate, '%Y-%m') as month", 'COUNT(s.id) as count')
            ->orderBy('month', 'DESC')
            ->groupBy('month')
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($query as $monthAndResponseLog) {
            $result[$monthAndResponseLog['month']] = $monthAndResponseLog['count'];
        }

        return $result;
    }

    public function update(FormInterface $form, SearchLog $searchLog): void
    {
        $folder = $form->get('person')->getData() ?? 'ambiguous';
        if ($form->get('deathDocument')->getData()) {
            $searchLog->setDeathDocument($this->uploaderHelper->uploadPrivateFile(
                $form->get('deathDocument')->getData(),
                UploaderHelper::DOCUMENTARY_EXCHANGE.'/'.$folder
            ));
        }

        if ($form->get('netDocument')->getData()) {
            $searchLog->setNetDocument($this->uploaderHelper->uploadPrivateFile(
                $form->get('netDocument')->getData(),
                UploaderHelper::DOCUMENTARY_EXCHANGE.'/'.$folder
            ));
        }

        if ($form->get('otherDocument')->getData()) {
            $searchLog->setOtherDocument($this->uploaderHelper->uploadPrivateFile(
                $form->get('otherDocument')->getData(),
                UploaderHelper::DOCUMENTARY_EXCHANGE.'/'.$folder
            ));
        }
    }

    public function findByDocument(string $fileName): array
    {
        return $this->createQueryBuilder('s')
            ->where('s.deathDocument = :fileName')
            ->orWhere('s.netDocument = :fileName')
            ->orWhere('s.otherDocument = :fileName')
            ->setParameter('fileName', $fileName)
            ->getQuery()
            ->getResult();
    }
}
