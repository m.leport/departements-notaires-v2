<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Person;
use App\Helper\SearchHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $sqlLogger)
    {
        parent::__construct($registry, Person::class);

        $this->logger = $sqlLogger;
    }

    /**
     * Compte le nombre de résultats de recherche.
     *
     * @param SearchData $data                           Données de recherche
     * @param bool       $useSoundex                     Utiliser soundex
     * @param int        $acceptErrorInBirthDate         Paramètre erreur dans la date de naissance autorisée
     * @param bool       $activeFilterBirthDate          Deuxième recherche avec filtre sur la date
     * @param bool       $ignoreDifferentFirstNameSearch Paramètre prénom différent donne ambigu au lieu de connue
     * @param bool       $withoutFirstName               Ignorer le prénom
     * @param bool       $onlyNullHelpCode               Individu sans code d'aide (demande en cours)
     *
     * @return int Nombre de résultats de recherche
     */
    public function countBySearch(
        SearchData $data,
        bool $useSoundex,
        int $acceptErrorInBirthDate,
        bool $activeFilterBirthDate = false,
        bool $ignoreDifferentFirstNameSearch = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ): int {
        $queryBuilder = $this->createQueryBuilder('p');
        // la fonction countBySearch() renvoie un nombre d'individu DISTINCT lié à leur numéro d'individu :
        // -> deux personnes ayant le même numéro d'individu seront alors considérés comme une seule et même personne
        $queryBuilder->select('COUNT(DISTINCT p.personNum)');
        $query = null;

        if ($useSoundex) {
            $query = $this->searchWithSoundex(
                $queryBuilder,
                $data,
                $acceptErrorInBirthDate,
                $activeFilterBirthDate,
                $ignoreDifferentFirstNameSearch,
                $withoutFirstName,
                $onlyNullHelpCode
            );
        } else {
            $query = $this->search(
                $queryBuilder,
                $data,
                $acceptErrorInBirthDate,
                $activeFilterBirthDate,
                $ignoreDifferentFirstNameSearch,
                $withoutFirstName
            );
        }

        if (null != $query) {
            $this->logQuery($query);
        }

        return $query->getSingleScalarResult();
    }

    /**
     * Retourne les individus trouvé par la recherche.
     *
     * @param SearchData $data                           Données de recherche
     * @param bool       $useSoundex                     Utiliser soundex
     * @param int        $acceptErrorInBirthDate         Paramètre erreur dans la date de naissance autorisée
     * @param bool       $activeFilterBirthDate          Deuxième recherche avec filtre sur la date
     * @param bool       $ignoreDifferentFirstNameSearch Paramètre prénom différent donne ambigu au lieu de connue
     * @param bool       $withoutFirstName               Ignorer le prénom
     * @param bool       $onlyNullHelpCode               Individu sans code d'aide (demande en cours)
     *
     * @return Person[]
     */
    public function findBySearch(
        SearchData $data,
        bool $useSoundex,
        int $acceptErrorInBirthDate,
        bool $activeFilterBirthDate = false,
        bool $ignoreDifferentFirstNameSearch = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ): array {
        $queryBuilder = $this->createQueryBuilder('p');
        $query = null;

        if ($useSoundex) {
            $query = $this->searchWithSoundex(
                $queryBuilder,
                $data,
                $acceptErrorInBirthDate,
                $activeFilterBirthDate,
                $ignoreDifferentFirstNameSearch,
                $withoutFirstName,
                $onlyNullHelpCode
            );
        } else {
            $query = $this->search(
                $queryBuilder,
                $data,
                $acceptErrorInBirthDate,
                $activeFilterBirthDate,
                $ignoreDifferentFirstNameSearch,
                $withoutFirstName
            );
        }

        if (null != $query) {
            $this->logQuery($query);
        }

        return $query->getResult();
    }

    /**
     * Recherche avec soundex.
     *
     * @param SearchData $data                           Données de recherche
     * @param int        $acceptErrorInBirthDate         Paramètre erreur dans la date de naissance autorisée
     * @param bool       $activeFilterBirthDate          Deuxième recherche avec filtre sur la date
     * @param bool       $ignoreDifferentFirstNameSearch Paramètre prénom différent donne ambigu au lieu de connue
     * @param bool       $onlyNullHelpCode               Individu sans code d'aide (demande en cours)
     * @param bool       $withoutFirstName               Ignorer le prénom
     *
     * @return Query
     */
    private function searchWithSoundex(
        QueryBuilder &$queryBuilder,
        SearchData $data,
        int $acceptErrorInBirthDate,
        bool $activeFilterBirthDate = false,
        bool $ignoreDifferentFirstNameSearch = false,
        bool $withoutFirstName = false,
        bool $onlyNullHelpCode = false
    ): Query {
        $parameters = new ArrayCollection();
        if ($data->isComposeName()) {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:reverseUseName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:reverseCivilName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:reverseUseName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:reverseCivilName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:useName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:civilName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:useName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:civilName)')
            );

            $parameters->add(
                new Parameter(
                    'reverseUseName',
                    $data->reverseComposeName(null !== $data->getUseName() ? $data->getUseName() : '')
                )
            );
            $parameters->add(
                new Parameter(
                    'reverseCivilName',
                    $data->reverseComposeName(null !== $data->getCivilName() ? $data->getCivilName() : '')
                )
            );
            $parameters->add(new Parameter('useName', $data->getUseName()));
            $parameters->add(new Parameter('civilName', $data->getCivilName()));
        } elseif ($activeFilterBirthDate) {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('p.useName', ':nameWithoutHyphens1'),
                $queryBuilder->expr()->like('p.useName', ':nameWithoutHyphens2'),
                $queryBuilder->expr()->like('p.civilName', ':nameWithoutHyphens1'),
                $queryBuilder->expr()->like('p.civilName', ':nameWithoutHyphens2')
            );
            $parameters->add(
                new Parameter(
                    'nameWithoutHyphens1',
                    '%'.$data->getSingleNameWithoutHyphens().'%'
                )
            );
            $parameters->add(
                new Parameter(
                    'nameWithoutHyphens2',
                    '%-'.$data->getSingleNameWithoutHyphens().'%'
                )
            );
        } else {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:useName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.useName)', 'SOUNDEX(:civilName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:useName)'),
                $queryBuilder->expr()->eq('SOUNDEX(p.civilName)', 'SOUNDEX(:civilName)')
            );
            $parameters->add(new Parameter('useName', $data->getUseName()));
            $parameters->add(new Parameter('civilName', $data->getCivilName()));
        }

        if ($withoutFirstName) {
            $orNames = $lastNames;
        } else {
            if (!$ignoreDifferentFirstNameSearch && !$activeFilterBirthDate) {
                $firstNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:firstName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:middleName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.firstName)', 'SOUNDEX(:thirdName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:firstName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:middleName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.middleName)', 'SOUNDEX(:thirdName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:firstName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:middleName)'),
                    $queryBuilder->expr()->eq('SOUNDEX(p.thirdName)', 'SOUNDEX(:thirdName)')
                );
            } else {
                $firstNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->eq('p.firstName', ':firstName'),
                    $queryBuilder->expr()->eq('p.firstName', ':middleName'),
                    $queryBuilder->expr()->eq('p.firstName', ':thirdName'),
                    $queryBuilder->expr()->eq('p.middleName', ':firstName'),
                    $queryBuilder->expr()->eq('p.middleName', ':middleName'),
                    $queryBuilder->expr()->eq('p.middleName', ':thirdName'),
                    $queryBuilder->expr()->eq('p.thirdName', ':firstName'),
                    $queryBuilder->expr()->eq('p.thirdName', ':middleName'),
                    $queryBuilder->expr()->eq('p.thirdName', ':thirdName')
                );
            }

            if (!$ignoreDifferentFirstNameSearch || $activeFilterBirthDate) {
                $orNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $firstNames
                    ),
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $queryBuilder->expr()->like('p.firstName', ':firstNameStart')
                    )
                );
                $parameters->add(
                    new Parameter(
                        'firstNameStart',
                        3 == strlen($data->getFirstNameStart()) ?
                            $data->getFirstNameStart().'%' : $data->getFirstNameStart()
                    )
                );
            } else {
                $orNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $firstNames
                    )
                );
            }

            $parameters->add(new Parameter('firstName', $data->getFirstName()));
            $parameters->add(new Parameter('middleName', $data->getMiddleName()));
            $parameters->add(new Parameter('thirdName', $data->getThirdName()));
        }
        $parameters->add(
            new Parameter(
                'birthDate',
                $data->getBirthDate()->format(
                    $this->filterByParameterForBirthday(
                        $queryBuilder,
                        $acceptErrorInBirthDate,
                        $activeFilterBirthDate
                    )[1]
                )
            )
        );

        if ($onlyNullHelpCode) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNull('p.helpCode'),
                    $queryBuilder->expr()->eq('p.helpCode', "''")
                )
            );
        } else {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->isNotNull('p.helpCode'),
                    $queryBuilder->expr()->neq('p.helpCode', "''")
                )
            );
        }

        return $queryBuilder
            ->andWhere($orNames)
            ->andWhere($this->filterByParameterForBirthday($queryBuilder, $acceptErrorInBirthDate, $activeFilterBirthDate)[0])
            ->setParameters(
                $parameters
            )
            ->getQuery();
    }

    /**
     * Recherche sans soundex.
     *
     * @param QueryBuilder $queryBuilder
     * @param SearchData $data Données de recherche
     * @param int $acceptErrorInBirthDate Paramètre erreur dans la date de naissance autorisée
     * @param bool $activeFilterBirthDate Deuxième recherche avec filtre sur la date
     * @param bool $ignoreDifferentFirstNameSearch Paramètre prénom différent donne ambigu au lieu de connue
     * @param bool $withoutFirstName Ignorer le prénom
     *
     * @return Query
     */
    private function search(
        QueryBuilder &$queryBuilder,
        SearchData $data,
        int $acceptErrorInBirthDate,
        bool $activeFilterBirthDate = false,
        bool $ignoreDifferentFirstNameSearch = false,
        bool $withoutFirstName = false
    ): Query {
        $parameters = new ArrayCollection();
        if ($data->isComposeName()) {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->in('p.useName', ':composeName'),
                $queryBuilder->expr()->in('p.civilName', ':composeName')
            );
            $parameters->add(new Parameter("composeName", $data->getComposeName()));
        } elseif ($activeFilterBirthDate) {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('p.useName', ':nameWithoutHyphens1'),
                $queryBuilder->expr()->like('p.useName', ':nameWithoutHyphens2'),
                $queryBuilder->expr()->like('p.civilName', ':nameWithoutHyphens1'),
                $queryBuilder->expr()->like('p.civilName', ':nameWithoutHyphens2')
            );
            $parameters->add(
                new Parameter(
                    'nameWithoutHyphens1',
                    '%'.$data->getSingleNameWithoutHyphens().'%'
                )
            );
            $parameters->add(
                new Parameter(
                    'nameWithoutHyphens2',
                    '%-'.$data->getSingleNameWithoutHyphens().'%'
                )
            );
        } else {
            $lastNames = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->in('p.useName', ':namesWithoutHyphens'),
                $queryBuilder->expr()->in('p.civilName', ':namesWithoutHyphens')
            );
            $parameters->add(new Parameter('namesWithoutHyphens', $data->getAllNamesWithoutHyphens()));
        }

        if ($withoutFirstName) {
            $orNames = $lastNames;
        } else {
            $firstName = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->in('p.firstName', ':firstNames'),
                $queryBuilder->expr()->in('p.middleName', ':firstNames'),
                $queryBuilder->expr()->in('p.thirdName', ':firstNames')
            );
            if (!$ignoreDifferentFirstNameSearch || $activeFilterBirthDate) {
                $orNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $firstName
                    ),
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $queryBuilder->expr()->like('p.firstName', ':firstNameStart')
                    )
                );

                $parameters->add(
                    new Parameter(
                        'firstNameStart',
                        3 == strlen($data->getFirstNameStart()) ?
                            $data->getFirstNameStart().'%' : $data->getFirstNameStart()
                    )
                );
            } else {
                $orNames = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX(
                        $lastNames,
                        $firstName
                    )
                );
            }
            $parameters->add(new Parameter('firstNames', $data->getAllFirstNames()));
        }

        $parameters->add(
            new Parameter(
                'birthDate',
                $data->getBirthDate()->format(
                    $this->filterByParameterForBirthday(
                        $queryBuilder,
                        $acceptErrorInBirthDate,
                        $activeFilterBirthDate
                    )[1]
                )
            )
        );

        return $queryBuilder
            ->andWhere($orNames)
            ->andWhere($this->filterByParameterForBirthday($queryBuilder, $acceptErrorInBirthDate, $activeFilterBirthDate)[0])
            ->setParameters($parameters)
            ->getQuery();
    }

    /**
     * Filtre pour la recherche sur la date de naissance en fonction des paramètres choisis.
     *
     * @param int $acceptErrorInBirthDate Paramètre erreur dans la date de naissance autorisée
     */
    private function filterByParameterForBirthday(
        QueryBuilder &$queryBuilder,
        int $acceptErrorInBirthDate,
        bool $activeFilterBirthDate
    ): array {
        $birthDay = null;
        $birthDayFormat = '';

        if ($activeFilterBirthDate) {
            if (SearchHelper::SEARCH_DIFFERENT_BIRTHDAY === $acceptErrorInBirthDate) {
                $birthDay = $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->neq('p.birthDate', ':birthDate'),
                    $queryBuilder->expr()->eq('p.birthDate', ':birthDate')
                );
                $birthDayFormat = 'Y-m-d';
            } else {
                if (SearchHelper::SEARCH_DIFFERENT_YEAR === $acceptErrorInBirthDate) {
                    $birthDay = $queryBuilder->expr()->eq("CONCAT(MONTH(p.birthDate),'-',DAY(p.birthDate))", ':birthDate');
                    $birthDayFormat = 'n-j';
                }
                if (SearchHelper::SEARCH_DIFFERENT_DAY === $acceptErrorInBirthDate) {
                    $birthDay = $queryBuilder->expr()->eq("CONCAT(YEAR(p.birthDate),'-',MONTH(p.birthDate))", ':birthDate');
                    $birthDayFormat = 'Y-n';
                }
                if (SearchHelper::SEARCH_DIFFERENT_MONTH === $acceptErrorInBirthDate) {
                    $birthDay = $queryBuilder->expr()->eq("CONCAT(YEAR(p.birthDate),'-',DAY(p.birthDate))", ':birthDate');
                    $birthDayFormat = 'Y-j';
                }
                if (SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH === $acceptErrorInBirthDate) {
                    $birthDay = $queryBuilder->expr()->eq('YEAR(p.birthDate)', ':birthDate');
                    $birthDayFormat = 'Y';
                }
            }
        } else {
            $birthDay = $queryBuilder->expr()->eq('p.birthDate', ':birthDate');
            $birthDayFormat = 'Y-m-d';
        }

        return [$birthDay, $birthDayFormat];
    }

    private function logQuery(Query $query): void
    {
        $this->logger->info($query->getSQL(), [$query->getParameters()]);
    }

    public function findTerritoryInstructors(): array
    {
        return $this->createQueryBuilder('instructor')
            ->select(
                'instructor.departmentalHouseName',
                'instructor.departmentalHouseAddress',
                'instructor.departmentalHousePhone',
                'instructor.departmentalHouseMail'
            )
            ->where("instructor.departmentalHouseMail != ''")
            ->groupBy('instructor.departmentalHouseMail')
            ->orderBy('instructor.departmentalHouseName', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findUniqueLabel(): array
    {
        return $this->createQueryBuilder('person')
            ->select('person.label')
            ->where("person.label != ''")
            ->groupBy('person.label')
            ->orderBy('person.label', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
