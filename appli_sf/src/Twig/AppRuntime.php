<?php

namespace App\Twig;

use App\Helper\SearchHelper;
use App\Helper\UploaderHelper;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $publicDir;
    private $cacheManager;
    private $uploaderHelper;
    private $entrypointLookup;

    /**
     * AppExtension constructor.
     */
    public function __construct(
        string $publicDir,
        CacheManager $cacheManager,
        UploaderHelper $uploaderHelper,
        EntrypointLookupInterface $entrypointLookup
    ) {
        $this->publicDir = $publicDir;
        $this->cacheManager = $cacheManager;
        $this->uploaderHelper = $uploaderHelper;
        $this->entrypointLookup = $entrypointLookup;
    }

    public function getUploadedAssetPath(?File $file): string
    {
        if ($file) {
            return $this->uploaderHelper
                ->getPublicPath($file);
        }

        return '';
    }

    public function getEncoreEntryCssSource(string $entryName): string
    {
        $files = $this->entrypointLookup
            ->getCssFiles($entryName);
        $source = '';
        foreach ($files as $file) {
            $source .= file_get_contents($this->publicDir.'/'.$file);
        }

        return $source;
    }

    public function getSearchHasPdf(array $responseType): bool
    {
        return SearchHelper::hasLetterPdf($responseType);
    }

    /**
     * @param null $resolver
     *
     * @return string
     */
    public function imageFilter(string $path, string $filter, array $runtimeConfig = [], $resolver = null)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        if ('svg' === $ext) {
            return $path;
        } else {
            return $this->cacheManager->getBrowserPath($path, $filter, $runtimeConfig, $resolver);
        }
    }

    public function getFileContent(string $path): string
    {
        return file_get_contents($this->publicDir.'/'.$path);
    }
}
