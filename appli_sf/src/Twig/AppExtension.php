<?php

namespace App\Twig;

use App\Helper\UploaderHelper;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension implements ServiceSubscriberInterface
{
    public static function getSubscribedServices(): array
    {
        return [
            UploaderHelper::class,
            EntrypointLookupInterface::class,
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('uploaded_asset', [AppRuntime::class, 'getUploadedAssetPath']),
            new TwigFunction('encore_entry_css_source', [AppRuntime::class, 'getEncoreEntryCssSource']),
            new TwigFunction('search_has_pdf', [AppRuntime::class, 'getSearchHasPdf']),
            new TwigFunction('get_content', [AppRuntime::class, 'getFileContent']),
        ];
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('image_filter', [AppRuntime::class, 'imageFilter']),
        ];
    }
}
