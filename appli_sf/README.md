# Départements et notaires

Vous trouverez toutes les infos sur
le [GitLab](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2)
d'Adullact

## Première Installation

- Suivez toutes les étapes
  d'installations [ICI](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/installation.md)

## Mise à jour

- Retrouvez toutes les informations
  nécessaires [ICI](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/mise-a-jour.md)

## Plus d'informations

- Consultez
  le [README](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/README.md)
  du repository
