const dayjs = require('dayjs')
import 'dayjs/locale/fr'
describe('Page des stats', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker exec -i depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-agent')
            .then((user) => {
                this.userAgent = user
            })
    })

    it('Login', function () {
        cy.login(this.userAgent.username, this.userAgent.password, 'agent')
    })

    it('Affichage 5 derniers mois', () => {
        const monthFormat = 'MMMM-YY'
        const currentMonth = dayjs().locale('fr')

        cy.visit('/stats')

        // Nombre de mois affichés
        cy.get('table.stats-table thead th').eq(1).should('have.text', currentMonth.format(monthFormat))
        cy.get('table.stats-table thead th').eq(2).should('have.text', currentMonth.subtract(1, 'month').format(monthFormat))
        cy.get('table.stats-table thead th').eq(3).should('have.text', currentMonth.subtract(2, 'month').format(monthFormat))
        cy.get('table.stats-table thead th').eq(4).should('have.text', currentMonth.subtract(3, 'month').format(monthFormat))
        cy.get('table.stats-table thead th').eq(5).should('have.text', currentMonth.subtract(4, 'month').format(monthFormat))
    })

    it('Incrémentation nombre de recherches', () => {
        cy.visit('/stats')

        // Nombre de réponses
        cy.get('table.stats-table tbody').then((statsTableBody) => {
            const nbResponses = parseInt(statsTableBody.find('tr').eq(1).find('td').eq(1).text().trim())
            const nbResponsesRecup = parseInt(statsTableBody.find('tr').eq(2).find('td').eq(1).text().trim())

            cy.visit('/search')
            cy.fillSearch({
                deathDate:            '10/02/2020',
                deathLocation:        'VILLEFRANCHE',
                deathCertificateDate: '12/02/2020',
                firstName:            'Aimé',
                useName:              'OLMO',
                birthDate:            '06/12/1977'
            })

            cy.visit('/stats')
            cy.get('table.stats-table tbody tr').eq(1).find('td').eq(1).contains(nbResponses + 1)
            cy.get('table.stats-table tbody tr').eq(2).find('td').eq(1).contains(nbResponsesRecup + 1)
        })
    })

    /**
     * 6.3. Gestion des individus avec un code d’aide vide (en cours d’instruction)
     * -- 6.3-3 statistiques
     * https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149
     */
    it('Affichage du status en cours d\'instruction du tableau de statistiques', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Arno',
            useName:              'Nimousse',
            birthDate:            '01/01/1920'
        })

        cy.visit('/stats')
        cy.wait(200)
        cy.get('.stats-table > tbody').contains('Aides en cours d\'instruction')
    });

    it('Déconnexion', () => {
        cy.logout()
    })
})
