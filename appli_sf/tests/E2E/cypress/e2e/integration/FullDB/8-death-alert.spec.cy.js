describe('Alerte décès', () => {
    let deatAlertMail = 'alerte-deces@notaire.fr'

    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker exec -i depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
        cy.fixture('user-notaire')
            .then((user) => {
                this.userNotaire = user
            })
        cy.deleteAllMails()
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activer alerte décès dans les paramètres', function () {
        cy.visit('/admin/params')

        cy.get('input#settings_deathAlert').check()
        cy.get('input#settings_deathAlertMail').clear().type(deatAlertMail)
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Pas d\'alerte décès', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '20/10/2003',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Benjamin',
            useName:              'DANSARD',
            birthDate:            '02/02/1957'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(deatAlertMail, "Alerte").should('be.false')
    })

    it('Envoi d\'une alerte décès', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '02/10/2003',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Benjamin',
            useName:              'DANSARD',
            birthDate:            '02/02/1957'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(deatAlertMail, "Alerte").should('be.true')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
