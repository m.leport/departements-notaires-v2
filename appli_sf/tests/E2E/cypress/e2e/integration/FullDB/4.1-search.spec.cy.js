describe('Recherche', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker exec -i depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
        cy.fixture('user-notaire')
            .then((user) => {
                this.userNotaire = user
            })
        cy.fixture('user-agent')
            .then((user) => {
                this.userAgent = user
            })
        cy.deleteAllMails()
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Définir nom de l\'application', () => {
        cy.visit('/admin/params')
        cy.get('input#settings_appName').type('D&N')
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Mickael',
            useName:              'Dupont',
            birthDate:            '07/08/1986'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Personne connue, demande en cours d\'instruction', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Yvette',
            useName:              'ROSA',
            birthDate:            '08/12/1968'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "aide\\(s\\) en cours d&#039;instruction").should('be.true') // TODO
    })

    it('ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '12/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Clara',
            useName:              'BOURDIAU',
            birthDate:            '01/09/1966'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Personne connue, année de naissance différente, paramètre Année différente donne ambigu au lieu d\'inconnue non sélectionné, Résultat inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/12/1976'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre année de naissance différente donne ambigu', () => {
        cy.visit('/admin/params')

        cy.get('input#settings_acceptErrorInBirthDate_3').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, année de naissance différente, paramètre Année différente donne ambigu au lieu d\'inconnue sélectionné, Résultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/12/1976'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Personne connue, prénom différent, paramètre Prénom différent donne ambigu au lieu de connue désactivé, Résultat connue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Jeanine',
            useName:              'MILLINER',
            birthDate:            '01/01/1930'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "connue").should('be.true')
    })

    it('Personne connue, recherche avec un prénom composé sans accent et tiret. Résultat connu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Marie Therese',
            useName:              'Delaroche',
            birthDate:            '03/02/1925'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "connue").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre ignorer prénom différent', () => {
        cy.visit('/admin/params')

        cy.get('input#settings_ignoreDifferentFirstNameSearch').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, prénom différent, paramètre Prénom différent donne ambigu au lieu de connue activé, Résultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Jeanine',
            useName:              'MILLINER',
            birthDate:            '01/01/1930'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Personne connue, jour de naissance différent,paramètre Jour différent donne ambigu au lieu d\'inconnue non sélectionné, Résultat inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '01/12/1977'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre jour de naissance différente donne ambigu', () => {
        cy.visit('/admin/params')

        cy.get('input#settings_acceptErrorInBirthDate_1').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, jour de naissance différent, paramètre Jour différent donne ambigu au lieu d\'inconnue sélectionné, Résultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '01/12/1977'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Personne connue, mois de naissance différent, paramètre Mois différent donne ambigu au lieu d\'inconnue non sélectionné, Résultat inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/01/1977'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre mois de naissance différente donne ambigu', () => {
        cy.visit('/admin/params')
        cy.get('input#settings_acceptErrorInBirthDate_2').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, mois de naissance différent, paramètre Mois différent donne ambigu au lieu d\'inconnue sélectionné, Résultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/01/1977'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Personne connue, date de naissance différente, paramètre Date différente donne ambigu au lieu d\'inconnue non sélectionné, Résultat inconnue', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '01/01/1901'
        })

        cy.contains('Cette personne est inconnue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>inconnue</strong>").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre date de naissance différente donne ambigu', () => {
        cy.visit('/admin/params')

        cy.get('input#settings_acceptErrorInBirthDate_4').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, date de naissance différente, paramètre Date différente donne ambigu au lieu d\'inconnue sélectionné, Resultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '01/01/1901'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })



    it('Recherche faite sur un nomA réponde ambigu si l\'individu nomA-NomB', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '16/06/2018',
            deathLocation:        'Villeurbanne',
            deathCertificateDate: '16/06/2018',
            firstName:            'Armand',
            useName:              'Garan',
            birthDate:            '01/01/1915'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Recherche faite sur un nomB réponde ambigu si l\'individu nomA-NomB', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '16/06/2018',
            deathLocation:        'Villeurbanne',
            deathCertificateDate: '16/06/2018',
            firstName:            'Armand',
            useName:              'Servier',
            birthDate:            '01/01/1915'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Activation du paramètre soundex', () => {
        cy.visit('/admin/params')

        cy.get('input#settings_soundexSearch').check()
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, mais prénom différent et paramètre Soundex activé et paramètre Prénom différent donne ambigu au lieu de connu, Resultat ambigu', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Jeanine',
            useName:              'MILLINER',
            birthDate:            '01/01/1930'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "avec certitude").should('be.true')
    })

    /**
     * 6.3. Gestion des individus avec un code d’aide vide (en cours d’instruction)
     * -- 6.3-2 historique de recherche Agent ou Administrateur
     * https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149
     */
    it('La liste de recherches d\'un administrateur avec le status en cours d\'instruction', function () {
        cy.login(this.userAgent.username, this.userAgent.password, 'agent') // Connexion en tant qu'agent
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Arno',
            useName:              'Nimousse',
            birthDate:            '01/01/1920'
        })

        cy.visit('/search_logs');
        cy.get('#search_logs_filters_responseType').find('option').contains('Aide en cours d\'instruction')
        cy.get('#search_logs_filters_responseType').select('Aide en cours d\'instruction')
        cy.get('#search_logs_filters_user').select('agent')
        cy.get('form[name=search_logs_filters]').submit()
        cy.get('.table-search-logs').contains('Récupération, Indus probable, Aide en cours d\'instruction')
    });

    it('Déconnexion', () => {
        cy.logout()
    })
})
