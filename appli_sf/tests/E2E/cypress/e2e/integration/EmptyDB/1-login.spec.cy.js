describe('Connexion avec CSRF', () => {
    const inAdministration = () => {
        cy.location('href').should('match', /admin\/?$/)
        cy.contains('Mes actions administrateur')
    }

    const visitAdministration = () => {
        cy.visit('/admin')
        inAdministration()
    }

    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données (sans individu)
            cy.exec('cd /srv && docker exec depnot-php-1 /entrypoint sh -c "./bin/console doctrine:fixtures:load --no-interaction --group=without_persons"')
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
    })

    it('Redirection vers la page de login', () => {
        cy.visit('/')

        cy.location('href').should('match', /login$/)
        cy.contains('Départements et Notaires')
    })

    it('Erreur si pas de CSRF', function () {
        cy.loginByCSRF(this.userAdmin.username, this.userAdmin.password, 'invalid-token')
            .its('status')
            .should('eq', 200)
            .location('href').should('match', /login$/)
    })

    it('Connexion en tant qu\'admin', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')

        visitAdministration()
    })

    it('Déconnexion', () => {
        cy.logout()

        cy.location('href').should('match', /login$/)
        cy.contains('Départements et Notaires')
    })
})
