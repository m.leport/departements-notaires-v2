<?php

namespace App\Tests\UnitTests\Controller;

use App\Controller\SearchController;
use App\Data\SearchData;
use App\Data\SettingsData;
use App\Entity\Person;
use App\Helper\SearchHelper;
use App\Repository\InstructorRepository;
use App\Repository\PersonRepository;
use App\Repository\SearchLogRepository;
use App\Service\SettingsDataService;
use App\Service\SettingService;
use App\Service\ThemeService;
use App\Tests\UnitTests\TestCase\FixtureAwareTestCase;
use Doctrine\ORM\EntityManager;
use ReflectionException;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSearchControllerTest extends FixtureAwareTestCase
{
    protected TranslatorInterface $translator;

    protected EntityManager $entityManager;

    protected SettingService $settingService;

    protected SettingsDataService $settingsDataService;

    protected SettingsData $settingsData;

    protected int|bool $appTerritoryInstructor = 0;

    /**
     * Test recherche d'une personne connue, aide récupérable
     */
    public function testFoundRecPerson()
    {
        $this->searchFoundRecPerson(false);
    }

    /**
     * Test recherche d'une personne connue, aide non récupérable
     */
    public function testFoundNotRecPerson()
    {
        $this->searchFoundNotRecPerson(false);
    }

    /**
     * Test recherche d'une personne inconnue
     */
    public function testNotFoundPerson()
    {
        $this->searchNotFoundPerson(false);
    }

    /**
     * Test recherche d'une personne avec résultat ambigu
     */
    public function testAmbiguousPerson()
    {
        $this->searchAmbiguousPerson(false);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne connue, aide récupérable
     */
    public function testFoundRecPersonWithSoundex()
    {
        $this->searchFoundRecPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne connue, aide non récupérable
     */
    public function testFoundNotRecPersonWithSoundex()
    {
        $this->searchFoundNotRecPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne inconnue
     */
    public function testNotFoundPersonWithSoundex()
    {
        $this->searchNotFoundPerson(true);
    }

    /**
     * [SOUNDEX] Test recherche d'une personne avec résultat ambigu
     */
    public function testAmbiguousPersonWithSoundex()
    {
        $this->searchAmbiguousPerson(true);
    }

    /**
     * Test alerte décès
     */
    public function testDeathAlert()
    {
        // Test qui doit déclencher une alerte décès
        $this->searchDeathAlertPerson();

        // Test qui ne doit pas déclencher une alerte décès
        $this->searchDeathAlertPerson(false);
    }

    abstract protected function initSettingsDataService();

    /**
     * @param bool $useSoundex
     *
     * @throws ReflectionException
     */
    private function searchFoundRecPerson(bool $useSoundex): void
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();
        $themeService = new ThemeService();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Aimé');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('OLMO');
                break;
            default:
                $searchData->setUseName('OLMO');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '06/12/1977'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController(
            $this->settingsDataService,
            $this->translator,
            $themeService,
            $this->appTerritoryInstructor,
            $this->entityManager,
            $this->instructorRepository,
            $this->searchLogRepository,
            $this->personRepository
        );

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex, SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH, false]
        );

        $nameField = $this->settingsDataService->getData()->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME ?
            'useName' : 'civilName';

        $expectedPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Aime',
                $nameField  => 'OLMO',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '06/12/1977'),
            ]
        );

        $this->assertContains(SearchHelper::SEARCH_STATUS_FOUND_REC, $searchStatus);
        $this->assertSame($expectedPerson, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws ReflectionException
     */
    private function searchFoundNotRecPerson(bool $useSoundex): void
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();
        $themeService = new ThemeService();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Maesson');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('ALOUANE');
                break;
            default:
                $searchData->setUseName('ALOUANE');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '03/12/1954'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController(
            $this->settingsDataService,
            $this->translator,
            $themeService,
            $this->appTerritoryInstructor,
            $this->entityManager,
            $this->instructorRepository,
            $this->searchLogRepository,
            $this->personRepository
        );

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex, SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH, false]
        );

        $nameField = $this->settingsDataService->getData()->getSearchByName() === SearchHelper::SEARCH_BY_USE_NAME ?
            'useName' : 'civilName';

        $expectedPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Maesson',
                $nameField  => 'ALOUANE',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '03/12/1954'),
            ]
        );

        $this->assertContains(SearchHelper::SEARCH_STATUS_FOUND_NOTREC, $searchStatus);
        $this->assertSame($expectedPerson, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws ReflectionException
     */
    private function searchNotFoundPerson(bool $useSoundex): void
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();
        $themeService = new ThemeService();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Mickael');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('Dupont');
                break;
            default:
                $searchData->setUseName('Dupont');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '07/08/1986'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController(
            $this->settingsDataService,
            $this->translator,
            $themeService,
            $this->appTerritoryInstructor,
            $this->entityManager,
            $this->instructorRepository,
            $this->searchLogRepository,
            $this->personRepository
        );

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex, SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH, false]
        );

        $this->assertContains(SearchHelper::SEARCH_STATUS_NOTFOUND, $searchStatus);
        $this->assertSame(null, $person);
    }

    /**
     * @param bool $useSoundex
     *
     * @throws ReflectionException
     */
    private function searchAmbiguousPerson(bool $useSoundex): void
    {
        $this->initSettingsDataService();
        $searchData = new SearchData();
        $themeService = new ThemeService();

        $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Clara');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('BOURDIAU');
                break;
            default:
                $searchData->setUseName('BOURDIAU');
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '01/09/1966'));

        $personRepository = $this->entityManager->getRepository(Person::class);

        $controller = new SearchController(
            $this->settingsDataService,
            $this->translator,
            $themeService,
            $this->appTerritoryInstructor,
            $this->entityManager,
            $this->instructorRepository,
            $this->searchLogRepository,
            $this->personRepository
        );

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        [$searchStatus, $person] = $this->invokeMethod(
            $controller,
            'searchPerson',
            [$personRepository, $searchData, $useSoundex, SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH, false]
        );

        $this->assertContains(SearchHelper::SEARCH_STATUS_AMBIGUOUS, $searchStatus);
        $this->assertSame(null, $person);
    }

    /**
     * @param bool $needSendDeathAlert
     *
     * @throws ReflectionException
     */
    private function searchDeathAlertPerson(bool $needSendDeathAlert = true): void
    {
        $this->settingsData->setDeathAlert(true);
        $this->settingsData->setDeathAlertMail('notaires@deptnot.test');
        $this->initSettingsDataService();
        $searchData = new SearchData();
        $themeService = new ThemeService();

        if ($needSendDeathAlert) {
            $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '15/05/2019'));
        } else {
            $searchData->setDeathDate(\DateTime::createFromFormat('j/m/Y', '13/05/2019'));
        }

        $searchData->setDeathLocation('VILLEFRANCHE');
        $searchData->setDeathCertificateDate(\DateTime::createFromFormat('j/m/Y', '10/02/2020'));
        $searchData->setFirstName('Adrienne');

        switch ($this->settingsDataService->getData()->getSearchByName()) {
            case SearchHelper::SEARCH_BY_CIVIL_NAME:
                $searchData->setCivilName('GENILLON');
                $nameField = 'civilName';
                break;
            default:
                $searchData->setUseName('GENILLON');
                $nameField = 'useName';
        }

        $searchData->setBirthDate(\DateTime::createFromFormat('j/m/Y', '26/02/2001'));

        $personRepository = $this->entityManager->getRepository(Person::class);
        $foundPerson = $personRepository->findOneBy(
            [
                'firstName' => 'Adrienne',
                $nameField  => 'GENILLON',
                'birthDate' => \DateTime::createFromFormat('j/m/Y', '26/02/2001'),
            ]
        );

        $controller = new SearchController(
            $this->settingsDataService,
            $this->translator,
            $themeService,
            $this->appTerritoryInstructor,
            $this->entityManager,
            $this->instructorRepository,
            $this->searchLogRepository,
            $this->personRepository
        );

        /**
         * @var string $searchStatus
         * @var Person $person
         */
        $hasDeathAlert = $this->invokeMethod(
            $controller,
            'sendDeathAlert',
            [$foundPerson, $searchData]
        );

        $this->assertSame(true, $hasDeathAlert);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object     Instantiated object that we will run method on.
     * @param string  $methodName Method name to call
     * @param array   $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     * @throws ReflectionException
     */
    private function invokeMethod(object &$object, string $methodName, array $parameters = []): mixed
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
