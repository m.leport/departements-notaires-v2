<?php

namespace App\Tests\UnitTests\Controller;

use App\Data\SettingsData;
use App\DataFixtures\PersonCivilNameFixtures;
use App\DataFixtures\UserFixtures;
use App\Helper\SearchHelper;
use App\Service\SettingsDataService;
use ReflectionException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SearchCivilNameControllerTest extends AbstractSearchControllerTest
{
    public function setUp(): void
    {
        parent::setUp();

        $container = static::getContainer();

        $this->settingService = $container->get('App\Service\SettingService');
        $this->translator = $container->get('translator');
        $encoder = $container->get(UserPasswordHasherInterface::class);

        // Chargement des fixtures
        $this->addFixture(new UserFixtures($encoder));
        $this->addFixture(new PersonCivilNameFixtures());
        $this->executeFixtures();

        $this->settingsData = new SettingsData($this->settingService);
    }

    /**
     * Initialise les paramètres de l'application
     *
     * @throws ReflectionException
     */
    protected function initSettingsDataService(): void
    {
        $this->settingsData->setAppName('D&N');
        $this->settingsData->setSearchByName(SearchHelper::SEARCH_BY_CIVIL_NAME);

        $this->settingsDataService = new SettingsDataService($this->settingsData, $this->entityManager);
    }
}
